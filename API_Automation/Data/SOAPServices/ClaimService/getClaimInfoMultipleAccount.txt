<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <ns2:getClaimInfoRequest xmlns:ns2="http://www.t-mobile.com/esi/claim/getclaiminfo">
      <requestHeader xmlns="http://www.t-mobile.com/esi/base">
        <applicationId>{{Input_application}}</applicationId>
        <channelId>{{Input_channelId}}</channelId>
        <operatorId>1991</operatorId>
        <requestStartTime>{{Input_requestStartTime}}</requestStartTime>
        <trackingId>20652858867269616</trackingId>
        <version>NA</version>
        <clientId>{{Input_partnerId}}</clientId>
        <applicationUserId>{{Input_applicationUserId}}</applicationUserId>
        <storeId>{{Input_storeId}}</storeId>
        <dealerCode>{{Input_dealerCode}}</dealerCode>
        <senderId>{{Input_senderId}}</senderId>
        <workflowId>{{Input_workFlowId}}</workflowId>
      </requestHeader>
      <ns2:BAN>{{Input_BAN}}</ns2:BAN>
      <ns2:programType>{{Input_programType}}</ns2:programType>
      <ns2:deviceInfo>
        <ns2:MSISDN>{{Input_MSISDN1}}</ns2:MSISDN>
        <ns2:serialNumber>{{Input_serialNumber1}}</ns2:serialNumber>
      </ns2:deviceInfo>
      <ns2:deviceInfo>
        <ns2:MSISDN>{{Input_MSISDN2}}</ns2:MSISDN>
        <ns2:serialNumber>{{Input_serialNumber2}}</ns2:serialNumber>
      </ns2:deviceInfo>
      <ns2:deviceInfo>
        <ns2:MSISDN>{{Input_MSISDN3}}</ns2:MSISDN>
        <ns2:serialNumber>{{Input_serialNumber3}}</ns2:serialNumber>
      </ns2:deviceInfo>
      
    </ns2:getClaimInfoRequest>
  </soap:Body>
</soap:Envelope>