<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
   <soap:Body>
      <ns4:generateQuoteRequest xmlns:ns4="http://esp.t-mobile.com/mo">
         <ns2:header xmlns:ns2="http://esp.t-mobile.com/2012/eo">
            <ns2:sender>
               <ns2:senderId>{{InpReq_senderId}}</ns2:senderId>
               <ns2:channelId>{{Input_channel}}</ns2:channelId>
               <ns2:applicationId>{{Input_application}}</ns2:applicationId>
               <ns2:sessionId>{{Input_sessionId}}</ns2:sessionId>
               <ns2:timestamp>{{Input_timestamp}}</ns2:timestamp>
               <ns2:dealerCode>{{Input_dealerCode}}</ns2:dealerCode>
            </ns2:sender>
         </ns2:header>
         <accountNumber>{{InpReq_accountNumber}}</accountNumber>
         <programType>{{InpReq_programType}}</programType>
         <generateQuote>
            <MSISDN>{{InpReq_MSISDN}}</MSISDN>
            <manufacturer>{{InpReq_manufacturer}}</manufacturer>
            <model>{{InpReq_model}}</model>
            <serviceProvider>{{InpReq_serviceProvider}}</serviceProvider>
            <serialNumber>{{InpReq_serialNumber}}</serialNumber>
            <questionResponse>
               <name>{{InpReq_name}}</name>
               <script language="EN">
                  <questionScript>{{InpReq_questionScript}}</questionScript>
                  <answer>
                     <answerScript>NO</answerScript>
                     <answerName>LD_NO</answerName>
                  </answer>
               </script>
            </questionResponse>
            <quoteInfo>
               <tradeInDeviceSKU>{{InpReq_tradeInDeviceSKU}}</tradeInDeviceSKU>
               <eipDeviceLineID>{{InpReq_eipDeviceLineID}}</eipDeviceLineID>
               <eipDevicelevelBalance>{{InpReq_eipDevicelevelBalance}}</eipDevicelevelBalance>
               <eipPlanID>{{InpReq_eipPlanID}}</eipPlanID>
            </quoteInfo>
         </generateQuote>
      </ns4:generateQuoteRequest>
   </soap:Body>
</soap:Envelope>