<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <userProfilesAndPermissionsRequest xmlns="http://retail.tmobile.com/sdo">
      <header>
        <partnerId>{{Input_partnerId}}</partnerId>
        <requestId>1598877189735140686</requestId>
        
        <partnerTransactionId>{{Input_partnerTransactionId}}</partnerTransactionId>
        <partnerTimestamp>{{Input_partnerTimestamp}}</partnerTimestamp>
        <application>/UniversalUI</application>
        <channel>RETAIL</channel>
        <targetSystemUserId/>
        <storeId>9999</storeId>
        <dealerCode>{{Input_dealerCode}}</dealerCode>
      </header>
      <userId>{{Input_userId}}</userId>
    </userProfilesAndPermissionsRequest>
  </soap:Body>
</soap:Envelope>