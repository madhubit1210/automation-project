<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <ns2:esiGetAllServiceAgreementsRequest xmlns:ns2="http://www.t-mobile.com/esi/subscriber/getallserviceagreements">
      <requestHeader xmlns="http://www.t-mobile.com/esi/base">
        <applicationId>ACUI</applicationId>
        <channelId>RPS</channelId>
        <operatorId>{{Input_operatorid}}</operatorId>
        <requestStartTime>2016-06-29T05:09:46.568-07:00</requestStartTime>
        <trackingId>1598877189735140750</trackingId>
        <version>NA</version>
        <clientId>ACUI</clientId>
        <applicationUserId>CARTOCIOH10</applicationUserId>
        <storeId>9999</storeId>
        <dealerCode>0083150</dealerCode>
        <senderId>ACUI</senderId>
        <workflowId>1598877189735140685</workflowId>
      </requestHeader>
      <ns2:listOfSubscriber>
        <ns2:msisdn xsi:nil="true" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"/>
        <ns2:msisdn>{{Input_msisdn}}</ns2:msisdn>
      </ns2:listOfSubscriber>
      <ns2:productCatalogInd>true</ns2:productCatalogInd>
      <ns2:cdsIndicator>false</ns2:cdsIndicator>
      <ns2:serviceLevel>SUB</ns2:serviceLevel>
      <ns2:getFutureEffSocInd>true</ns2:getFutureEffSocInd>
      <ns2:hybridBanCategorization>true</ns2:hybridBanCategorization>
      <ns2:returnClassicToValue>true</ns2:returnClassicToValue>
      <ns2:retrievePHPCosts>true</ns2:retrievePHPCosts>
    </ns2:esiGetAllServiceAgreementsRequest>
  </soap:Body>
</soap:Envelope>