<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
  <soap:Body>
    <getSubscriberStatusCountRequest xmlns="http://retail.tmobile.com/sdo">
      <header>
        <partnerId>ACUI</partnerId>
        <requestId>1598877189735140537</requestId>
        <partnerSessionId>ae3SwRt4aTW015vFuzOiUful6nW0y9tsjsUCvjyhPXrHxYz8f6yO</partnerSessionId>
        <partnerTransactionId>1598877189735140536</partnerTransactionId>
        <partnerTimestamp>{{Input_partnerTimestamp}}</partnerTimestamp>
        <application>ACUI</application>
        <applicationUserId>CARTOCIOH10</applicationUserId>
        <channel>RPS</channel>
        <targetSystemUserId>2024</targetSystemUserId>
        <storeId>9999</storeId>
        <dealerCode>0083150</dealerCode>
      </header>
      <ban>{{Input_ban}}</ban>
    </getSubscriberStatusCountRequest>
  </soap:Body>
</soap:Envelope>