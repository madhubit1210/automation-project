<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
   <soapenv:Header/>
   <soapenv:Body>
      <passAgreementRequest xmlns="http://retail.tmobile.com/sdo">
         <header>
            <partnerId>{{Input_partnerId}}</partnerId>
            <requestId>{{Input_requestId}}</requestId>
            <partnerSessionId>{{Input_partnerSessionId}}</partnerSessionId>
            <partnerTransactionId>{{Input_partnerTransactionId}}-7</partnerTransactionId>
            <partnerTimestamp>2016-02-22T14:34:43</partnerTimestamp>
            <application>{{Input_application}}</application>
            <applicationUserId>{{Input_applicationUserId}}</applicationUserId>
            <channel>{{Input_channel}}</channel>
            <targetSystemUserId>{{Input_targetSystemUserId}}</targetSystemUserId>
            <storeId>{{Input_storeId}}</storeId>
            <dealerCode>{{Input_dealerCode}}</dealerCode>
         </header>
         <accountNumber>{{InputReq_accountNumber}}</accountNumber>
         <periodStartDate>2016-02-22T14:33:58</periodStartDate>
         <returnExpiredPass>false</returnExpiredPass>
      </passAgreementRequest>
   </soapenv:Body>
</soapenv:Envelope>