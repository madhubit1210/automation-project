<?xml version="1.0" ?>

<TestCase name="1" version="5">

<meta>
   <create version="8.2.0" buildNumber="8.2.0.244" author="DMethuk" date="07/18/2016" host="dmethuku" />
   <lastEdited version="8.2.0" buildNumber="8.2.0.244" author="DMethuk" date="07/21/2016" host="dmethuku" />
</meta>

<id>EA78B7CF4CFF11E6A97612DE20524153</id>
<Documentation>Put documentation of the Test Case here.</Documentation>
<IsInProject>true</IsInProject>
<sig>ZWQ9NSZ0Y3Y9NSZsaXNhdj04LjIuMCAoOC4yLjAuMjQ0KSZub2Rlcz04ODMxNDgzODQ=</sig>
<subprocess>false</subprocess>

<initState>
</initState>

<resultState>
</resultState>

<deletedProps>
</deletedProps>

    <Node name="Output Log Message" log=""
          type="com.itko.lisa.test.TestNodeLogger" 
          version="1" 
          uid="6B4549394FBB11E6B34B108220524153" 
          think="500-1S" 
          useFilters="true" 
          quiet="true" 
          next="end" > 


      <!-- Filters -->
      <Filter type="com.itko.lisa.xml.FilterXMLXPath">
        <valueToFilterKey>lisa.Output Log Message.rsp</valueToFilterKey>
<prop>1</prop>
<xpathq>/SOAP-ENV:Envelope/SOAP-ENV:Body/ns0:getClaimInfoResponse/ns1:status/ns1:statusCode/text()</xpathq>
<nsMap0>ns1=http://www.t-mobile.com/esi/base</nsMap0>
<nsMap1>ns0=http://www.t-mobile.com/esi/claim/getclaiminfo</nsMap1>
<nsMap2>SOAP-ENV=http://schemas.xmlsoap.org/soap/envelope/</nsMap2>
      </Filter>

      <Filter type="com.itko.lisa.xml.FilterXMLXPath">
        <valueToFilterKey>lisa.Output Log Message.rsp</valueToFilterKey>
<prop>2</prop>
<xpathq>/SOAP-ENV:Envelope/SOAP-ENV:Body/ns0:getClaimInfoResponse/ns1:status/ns1:statusMessage/text()</xpathq>
<nsMap0>ns1=http://www.t-mobile.com/esi/base</nsMap0>
<nsMap1>ns0=http://www.t-mobile.com/esi/claim/getclaiminfo</nsMap1>
<nsMap2>SOAP-ENV=http://schemas.xmlsoap.org/soap/envelope/</nsMap2>
      </Filter>

      <Filter type="com.itko.lisa.xml.FilterXMLXPath">
        <valueToFilterKey>lisa.Output Log Message.rsp</valueToFilterKey>
<prop>4</prop>
<xpathq>/SOAP-ENV:Envelope/SOAP-ENV:Body/ns0:getClaimInfoResponse/ns1:status/ns1:subStatus/ns1:code/text()</xpathq>
<nsMap0>ns1=http://www.t-mobile.com/esi/base</nsMap0>
<nsMap1>ns0=http://www.t-mobile.com/esi/claim/getclaiminfo</nsMap1>
<nsMap2>SOAP-ENV=http://schemas.xmlsoap.org/soap/envelope/</nsMap2>
      </Filter>

<log>&lt;?xml version=&quot;1.0&quot; encoding=&quot;UTF-8&quot;?&gt;&#13;&#10;&lt;SOAP-ENV:Envelope xmlns:SOAP-ENV=&quot;http://schemas.xmlsoap.org/soap/envelope/&quot;&gt;&#13;&#10;  &lt;SOAP-ENV:Body&gt;&#13;&#10;    &lt;ns0:getClaimInfoResponse xmlns:ns0=&quot;http://www.t-mobile.com/esi/claim/getclaiminfo&quot;&gt;&#13;&#10;      &lt;ns1:status xmlns:ns1=&quot;http://www.t-mobile.com/esi/base&quot;&gt;&#13;&#10;        &lt;ns1:statusCode&gt;102&lt;/ns1:statusCode&gt;&#13;&#10;        &lt;ns1:statusMessage&gt;Error&lt;/ns1:statusMessage&gt;&#13;&#10;        &lt;ns1:debugData&gt;&amp;lt;?xml version=&quot;1.0&quot; encoding=&quot;UTF-8&quot;?&amp;gt; &amp;lt;ns2:getClaimInfoRequest xmlns:ns2=&quot;http://www.t-mobile.com/esi/claim/getclaiminfo&quot; xmlns:soap=&quot;http://schemas.xmlsoap.org/soap/envelope/&quot;&amp;gt;     &amp;lt;requestHeader xmlns=&quot;http://www.t-mobile.com/esi/base&quot;&amp;gt;         &amp;lt;applicationId&amp;gt;ACUI&amp;lt;/applicationId&amp;gt;         &amp;lt;channelId&amp;gt;Retail&amp;lt;/channelId&amp;gt;         &amp;lt;operatorId&amp;gt;1991&amp;lt;/operatorId&amp;gt;         &amp;lt;requestStartTime&amp;gt;2016-06-29T05:09:47.268-07:00&amp;lt;/requestStartTime&amp;gt;         &amp;lt;trackingId&amp;gt;20652858867269616&amp;lt;/trackingId&amp;gt;         &amp;lt;version&amp;gt;NA&amp;lt;/version&amp;gt;         &amp;lt;clientId&amp;gt;ACUI&amp;lt;/clientId&amp;gt;         &amp;lt;applicationUserId&amp;gt;retailjump&amp;lt;/applicationUserId&amp;gt;         &amp;lt;storeId&amp;gt;9999&amp;lt;/storeId&amp;gt;         &amp;lt;dealerCode&amp;gt;0083150&amp;lt;/dealerCode&amp;gt;         &amp;lt;senderId&amp;gt;ACUI&amp;lt;/senderId&amp;gt;         &amp;lt;workflowId&amp;gt;1598877189735140685&amp;lt;/workflowId&amp;gt;     &amp;lt;/requestHeader&amp;gt;     &amp;lt;ns2:BAN&amp;gt;953603657&amp;lt;/ns2:BAN&amp;gt;     &amp;lt;ns2:programType&amp;gt;JUMP&amp;lt;/ns2:programType&amp;gt;     &amp;lt;ns2:deviceInfo&amp;gt;         &amp;lt;ns2:MSISDN/&amp;gt;         &amp;lt;ns2:serialNumber&amp;gt;359230064569874&amp;lt;/ns2:serialNumber&amp;gt;     &amp;lt;/ns2:deviceInfo&amp;gt;     &amp;lt;ns2:deviceInfo&amp;gt;         &amp;lt;ns2:MSISDN&amp;gt;4042000845&amp;lt;/ns2:MSISDN&amp;gt;         &amp;lt;ns2:serialNumber&amp;gt;355880058178597&amp;lt;/ns2:serialNumber&amp;gt;     &amp;lt;/ns2:deviceInfo&amp;gt;     &amp;lt;ns2:deviceInfo&amp;gt;         &amp;lt;ns2:MSISDN&amp;gt;4042000846&amp;lt;/ns2:MSISDN&amp;gt;         &amp;lt;ns2:serialNumber&amp;gt;355880058385929&amp;lt;/ns2:serialNumber&amp;gt;     &amp;lt;/ns2:deviceInfo&amp;gt; &amp;lt;/ns2:getClaimInfoRequest&amp;gt;&lt;/ns1:debugData&gt;&#13;&#10;        &lt;ns1:subStatus&gt;&#13;&#10;          &lt;ns1:code1&gt;VALIDATION_ERROR&lt;/ns1:code1&gt;&#13;&#10;          &lt;ns1:description&gt;MSISDN is Invalid/Missing&lt;/ns1:description&gt;&#13;&#10;        &lt;/ns1:subStatus&gt;&#13;&#10;      &lt;/ns1:status&gt;&#13;&#10;    &lt;/ns0:getClaimInfoResponse&gt;&#13;&#10;  &lt;/SOAP-ENV:Body&gt;&#13;&#10;&lt;/SOAP-ENV:Envelope&gt;&#13;&#10;&#13;&#10;</log>
    </Node>


    <Node name="abort" log=""
          type="com.itko.lisa.test.AbortStep" 
          version="1" 
          uid="EA78DEE14CFF11E6A97612DE20524153" 
          think="0h" 
          useFilters="true" 
          quiet="true" 
          next="" > 

    </Node>


    <Node name="fail" log=""
          type="com.itko.lisa.test.Abend" 
          version="1" 
          uid="EA78DEE34CFF11E6A97612DE20524153" 
          think="0h" 
          useFilters="true" 
          quiet="true" 
          next="abort" > 

    </Node>


    <Node name="end" log=""
          type="com.itko.lisa.test.NormalEnd" 
          version="1" 
          uid="EA78DEE54CFF11E6A97612DE20524153" 
          think="0h" 
          useFilters="true" 
          quiet="true" 
          next="fail" > 

    </Node>


</TestCase>
