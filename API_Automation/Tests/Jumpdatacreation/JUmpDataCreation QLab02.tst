<?xml version="1.0" encoding="UTF-8"?>
<TestCase name="JUmpDataCreation QLab02" version="5">

<meta>
   <create author="DMethuk" buildNumber="8.2.0.244" date="07/07/2016" host="dmethuku" version="8.2.0"/>
   <lastEdited author="DMethuk" buildNumber="8.2.0.244" date="07/22/2016" host="dmethuku" version="8.2.0"/>
</meta>

<id>8A40C6EF529E11E6A791082820524153</id>
<Documentation>Put documentation of the Test Case here.</Documentation>
<IsInProject>true</IsInProject>
<sig>ZWQ9NSZ0Y3Y9NSZsaXNhdj04LjIuMCAoOC4yLjAuMjQ0KSZub2Rlcz0tNTQ4OTk0NjMw</sig>
<subprocess>false</subprocess>

<initState>
</initState>

<resultState>
</resultState>

<deletedProps>
</deletedProps>

    <Node log="" name="SSNRetail" next="BanCreation" quiet="false" think="500-1S" type="com.itko.lisa.ws.nx.NxWSStep" uid="8A40ED00529E11E6A791082820524153" useFilters="true" version="1"> 


      <!-- Filters -->
      <Filter type="com.itko.lisa.xml.FilterXMLXPath">
        <valueToFilterKey>lisa.SSNRetail.rsp</valueToFilterKey>
<prop>WIP_CustomerId</prop>
<xpathq>/S:Envelope/S:Body/:customerQualificationResponse/:customerQualificationInfo/:wipCustomerId/text()</xpathq>
<nsMap0>S=http://schemas.xmlsoap.org/soap/envelope/</nsMap0>
<nsMap1>=http://retail.tmobile.com/sdo</nsMap1>
      </Filter>

      <Filter type="com.itko.lisa.test.TimeStampFilter">
        <valueToFilterKey>lisa.Web Service customerQualificationRequest.rsp</valueToFilterKey>
<pre>true</pre>
<post>false</post>
<datePattern>yyyy-MM-dd</datePattern>
<preprop>Date</preprop>
<postprop/>
<Offset>0</Offset>
      </Filter>


      <!-- Data Sets -->
<readrec>Read Rows from Excel File</readrec>
<wsdl>{{LISA_RELATIVE_PROJ_URL}}/Data/wsdls/CustomerQualificationService_SOAP_V1_1.wsdl</wsdl>
<endpoint>https://QATTBCO523.UNIX.GSM1900.ORG:4116/eProxy/service/CustomerQualification_SOAP_V1</endpoint>
<targetNamespace>http://retail.tmobile.com/service</targetNamespace>
<service>CustomerQualificationService</service>
<port>CustomerQualificationServiceSOAPPort_V1</port>
<operation>submitCreditApplication</operation>
<onError>abort</onError>
<maintainSession>true</maintainSession>
<clearSession>false</clearSession>
<request itko_enc="base64">PHNvYXBlbnY6RW52ZWxvcGUgeG1sbnM6c29hcGVudj0iaHR0cDovL3NjaGVtYXMueG1sc29hcC5vcmcvc29hcC9lbnZlbG9wZS8iIHhtbG5zOnNkbz0iaHR0cDovL3JldGFpbC50bW9iaWxlLmNvbS9zZG8iPgogICAgPHNvYXBlbnY6SGVhZGVyLz4KICAgIDxzb2FwZW52OkJvZHk+CiAgICAgICAgPHNkbzpjdXN0b21lclF1YWxpZmljYXRpb25SZXF1ZXN0PgogICAgICAgICAgICA8c2RvOmhlYWRlciByc3BTZXJ2aWNlVmVyc2lvbj0iMS4xMy4xIj4KICAgICAgICAgICAgICAgIDxzZG86cGFydG5lcklkPlJFVEFJTERJUkVDVDwvc2RvOnBhcnRuZXJJZD4KICAgICAgICAgICAgICAgIDxzZG86cGFydG5lclRyYW5zYWN0aW9uSWQ+Q0JSSU5ER1NDPC9zZG86cGFydG5lclRyYW5zYWN0aW9uSWQ+CiAgICAgICAgICAgICAgICA8c2RvOnBhcnRuZXJUaW1lc3RhbXA+MjAxNi0wMy0yOVQxMTo0NDoxMS44MzA2NDY3KzA1OjMwPC9zZG86cGFydG5lclRpbWVzdGFtcD4KICAgICAgICAgICAgICAgIDxzZG86Y2hhbm5lbD5SRVRBSUw8L3NkbzpjaGFubmVsPgogICAgICAgICAgICAgICAgPHNkbzpkZWFsZXJDb2RlPjI2OTM2NjQ8L3NkbzpkZWFsZXJDb2RlPgogICAgICAgICAgICAgICAgPHNkbzpyc3BUcmFuc2FjdGlvbklkPkNCUklOREdTQzwvc2RvOnJzcFRyYW5zYWN0aW9uSWQ+CiAgICAgICAgICAgICAgICA8c2RvOmF1dGhlbnRpY2F0ZWRJZD5DQlJJTkRHU0M8L3NkbzphdXRoZW50aWNhdGVkSWQ+CiAgICAgICAgICAgIDwvc2RvOmhlYWRlcj4KICAgICAgICAgICAgPHNkbzpjcmVkaXRBcHBsaWNhdGlvbj48IS0tc2RvOm5hbWVUaXRsZT4/PC9zZG86bmFtZVRpdGxlLS0+CiAgICAgICAgICAgICAgICA8c2RvOmZpcnN0TmFtZT5ETE08L3NkbzpmaXJzdE5hbWU+CiAgICAgICAgICAgICAgICA8c2RvOm1pZGRsZUluaXRpYWw+QTwvc2RvOm1pZGRsZUluaXRpYWw+CiAgICAgICAgICAgICAgICA8c2RvOmxhc3ROYW1lPlJFVEFJTCBEQVRBIERPTk9UVVNFPC9zZG86bGFzdE5hbWU+PCEtLXNkbzpuYW1lU3VmZml4Pj88L3NkbzpuYW1lU3VmZml4LS0+CiAgICAgICAgICAgICAgICA8c2RvOmJpbGxpbmdBZGRyZXNzPgogICAgICAgICAgICAgICAgICAgIDxzZG86YWRkcmVzc0NsYXNzaWZpY2F0aW9uPlNUUkVFVF9BRERSRVNTPC9zZG86YWRkcmVzc0NsYXNzaWZpY2F0aW9uPgogICAgICAgICAgICAgICAgICAgIDxzZG86YWRkcmVzczE+MSBSQVZJTklBIERSIFNURSAxMDAwPC9zZG86YWRkcmVzczE+CiAgICAgICAgICAgICAgICAgICAgPHNkbzpjaXR5PkFUTEFOVEE8L3NkbzpjaXR5PgogICAgICAgICAgICAgICAgICAgIDxzZG86c3RhdGU+R0E8L3NkbzpzdGF0ZT4KICAgICAgICAgICAgICAgICAgICA8c2RvOnppcENvZGU+MzAzNDY8L3Nkbzp6aXBDb2RlPgogICAgICAgICAgICAgICAgPC9zZG86YmlsbGluZ0FkZHJlc3M+CiAgICAgICAgICAgICAgICA8c2RvOmhvbWVQaG9uZT40MjU0NzU0NzM1PC9zZG86aG9tZVBob25lPgogICAgICAgICAgICAgICAgPHNkbzptb2JpbGVOdW1iZXI+NDI1NDc1NDczNTwvc2RvOm1vYmlsZU51bWJlcj4KICAgICAgICAgICAgICAgIDxzZG86d29ya1Bob25lPjQyNTQ3NTQ3MzU8L3Nkbzp3b3JrUGhvbmU+CiAgICAgICAgICAgICAgICA8c2RvOnNzbj57e1NTTn19PC9zZG86c3NuPjwhLS1zZG86ZmVpbj4/PC9zZG86ZmVpbi0tPjwhLS1Zb3UgaGF2ZSBhIENIT0lDRSBvZiB0aGUgbmV4dCAyIGl0ZW1zIGF0IHRoaXMgbGV2ZWwtLT48IS0tc2RvOmJ1c2luZXNzTmFtZT4/PC9zZG86YnVzaW5lc3NOYW1lLS0+CiAgICAgICAgICAgICAgICA8c2RvOmVtcGxveWVyTmFtZT5CT0VJTkc8L3NkbzplbXBsb3llck5hbWU+CiAgICAgICAgICAgICAgICA8c2RvOmRhdGVPZkJpcnRoPjE5NzgtMDgtMDc8L3NkbzpkYXRlT2ZCaXJ0aD4KICAgICAgICAgICAgICAgIDxzZG86aWRlbnRpZmljYXRpb24+CiAgICAgICAgICAgICAgICAgICAgPHNkbzppZE51bWJlcj4xMzg1MjQxOTM8L3NkbzppZE51bWJlcj4KICAgICAgICAgICAgICAgICAgICA8c2RvOmlkVHlwZT5ETDwvc2RvOmlkVHlwZT4KICAgICAgICAgICAgICAgICAgICA8c2RvOmlkSXNzdWluZ1N0YXRlPkdBPC9zZG86aWRJc3N1aW5nU3RhdGU+CiAgICAgICAgICAgICAgICAgICAgPHNkbzppZEV4cGlyYXRpb25EYXRlPjIwMTgtMDYtMjc8L3NkbzppZEV4cGlyYXRpb25EYXRlPgogICAgICAgICAgICAgICAgPC9zZG86aWRlbnRpZmljYXRpb24+PCEtLU9wdGlvbmFsOi0tPgogICAgICAgICAgICAgICAgPHNkbzpjb250cmFjdD50cnVlPC9zZG86Y29udHJhY3Q+CiAgICAgICAgICAgIDwvc2RvOmNyZWRpdEFwcGxpY2F0aW9uPgogICAgICAgICAgICA8c2RvOmFjY2VwdGVkQ3JlZGl0Y2hlY2s+dHJ1ZTwvc2RvOmFjY2VwdGVkQ3JlZGl0Y2hlY2s+PCEtLU9wdGlvbmFsOi0tPjwhLS1zZG86Y3JlZGl0QXBwbGljYXRpb25SZWZOdW1iZXI+Pzwvc2RvOmNyZWRpdEFwcGxpY2F0aW9uUmVmTnVtYmVyLS0+CiAgICAgICAgPC9zZG86Y3VzdG9tZXJRdWFsaWZpY2F0aW9uUmVxdWVzdD4KICAgIDwvc29hcGVudjpCb2R5Pgo8L3NvYXBlbnY6RW52ZWxvcGU+</request>
<style>document</style>
<use>literal</use>
<soapAction>submitCreditApplication</soapAction>
<sslInfo>
<ssl-keystore-file/>
<ssl-keystore-password-enc>f5504e2d23a7888253a27e8ef52607d8</ssl-keystore-password-enc>
<ssl-alias/>
<ssl-key-password-enc>f5504e2d23a7888253a27e8ef52607d8</ssl-key-password-enc>
</sslInfo>
<wsiInfo>
<wsi-check-wsdl>false</wsi-check-wsdl>
<wsi-check-msg>false</wsi-check-msg>
<wsi-display-type>onlyFailed</wsi-display-type>
<wsi-on-fail>continue</wsi-on-fail>
</wsiInfo>
<transport>
<soapVersionUri>1.1</soapVersionUri>
<httpVersionUri>1.1</httpVersionUri>
<webMethodUri>POST</webMethodUri>
<mtom>false</mtom>
<dontSendRequest>false</dontSendRequest>
<fault-as-error>true</fault-as-error>
<callTimeout>30000</callTimeout>
</transport>
<uddiActive>false</uddiActive>
<uddi-result>
<uddi-selection>
</uddi-selection>
</uddi-result>
<wss4jInfo>
<version>5</version>
<wssecurity>
<isSend>true</isSend>
<must>false</must>
<role/>
<usernameToken>
<enabled>true</enabled>
<username>BESTBUY01</username>
<password>@Password1</password>
<type>PasswordText</type>
<signed>false</signed>
<nonce>true</nonce>
<craeted>true</craeted>
<useMsTimestamp>true</useMsTimestamp>
</usernameToken>
</wssecurity>
<wsi-bsp>false</wsi-bsp>
</wss4jInfo>
<wsaInfo>
<useAddressing>false</useAddressing>
<must>false</must>
<to/>
<toOverride>false</toOverride>
<toDefault>true</toDefault>
<from/>
<fromOverride>false</fromOverride>
<fromDefault>true</fromDefault>
<action/>
<actionOverride>false</actionOverride>
<actionDefault>true</actionDefault>
<msgid/>
<msgidOverride>false</msgidOverride>
<msgidDefault>true</msgidDefault>
<replyTo/>
<replyToOverride>false</replyToOverride>
<faultTo/>
<faulttToOverride>false</faulttToOverride>
<addressingVersion>http://www.w3.org/2005/08/addressing</addressingVersion>
</wsaInfo>
<customHTTPHeaderInfo>
    <Parameter>
    <key>username</key>
    <value>BESTBUY01</value>
    </Parameter>
    <Parameter>
    <key>password</key>
    <value>@Password1</value>
    </Parameter>
</customHTTPHeaderInfo>
<attachments>
<attachment-type>MIME</attachment-type>
</attachments>
<mtom-paths>
</mtom-paths>
    </Node>


    <Node log="" name="BanCreation" next="CreateInstallmentPlan_1" quiet="false" think="0" type="com.itko.lisa.ws.nx.NxWSStep" uid="8A40ED01529E11E6A791082820524153" useFilters="true" version="1"> 


      <!-- Filters -->
      <Filter type="com.itko.lisa.xml.FilterXMLXPath">
        <valueToFilterKey>lisa.BanCreation.rsp</valueToFilterKey>
<prop>BAN</prop>
<xpathq>/S:Envelope/S:Body/:activationResponse/:activationDetails/:ban/text()</xpathq>
<nsMap0>S=http://schemas.xmlsoap.org/soap/envelope/</nsMap0>
<nsMap1>=http://retail.tmobile.com/sdo</nsMap1>
      </Filter>

      <Filter type="com.itko.lisa.xml.FilterXMLXPath">
        <valueToFilterKey>lisa.BanCreation.rsp</valueToFilterKey>
<prop>MSISDN_1</prop>
<xpathq>/S:Envelope/S:Body/:activationResponse/:activationDetails/:lineDetails[1]/:msisdn/text()</xpathq>
<nsMap0>S=http://schemas.xmlsoap.org/soap/envelope/</nsMap0>
<nsMap1>=http://retail.tmobile.com/sdo</nsMap1>
      </Filter>

      <Filter type="com.itko.lisa.xml.FilterXMLXPath">
        <valueToFilterKey>lisa.BanCreation.rsp</valueToFilterKey>
<prop>MSISDN_2</prop>
<xpathq>/S:Envelope/S:Body/:activationResponse/:activationDetails/:lineDetails[2]/:msisdn/text()</xpathq>
<nsMap0>S=http://schemas.xmlsoap.org/soap/envelope/</nsMap0>
<nsMap1>=http://retail.tmobile.com/sdo</nsMap1>
      </Filter>

<wsdl>{{LISA_RELATIVE_PROJ_URL}}/Data/wsdls/ActivationService_SOAP_V1_1.wsdl</wsdl>
<endpoint>https://qattbco523.unix.gsm1900.org:4116/eProxy/service/ActivationService_SOAP_V1</endpoint>
<targetNamespace>http://retail.tmobile.com/service</targetNamespace>
<service>ActivationService</service>
<port>ActivationServiceSOAPPort_V1</port>
<operation>activateSubscribers</operation>
<onError>abort</onError>
<maintainSession>true</maintainSession>
<clearSession>false</clearSession>
<request itko_enc="base64">PHNvYXBlbnY6RW52ZWxvcGUgeG1sbnM6c29hcGVudj0iaHR0cDovL3NjaGVtYXMueG1sc29hcC5vcmcvc29hcC9lbnZlbG9wZS8iIHhtbG5zOnNkbz0iaHR0cDovL3JldGFpbC50bW9iaWxlLmNvbS9zZG8iPgogICAgPHNvYXBlbnY6SGVhZGVyLz4KICAgIDxzb2FwZW52OkJvZHk+CiAgICAgICAgPHNkbzphY3RpdmF0aW9uUmVxdWVzdD4KICAgICAgICAgICAgPHNkbzpoZWFkZXI+CiAgICAgICAgICAgICAgICA8c2RvOnBhcnRuZXJJZD5TT05ZPC9zZG86cGFydG5lcklkPgogICAgICAgICAgICAgICAgPHNkbzpwYXJ0bmVyVHJhbnNhY3Rpb25JZD5UTU9CSUxFPC9zZG86cGFydG5lclRyYW5zYWN0aW9uSWQ+CiAgICAgICAgICAgICAgICA8c2RvOnBhcnRuZXJUaW1lc3RhbXA+MjAxNS0xMS0xNVQwMDowMDowMFo8L3NkbzpwYXJ0bmVyVGltZXN0YW1wPgogICAgICAgICAgICAgICAgPHNkbzpkZWFsZXJDb2RlPjI2OTM2NjQ8L3NkbzpkZWFsZXJDb2RlPgogICAgICAgICAgICA8L3NkbzpoZWFkZXI+CiAgICAgICAgICAgIDxzZG86Y3VzdG9tZXJMb29rdXA+CiAgICAgICAgICAgICAgICA8c2RvOndpcEN1c3RvbWVySWQ+e3tXSVBfQ3VzdG9tZXJJZH19PC9zZG86d2lwQ3VzdG9tZXJJZD4KICAgICAgICAgICAgPC9zZG86Y3VzdG9tZXJMb29rdXA+CiAgICAgICAgICAgIDxzZG86YWNjb3VudFR5cGU+SU5ESVZJRFVBTF9SRUdVTEFSPC9zZG86YWNjb3VudFR5cGU+CiAgICAgICAgIDwhLS0xIG9yIG1vcmUgcmVwZXRpdGlvbnM6LS0+CiAgICAgICAgICAgIDxzZG86bGluZURldGFpbHM+CiAgICAgICAgICAgICAgICA8c2RvOmxpbmVJZD4xPC9zZG86bGluZUlkPgogICAgICAgICAgICAgICAgPHNkbzpzZWxlY3RlZFJhdGVwbGFuPk5BVFRVPC9zZG86c2VsZWN0ZWRSYXRlcGxhbj4KICAgICAgICAgICAgICAgIDxzZG86bWFya2V0Q29kZT5BVEc8L3NkbzptYXJrZXRDb2RlPgogICAgICAgICAgICAgICAgPHNkbzpucGFJbmZvPgogICAgICAgICAgICAgICAgICAgIDxzZG86bnBhPjQwNDwvc2RvOm5wYT4KICAgICAgICAgICAgICAgPCEtLXNkbzpucGFOeHg+NDI1NTg2PC9zZG86bnBhTnh4LS0+CiAgICAgICAgICAgICAgICAgICAgPHNkbzpuZ3A+QVRHPC9zZG86bmdwPgogICAgICAgICAgICAgICAgICAgIDxzZG86ZGVzY3JpcHRpb24+QXRsYW50YSxHQTwvc2RvOmRlc2NyaXB0aW9uPgogICAgICAgICAgICAgICAgICAgIDxzZG86bWFya2V0Q29kZT5BVEc8L3NkbzptYXJrZXRDb2RlPgogICAgICAgICAgICAgICAgPC9zZG86bnBhSW5mbz4JCQkKICAgICAgICAgICAgICAgIDxzZG86c2VydmljZUJlZ2luRGF0ZT57e0RhdGV9fTwvc2RvOnNlcnZpY2VCZWdpbkRhdGU+CiAgICAgICAgICAgICAgICA8c2RvOmNvbnRyYWN0TGVuZ3RoPjA8L3Nkbzpjb250cmFjdExlbmd0aD4KICAgICAgICAgICAgICAgIDxzZG86c2VsZWN0ZWRTZXJ2aWNlPgogICAgICAgICAgICAgICAgICAgIDxzZG86c29jPkpVTVAyPC9zZG86c29jPgogICAgICAgICAgICAgICAgICAgIDxzZG86bGV2ZWw+U1VCU0NSSUJFUjwvc2RvOmxldmVsPgogICAgICAgICAgICAgICAgICAgIDxzZG86cHJvZHVjdFR5cGU+R1NNPC9zZG86cHJvZHVjdFR5cGU+CiAgICAgICAgICAgICAgICA8L3NkbzpzZWxlY3RlZFNlcnZpY2U+CiAgICAgICAgICAgICAgICA8c2RvOnByb2R1Y3RUeXBlPkdTTTwvc2RvOnByb2R1Y3RUeXBlPgogICAgICAgICAgICAgICAgPHNkbzpzaW0+e3tTSU19fTwvc2RvOnNpbT4KICAgICAgICAgICAgICAgIDxzZG86aW1laT57e0lNRUl9fTwvc2RvOmltZWk+CiAgICAgICAgICAgIDwvc2RvOmxpbmVEZXRhaWxzPgogICAgICAgICAgICA8c2RvOmxpbmVEZXRhaWxzPgogICAgICAgICAgICAgICAgPHNkbzpsaW5lSWQ+Mjwvc2RvOmxpbmVJZD4KICAgICAgICAgICAgICAgIDxzZG86c2VsZWN0ZWRSYXRlcGxhbj5OQVRUVTwvc2RvOnNlbGVjdGVkUmF0ZXBsYW4+CiAgICAgICAgICAgICAgICA8c2RvOm1hcmtldENvZGU+QVRHPC9zZG86bWFya2V0Q29kZT4KICAgICAgICAgICAgICAgIDxzZG86bnBhSW5mbz4KICAgICAgICAgICAgICAgICAgICA8c2RvOm5wYT40MDQ8L3NkbzpucGE+CiAgICAgICAgICAgICAgIDwhLS1zZG86bnBhTnh4PjQyNTU4Njwvc2RvOm5wYU54eC0tPgogICAgICAgICAgICAgICAgICAgIDxzZG86bmdwPkFURzwvc2RvOm5ncD4KICAgICAgICAgICAgICAgICAgICA8c2RvOmRlc2NyaXB0aW9uPkF0bGFudGEsR0E8L3NkbzpkZXNjcmlwdGlvbj4KICAgICAgICAgICAgICAgIDwvc2RvOm5wYUluZm8+CiAgICAgICAgICAgICAgICA8c2RvOnNlcnZpY2VCZWdpbkRhdGU+e3tEYXRlfX08L3NkbzpzZXJ2aWNlQmVnaW5EYXRlPgogICAgICAgICAgICAgICAgPHNkbzpjb250cmFjdExlbmd0aD4wPC9zZG86Y29udHJhY3RMZW5ndGg+CiAgICAgICAgICAgICAgICA8c2RvOnNlbGVjdGVkU2VydmljZT4KICAgICAgICAgICAgICAgICAgICA8c2RvOnNvYz5KVU1QMjwvc2RvOnNvYz4KICAgICAgICAgICAgICAgICAgICA8c2RvOmxldmVsPlNVQlNDUklCRVI8L3NkbzpsZXZlbD4KICAgICAgICAgICAgICAgICAgICA8c2RvOnByb2R1Y3RUeXBlPkdTTTwvc2RvOnByb2R1Y3RUeXBlPgogICAgICAgICAgICAgICAgPC9zZG86c2VsZWN0ZWRTZXJ2aWNlPgogICAgICAgICAgICAgICAgPHNkbzpwcm9kdWN0VHlwZT5HU008L3Nkbzpwcm9kdWN0VHlwZT4KICAgICAgICAgICAgICAgIDxzZG86c2ltPnt7U0lNXzF9fTwvc2RvOnNpbT4KICAgICAgICAgICAgICAgIDxzZG86aW1laT57e0lNRUlfMX19PC9zZG86aW1laT4KICAgICAgICAgICAgPC9zZG86bGluZURldGFpbHM+CiAgICAgICAgICAgIDxzZG86YmlsbExhbmd1YWdlT3B0aW9uPkVOR0xJU0g8L3NkbzpiaWxsTGFuZ3VhZ2VPcHRpb24+CiAgICAgICAgPC9zZG86YWN0aXZhdGlvblJlcXVlc3Q+CiAgICA8L3NvYXBlbnY6Qm9keT4KPC9zb2FwZW52OkVudmVsb3BlPg==</request>
<style>document</style>
<use>literal</use>
<soapAction>activateSubscribers</soapAction>
<sslInfo>
<ssl-keystore-password-enc>f5504e2d23a7888253a27e8ef52607d8</ssl-keystore-password-enc>
<ssl-key-password-enc>f5504e2d23a7888253a27e8ef52607d8</ssl-key-password-enc>
</sslInfo>
<wsiInfo>
<wsi-check-wsdl>false</wsi-check-wsdl>
<wsi-check-msg>false</wsi-check-msg>
<wsi-display-type>onlyFailed</wsi-display-type>
<wsi-on-fail>continue</wsi-on-fail>
</wsiInfo>
<transport>
<soapVersionUri>1.1</soapVersionUri>
<httpVersionUri>1.1</httpVersionUri>
<webMethodUri>POST</webMethodUri>
<mtom>false</mtom>
<dontSendRequest>false</dontSendRequest>
<fault-as-error>true</fault-as-error>
<callTimeout>30000</callTimeout>
</transport>
<uddiActive>false</uddiActive>
<uddi-result>
<uddi-selection>
</uddi-selection>
</uddi-result>
<wss4jInfo>
<version>5</version>
<wssecurity>
<isSend>true</isSend>
<must>false</must>
<usernameToken>
<enabled>true</enabled>
<username>BESTBUY01</username>
<password>@Password1</password>
<type>PasswordText</type>
<signed>false</signed>
<nonce>true</nonce>
<craeted>true</craeted>
<useMsTimestamp>true</useMsTimestamp>
</usernameToken>
</wssecurity>
<wsi-bsp>false</wsi-bsp>
</wss4jInfo>
<wsaInfo>
<useAddressing>false</useAddressing>
<must>false</must>
<to/>
<toOverride>false</toOverride>
<toDefault>true</toDefault>
<from/>
<fromOverride>false</fromOverride>
<fromDefault>true</fromDefault>
<action/>
<actionOverride>false</actionOverride>
<actionDefault>true</actionDefault>
<msgid/>
<msgidOverride>false</msgidOverride>
<msgidDefault>true</msgidDefault>
<replyTo/>
<replyToOverride>false</replyToOverride>
<faultTo/>
<faulttToOverride>false</faulttToOverride>
<addressingVersion>http://www.w3.org/2005/08/addressing</addressingVersion>
</wsaInfo>
<customHTTPHeaderInfo>
    <Parameter>
    <key>username</key>
    <value>BESTBUY01</value>
    </Parameter>
    <Parameter>
    <key>password</key>
    <value>@Password1</value>
    </Parameter>
</customHTTPHeaderInfo>
<attachments>
<attachment-type>MIME</attachment-type>
</attachments>
<mtom-paths>
</mtom-paths>
    </Node>


    <Node log="" name="CreateInstallmentPlan_1" next="CreateInstallmentPlan_2" quiet="false" think="500-1S" type="com.itko.lisa.ws.nx.NxWSStep" uid="8A40ED02529E11E6A791082820524153" useFilters="true" version="1"> 


      <!-- Filters -->
      <Filter type="com.itko.lisa.xml.FilterXMLXPath">
        <valueToFilterKey>lisa.CreateInstallmentPlan_1.rsp</valueToFilterKey>
<prop>INSTALLMENTPLANID_1</prop>
<xpathq>/SOAP-ENV:Envelope/SOAP-ENV:Body/ns0:esiCreateInstallmentPlanResponse/ns0:installmentPlanId/text()</xpathq>
<nsMap0>SOAP-ENV=http://schemas.xmlsoap.org/soap/envelope/</nsMap0>
<nsMap1>ns0=http://www.t-mobile.com/esi/installment/createinstallmentplan</nsMap1>
<nsMap2>ns1=http://www.t-mobile.com/esi/base</nsMap2>
      </Filter>

<wsdl>{{LISA_RELATIVE_PROJ_URL}}/Data/wsdls/Installment_1.wsdl</wsdl>
<endpoint>https://QATTBCO522.unix.gsm1900.org:8116/Enterprise/Installment</endpoint>
<targetNamespace>http://www.t-mobile.com/es/Installment</targetNamespace>
<service>Installment</service>
<port>Installment</port>
<operation>CreateInstallmentPlan</operation>
<onError>abort</onError>
<maintainSession>true</maintainSession>
<clearSession>false</clearSession>
<request itko_enc="base64">PHNvYXBlbnY6RW52ZWxvcGUgeG1sbnM6c29hcGVudj0iaHR0cDovL3NjaGVtYXMueG1sc29hcC5vcmcvc29hcC9lbnZlbG9wZS8iIHhtbG5zOmNyZT0iaHR0cDovL3d3dy50LW1vYmlsZS5jb20vZXNpL2luc3RhbGxtZW50L2NyZWF0ZWluc3RhbGxtZW50cGxhbiIgeG1sbnM6YmFzZT0iaHR0cDovL3d3dy50LW1vYmlsZS5jb20vZXNpL2Jhc2UiIHhtbG5zOmJhc2UxPSJodHRwOi8vd3d3LnQtbW9iaWxlLmNvbS9lby9zdWJzY3JpYmVyL2Jhc2UiIHhtbG5zOmlucz0iaHR0cDovL3d3dy50LW1vYmlsZS5jb20vZW8vaW5zdGFsbG1lbnQiIHhtbG5zOmNyZWQ9Imh0dHA6Ly93d3cudC1tb2JpbGUuY29tL2VvL2FjY291bnQvY3JlZGl0cHJvZmlsZSI+CiAgICA8c29hcGVudjpIZWFkZXIvPgogICAgPHNvYXBlbnY6Qm9keT4KICAgICAgICA8Y3JlOmVzaUNyZWF0ZUluc3RhbGxtZW50UGxhblJlcXVlc3Q+PCEtLU9wdGlvbmFsOi0tPgogICAgICAgICAgICA8YmFzZTpyZXF1ZXN0SGVhZGVyPgogICAgICAgICAgICAgICAgPGJhc2U6YXBwbGljYXRpb25JZD5QT1M8L2Jhc2U6YXBwbGljYXRpb25JZD4KICAgICAgICAgICAgICAgIDxiYXNlOmNoYW5uZWxJZD4yMDAwPC9iYXNlOmNoYW5uZWxJZD4KICAgICAgICAgICAgICAgIDxiYXNlOm9wZXJhdG9ySWQ+YmV0YW1uZ3I8L2Jhc2U6b3BlcmF0b3JJZD4KICAgICAgICAgICAgPC9iYXNlOnJlcXVlc3RIZWFkZXI+PCEtLU9wdGlvbmFsOi0tPgogICAgICAgICAgICA8Y3JlOkJBTj57e0JBTn19PC9jcmU6QkFOPgogICAgICAgICAgICA8Y3JlOmFjY291bnRUeXBlPkk8L2NyZTphY2NvdW50VHlwZT4KICAgICAgICAgICAgPGNyZTpjcmVkaXRDbGFzcz5BPC9jcmU6Y3JlZGl0Q2xhc3M+CiAgICAgICAgICAgIDxjcmU6YmlsbEN5Y2xlQ29kZT4xMTwvY3JlOmJpbGxDeWNsZUNvZGU+CiAgICAgICAgICAgIDxjcmU6Zmlyc3RJbnN0YWxsbWVudD4xMDA8L2NyZTpmaXJzdEluc3RhbGxtZW50PgogICAgICAgICAgICA8Y3JlOnBsYW5UeXBlPk48L2NyZTpwbGFuVHlwZT4KICAgICAgICAgICAgPGNyZTpjaXR5PkFUTEFOQVRBPC9jcmU6Y2l0eT4KICAgICAgICAgICAgPGNyZTpzdGF0ZT5HQTwvY3JlOnN0YXRlPgogICAgICAgICAgICA8Y3JlOnRyYW5zYWN0aW9uSWQ+e3s9W0RERERERERERERERERERERERERERERERERERF19fTwvY3JlOnRyYW5zYWN0aW9uSWQ+CiAgICAgICAgICAgIDxjcmU6dXNlcklkPkJFVEFNTkdSPC9jcmU6dXNlcklkPgogICAgICAgICAgICA8Y3JlOnRpbWVTdGFtcD4yMDEzLTExLTE1VDEzOjIwOjAwLTA1OjAwPC9jcmU6dGltZVN0YW1wPgogICAgICAgICAgICA8Y3JlOnNhbGVzQ2hhbm5lbFR5cGU+UmV0YWlsPC9jcmU6c2FsZXNDaGFubmVsVHlwZT48IS0tMSBvciBtb3JlIHJlcGV0aXRpb25zOi0tPgogICAgICAgICAgICA8Y3JlOmVpcFN1YnNjcmliZXI+CiAgICAgICAgICAgICAgICA8aW5zOk1TSVNETj57e01TSVNETl8xfX08L2luczpNU0lTRE4+CiAgICAgICAgICAgICAgICA8aW5zOnBsYW5UeXBlPk48L2luczpwbGFuVHlwZT4KICAgICAgICAgICAgICAgIDxpbnM6cGxhbkNvbnRyYWN0VGVybT4wPC9pbnM6cGxhbkNvbnRyYWN0VGVybT4KICAgICAgICAgICAgICAgIDxpbnM6c3Vic2lkeVR5cGU+RTwvaW5zOnN1YnNpZHlUeXBlPgogICAgICAgICAgICAgICAgPGluczpzdWJzaWR5RGF0ZT4yMDEzLTExLTE1VDEzOjIwOjAwLTA1OjAwPC9pbnM6c3Vic2lkeURhdGU+CiAgICAgICAgICAgICAgICA8aW5zOmRlcG9zaXRJbmRpY2F0b3I+ZmFsc2U8L2luczpkZXBvc2l0SW5kaWNhdG9yPgogICAgICAgICAgICAgICAgPGluczphY3RpdmF0aW9uRGF0ZT4yMDE2LTA2LTE3VDEzOjIwOjAwLTA1OjAwPC9pbnM6YWN0aXZhdGlvbkRhdGU+CiAgICAgICAgICAgICAgICA8aW5zOm1pZ3JhdGlvbkZlZVRlbnVyZT4tMTwvaW5zOm1pZ3JhdGlvbkZlZVRlbnVyZT4KICAgICAgICAgICAgICAgIDxpbnM6ZWFybHlVcGdyYWRlPmZhbHNlPC9pbnM6ZWFybHlVcGdyYWRlPjwhLS1aZXJvIG9yIG1vcmUgcmVwZXRpdGlvbnM6LS0+CiAgICAgICAgICAgICAgICA8aW5zOnB1cmNoYXNlZEVxdWlwbWVudD4KICAgICAgICAgICAgICAgPGluczpza3U+TUc1NDJMTC9BPC9pbnM6c2t1PgogICAgICAgICAgICAgICA8aW5zOmRlc2NyaXB0aW9uPkFwcGxlIGlQaG9uZSA2IDE2R0IgU3BhY2UgR3JheTwvaW5zOmRlc2NyaXB0aW9uPgogICAgICAgICAgICAgICA8aW5zOmltZWk+e3tJTUVJfX08L2luczppbWVpPgogICAgICAgICAgICAgICA8aW5zOnByaWNlPjQ0OS43NjwvaW5zOnByaWNlPgogICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgPGluczp0cmFuc2FjdGlvblR5cGU+UDwvaW5zOnRyYW5zYWN0aW9uVHlwZT4KICAgICAgICAgICAgICAgPGluczpkb3duUGF5bWVudEFkanVzdG1lbnQ+MDwvaW5zOmRvd25QYXltZW50QWRqdXN0bWVudD4KICAgICAgICAgICAgPC9pbnM6cHVyY2hhc2VkRXF1aXBtZW50PgoKICAgICAgICAgICAgPC9jcmU6ZWlwU3Vic2NyaWJlcj48IS0tT3B0aW9uYWw6LS0+CiAgICAgICAgICAgIDxjcmU6Y3JlZGl0Umlza1Byb2ZpbGU+CiAgICAgICAgICAgICAgICA8Y3JlZDppZGVudGlmaWVyPjE8L2NyZWQ6aWRlbnRpZmllcj4KICAgICAgICAgICAgICAgIDxjcmVkOmFjY291bnRUZW51cmU+MTwvY3JlZDphY2NvdW50VGVudXJlPgogICAgICAgICAgICAgICAgPGNyZWQ6Y3JlZGl0Q2xhc3M+QTwvY3JlZDpjcmVkaXRDbGFzcz48IS0tT3B0aW9uYWw6LS0+CiAgICAgICAgICAgICAgICA8Y3JlZDpiZWhhdmlvclNjb3JlPjA8L2NyZWQ6YmVoYXZpb3JTY29yZT4KICAgICAgICAgICAgICAgIDxjcmVkOmRlY2lzaW9uQ3JpdGVyaWE+Q1JFRElUX0NMQVNTX0JBU0VEPC9jcmVkOmRlY2lzaW9uQ3JpdGVyaWE+CiAgICAgICAgICAgICAgICA8Y3JlZDpjcmVkaXRBcHBsaWNhdGlvblJlZmVyZW5jZU51bWJlcj4wPC9jcmVkOmNyZWRpdEFwcGxpY2F0aW9uUmVmZXJlbmNlTnVtYmVyPgogICAgICAgICAgICA8L2NyZTpjcmVkaXRSaXNrUHJvZmlsZT4KICAgICAgICA8L2NyZTplc2lDcmVhdGVJbnN0YWxsbWVudFBsYW5SZXF1ZXN0PgogICAgPC9zb2FwZW52OkJvZHk+Cjwvc29hcGVudjpFbnZlbG9wZT4=</request>
<style>document</style>
<use>literal</use>
<soapAction>/CreateInstallmentPlan</soapAction>
<sslInfo>
<ssl-keystore-file>C:\Users\DMethuk\ESGDevTest\Data\Cert\keystore.jks</ssl-keystore-file>
<ssl-keystore-password-enc>eea8896d65275dd8fdd5573d6ef40563</ssl-keystore-password-enc>
<ssl-alias>INSTALL3</ssl-alias>
<ssl-key-password-enc>f5504e2d23a7888253a27e8ef52607d8</ssl-key-password-enc>
</sslInfo>
<wsiInfo>
<wsi-check-wsdl>true</wsi-check-wsdl>
<wsi-check-msg>false</wsi-check-msg>
<wsi-display-type>onlyFailed</wsi-display-type>
<wsi-on-fail>continue</wsi-on-fail>
</wsiInfo>
<transport>
<soapVersionUri>1.1</soapVersionUri>
<httpVersionUri>1.1</httpVersionUri>
<webMethodUri>POST</webMethodUri>
<mtom>false</mtom>
<dontSendRequest>false</dontSendRequest>
<fault-as-error>true</fault-as-error>
<callTimeout>30000</callTimeout>
</transport>
<uddiActive>false</uddiActive>
<uddi-result>
<uddi-selection>
</uddi-selection>
</uddi-result>
<wss4jInfo>
<version>5</version>
<wssecurity>
<isSend>true</isSend>
<must>false</must>
</wssecurity>
<wsi-bsp>false</wsi-bsp>
</wss4jInfo>
<wsaInfo>
<useAddressing>false</useAddressing>
<must>false</must>
<to/>
<toOverride>false</toOverride>
<toDefault>true</toDefault>
<from/>
<fromOverride>false</fromOverride>
<fromDefault>true</fromDefault>
<action/>
<actionOverride>false</actionOverride>
<actionDefault>true</actionDefault>
<msgid/>
<msgidOverride>false</msgidOverride>
<msgidDefault>true</msgidDefault>
<replyTo/>
<replyToOverride>false</replyToOverride>
<faultTo/>
<faulttToOverride>false</faulttToOverride>
<addressingVersion>http://www.w3.org/2005/08/addressing</addressingVersion>
</wsaInfo>
<customHTTPHeaderInfo>
    <Parameter>
    <key>username</key>
    <value>BESTBUY01</value>
    </Parameter>
    <Parameter>
    <key>password</key>
    <value>@Password1</value>
    </Parameter>
</customHTTPHeaderInfo>
<attachments>
<attachment-type>MIME</attachment-type>
</attachments>
<mtom-paths>
</mtom-paths>
    </Node>


    <Node log="" name="CreateInstallmentPlan_2" next="EquipmentID_!" quiet="false" think="500-1S" type="com.itko.lisa.ws.nx.NxWSStep" uid="8A40ED03529E11E6A791082820524153" useFilters="true" version="1"> 


      <!-- Filters -->
      <Filter type="com.itko.lisa.xml.FilterXMLXPath">
        <valueToFilterKey>lisa.CreateInstallmentPlan_2.rsp</valueToFilterKey>
<prop>INSTALLMENTPLANID_2</prop>
<xpathq>/SOAP-ENV:Envelope/SOAP-ENV:Body/ns0:esiCreateInstallmentPlanResponse/ns0:installmentPlanId/text()</xpathq>
      </Filter>

<wsdl>{{LISA_RELATIVE_PROJ_URL}}/Data/wsdls/Installment_1.wsdl</wsdl>
<endpoint>https://QATTBCO522.unix.gsm1900.org:8116/Enterprise/Installment</endpoint>
<targetNamespace>http://www.t-mobile.com/es/Installment</targetNamespace>
<service>Installment</service>
<port>Installment</port>
<operation>CreateInstallmentPlan</operation>
<onError>abort</onError>
<maintainSession>true</maintainSession>
<clearSession>false</clearSession>
<request itko_enc="base64">PHNvYXBlbnY6RW52ZWxvcGUgeG1sbnM6c29hcGVudj0iaHR0cDovL3NjaGVtYXMueG1sc29hcC5vcmcvc29hcC9lbnZlbG9wZS8iIHhtbG5zOmNyZT0iaHR0cDovL3d3dy50LW1vYmlsZS5jb20vZXNpL2luc3RhbGxtZW50L2NyZWF0ZWluc3RhbGxtZW50cGxhbiIgeG1sbnM6YmFzZT0iaHR0cDovL3d3dy50LW1vYmlsZS5jb20vZXNpL2Jhc2UiIHhtbG5zOmJhc2UxPSJodHRwOi8vd3d3LnQtbW9iaWxlLmNvbS9lby9zdWJzY3JpYmVyL2Jhc2UiIHhtbG5zOmlucz0iaHR0cDovL3d3dy50LW1vYmlsZS5jb20vZW8vaW5zdGFsbG1lbnQiIHhtbG5zOmNyZWQ9Imh0dHA6Ly93d3cudC1tb2JpbGUuY29tL2VvL2FjY291bnQvY3JlZGl0cHJvZmlsZSI+CiAgICA8c29hcGVudjpIZWFkZXIvPgogICAgPHNvYXBlbnY6Qm9keT4KICAgICAgICA8Y3JlOmVzaUNyZWF0ZUluc3RhbGxtZW50UGxhblJlcXVlc3Q+PCEtLU9wdGlvbmFsOi0tPgogICAgICAgICAgICA8YmFzZTpyZXF1ZXN0SGVhZGVyPgogICAgICAgICAgICAgICAgPGJhc2U6YXBwbGljYXRpb25JZD5QT1M8L2Jhc2U6YXBwbGljYXRpb25JZD4KICAgICAgICAgICAgICAgIDxiYXNlOmNoYW5uZWxJZD4yMDAwPC9iYXNlOmNoYW5uZWxJZD4KICAgICAgICAgICAgICAgIDxiYXNlOm9wZXJhdG9ySWQ+YmV0YW1uZ3I8L2Jhc2U6b3BlcmF0b3JJZD4KICAgICAgICAgICAgPC9iYXNlOnJlcXVlc3RIZWFkZXI+PCEtLU9wdGlvbmFsOi0tPgogICAgICAgICAgICA8Y3JlOkJBTj57e0JBTn19PC9jcmU6QkFOPgogICAgICAgICAgICA8Y3JlOmFjY291bnRUeXBlPkk8L2NyZTphY2NvdW50VHlwZT4KICAgICAgICAgICAgPGNyZTpjcmVkaXRDbGFzcz5BPC9jcmU6Y3JlZGl0Q2xhc3M+CiAgICAgICAgICAgIDxjcmU6YmlsbEN5Y2xlQ29kZT4xMTwvY3JlOmJpbGxDeWNsZUNvZGU+CiAgICAgICAgICAgIDxjcmU6Zmlyc3RJbnN0YWxsbWVudD4xMDA8L2NyZTpmaXJzdEluc3RhbGxtZW50PgogICAgICAgICAgICA8Y3JlOnBsYW5UeXBlPk48L2NyZTpwbGFuVHlwZT4KICAgICAgICAgICAgPGNyZTpjaXR5PkFUTEFOQVRBPC9jcmU6Y2l0eT4KICAgICAgICAgICAgPGNyZTpzdGF0ZT5HQTwvY3JlOnN0YXRlPgogICAgICAgICAgICA8Y3JlOnRyYW5zYWN0aW9uSWQ+e3s9W0RERERERERERERERERERERERERERERERERERF19fTwvY3JlOnRyYW5zYWN0aW9uSWQ+CiAgICAgICAgICAgIDxjcmU6dXNlcklkPkJFVEFNTkdSPC9jcmU6dXNlcklkPgogICAgICAgICAgICA8Y3JlOnRpbWVTdGFtcD4yMDEzLTExLTE1VDEzOjIwOjAwLTA1OjAwPC9jcmU6dGltZVN0YW1wPgogICAgICAgICAgICA8Y3JlOnNhbGVzQ2hhbm5lbFR5cGU+UmV0YWlsPC9jcmU6c2FsZXNDaGFubmVsVHlwZT48IS0tMSBvciBtb3JlIHJlcGV0aXRpb25zOi0tPgogICAgICAgICAgICA8Y3JlOmVpcFN1YnNjcmliZXI+CiAgICAgICAgICAgICAgICA8aW5zOk1TSVNETj57e01TSVNETl8yfX08L2luczpNU0lTRE4+CiAgICAgICAgICAgICAgICA8aW5zOnBsYW5UeXBlPk48L2luczpwbGFuVHlwZT4KICAgICAgICAgICAgICAgIDxpbnM6cGxhbkNvbnRyYWN0VGVybT4wPC9pbnM6cGxhbkNvbnRyYWN0VGVybT4KICAgICAgICAgICAgICAgIDxpbnM6c3Vic2lkeVR5cGU+RTwvaW5zOnN1YnNpZHlUeXBlPgogICAgICAgICAgICAgICAgPGluczpzdWJzaWR5RGF0ZT4yMDEzLTExLTE1VDEzOjIwOjAwLTA1OjAwPC9pbnM6c3Vic2lkeURhdGU+CiAgICAgICAgICAgICAgICA8aW5zOmRlcG9zaXRJbmRpY2F0b3I+ZmFsc2U8L2luczpkZXBvc2l0SW5kaWNhdG9yPgogICAgICAgICAgICAgICAgPGluczphY3RpdmF0aW9uRGF0ZT4yMDE2LTA2LTE3VDEzOjIwOjAwLTA1OjAwPC9pbnM6YWN0aXZhdGlvbkRhdGU+CiAgICAgICAgICAgICAgICA8aW5zOm1pZ3JhdGlvbkZlZVRlbnVyZT4tMTwvaW5zOm1pZ3JhdGlvbkZlZVRlbnVyZT4KICAgICAgICAgICAgICAgIDxpbnM6ZWFybHlVcGdyYWRlPmZhbHNlPC9pbnM6ZWFybHlVcGdyYWRlPjwhLS1aZXJvIG9yIG1vcmUgcmVwZXRpdGlvbnM6LS0+CiAgICAgICAgICAgICAgICA8aW5zOnB1cmNoYXNlZEVxdWlwbWVudD4KICAgICAgICAgICAgICAgICAgICA8aW5zOnNrdT5NRzU0MkxML0E8L2luczpza3U+CiAgICAgICAgICAgICAgICAgICAgPGluczpkZXNjcmlwdGlvbj5BcHBsZSBpUGhvbmUgNiAxNkdCIFNwYWNlIEdyYXk8L2luczpkZXNjcmlwdGlvbj4KICAgICAgICAgICAgICAgICAgICA8aW5zOmltZWk+e3tJTUVJXzF9fTwvaW5zOmltZWk+CiAgICAgICAgICAgICAgICAgICAgPGluczpwcmljZT40NDkuNzY8L2luczpwcmljZT4KICAgICAgICAgICAgICAgICAgICA8aW5zOnRyYW5zYWN0aW9uVHlwZT5QPC9pbnM6dHJhbnNhY3Rpb25UeXBlPgogICAgICAgICAgICAgICAgICAgIDxpbnM6ZG93blBheW1lbnRBZGp1c3RtZW50PjA8L2luczpkb3duUGF5bWVudEFkanVzdG1lbnQ+CiAgICAgICAgICAgICAgICA8L2luczpwdXJjaGFzZWRFcXVpcG1lbnQ+CiAgICAgICAgICAgIDwvY3JlOmVpcFN1YnNjcmliZXI+PCEtLU9wdGlvbmFsOi0tPgogICAgICAgICAgICA8Y3JlOmNyZWRpdFJpc2tQcm9maWxlPgogICAgICAgICAgICAgICAgPGNyZWQ6aWRlbnRpZmllcj4xPC9jcmVkOmlkZW50aWZpZXI+CiAgICAgICAgICAgICAgICA8Y3JlZDphY2NvdW50VGVudXJlPjE8L2NyZWQ6YWNjb3VudFRlbnVyZT4KICAgICAgICAgICAgICAgIDxjcmVkOmNyZWRpdENsYXNzPkE8L2NyZWQ6Y3JlZGl0Q2xhc3M+PCEtLU9wdGlvbmFsOi0tPgogICAgICAgICAgICAgICAgPGNyZWQ6YmVoYXZpb3JTY29yZT4wPC9jcmVkOmJlaGF2aW9yU2NvcmU+CiAgICAgICAgICAgICAgICA8Y3JlZDpkZWNpc2lvbkNyaXRlcmlhPkNSRURJVF9DTEFTU19CQVNFRDwvY3JlZDpkZWNpc2lvbkNyaXRlcmlhPgogICAgICAgICAgICAgICAgPGNyZWQ6Y3JlZGl0QXBwbGljYXRpb25SZWZlcmVuY2VOdW1iZXI+MDwvY3JlZDpjcmVkaXRBcHBsaWNhdGlvblJlZmVyZW5jZU51bWJlcj4KICAgICAgICAgICAgPC9jcmU6Y3JlZGl0Umlza1Byb2ZpbGU+CiAgICAgICAgPC9jcmU6ZXNpQ3JlYXRlSW5zdGFsbG1lbnRQbGFuUmVxdWVzdD4KICAgIDwvc29hcGVudjpCb2R5Pgo8L3NvYXBlbnY6RW52ZWxvcGU+</request>
<style>document</style>
<use>literal</use>
<soapAction>/CreateInstallmentPlan</soapAction>
<sslInfo>
<ssl-keystore-password-enc>f5504e2d23a7888253a27e8ef52607d8</ssl-keystore-password-enc>
<ssl-key-password-enc>f5504e2d23a7888253a27e8ef52607d8</ssl-key-password-enc>
</sslInfo>
<wsiInfo>
<wsi-check-wsdl>false</wsi-check-wsdl>
<wsi-check-msg>false</wsi-check-msg>
<wsi-display-type>onlyFailed</wsi-display-type>
<wsi-on-fail>continue</wsi-on-fail>
</wsiInfo>
<transport>
<soapVersionUri>1.1</soapVersionUri>
<httpVersionUri>1.1</httpVersionUri>
<webMethodUri>POST</webMethodUri>
<mtom>false</mtom>
<dontSendRequest>false</dontSendRequest>
<fault-as-error>true</fault-as-error>
<callTimeout>30000</callTimeout>
</transport>
<uddiActive>false</uddiActive>
<uddi-result>
<uddi-selection>
</uddi-selection>
</uddi-result>
<wss4jInfo>
<version>5</version>
<wssecurity>
<isSend>true</isSend>
<must>false</must>
<usernameToken>
<enabled>true</enabled>
<username>BESTBUY01</username>
<password>@Password1</password>
<type>PasswordText</type>
<signed>false</signed>
<nonce>true</nonce>
<craeted>true</craeted>
<useMsTimestamp>true</useMsTimestamp>
</usernameToken>
</wssecurity>
<wsi-bsp>false</wsi-bsp>
</wss4jInfo>
<wsaInfo>
<useAddressing>false</useAddressing>
<must>false</must>
<to/>
<toOverride>false</toOverride>
<toDefault>true</toDefault>
<from/>
<fromOverride>false</fromOverride>
<fromDefault>true</fromDefault>
<action/>
<actionOverride>false</actionOverride>
<actionDefault>true</actionDefault>
<msgid/>
<msgidOverride>false</msgidOverride>
<msgidDefault>true</msgidDefault>
<replyTo/>
<replyToOverride>false</replyToOverride>
<faultTo/>
<faulttToOverride>false</faulttToOverride>
<addressingVersion>http://www.w3.org/2005/08/addressing</addressingVersion>
</wsaInfo>
<customHTTPHeaderInfo>
    <Parameter>
    <key>username</key>
    <value>BESTBUY01</value>
    </Parameter>
    <Parameter>
    <key>password</key>
    <value>@Password1</value>
    </Parameter>
</customHTTPHeaderInfo>
<attachments>
<attachment-type>MIME</attachment-type>
</attachments>
<mtom-paths>
</mtom-paths>
    </Node>


    <Node log="" name="EquipmentID_!" next="EquipmentID_2" quiet="false" think="500-1S" type="com.itko.lisa.jdbc.JDBCNode" uid="8A411414529E11E6A791082820524153" useFilters="true" version="1"> 


      <!-- Filters -->
      <Filter type="com.itko.lisa.jdbc.JDBCValueFilter">
        <valueToFilterKey>lisa.EquipmentID_!.rsp</valueToFilterKey>
      <col>1</col>
      <row>0</row>
      <prop>EQUIPMENTID_1</prop>
      </Filter>

<driver>oracle.jdbc.driver.OracleDriver</driver>
<dataSourceConnect>false</dataSourceConnect>
<jndiFactory/>
<jndiServerURL/>
<jndiDataSourceName/>
<connect>jdbc:oracle:thin:@QEIPP2.UNIX.GSM1900.ORG:7281:QEIPP2</connect>
<user>EIP_TEST01</user>
<password_enc>96b9fd85f12a9fc92395254c626a8a76</password_enc>
<onSQLError>abort</onSQLError>
<resultSet>true</resultSet>
<maxRows>-1</maxRows>
<keepOpen>false</keepOpen>
<usePool>true</usePool>
<sql>select  EIP_EQUIPMENT_ID from eip_app01.financial_activity where EIP_plan_ID='{{INSTALLMENTPLANID_1}}'</sql>
<IsStoredProc>false</IsStoredProc>
    </Node>


    <Node log="" name="EquipmentID_2" next="makePayment_1" quiet="false" think="500-1S" type="com.itko.lisa.jdbc.JDBCNode" uid="8A411415529E11E6A791082820524153" useFilters="true" version="1"> 


      <!-- Filters -->
      <Filter type="com.itko.lisa.jdbc.JDBCValueFilter">
        <valueToFilterKey>lisa.EquipmentID_2.rsp</valueToFilterKey>
      <col>1</col>
      <row>0</row>
      <prop>EQUIPMENTID_2</prop>
      </Filter>

<driver>oracle.jdbc.driver.OracleDriver</driver>
<dataSourceConnect>false</dataSourceConnect>
<jndiFactory/>
<jndiServerURL/>
<jndiDataSourceName/>
<connect>jdbc:oracle:thin:@QEIPP2.UNIX.GSM1900.ORG:7281:QEIPP2</connect>
<user>EIP_TEST01</user>
<password_enc>96b9fd85f12a9fc92395254c626a8a76</password_enc>
<onSQLError>abort</onSQLError>
<resultSet>true</resultSet>
<maxRows>-1</maxRows>
<keepOpen>false</keepOpen>
<usePool>true</usePool>
<sql>select  EIP_EQUIPMENT_ID from eip_app01.financial_activity where EIP_plan_ID='{{INSTALLMENTPLANID_2}}'</sql>
<IsStoredProc>false</IsStoredProc>
    </Node>


    <Node log="" name="makePayment_1" next="makePayment_2" quiet="false" think="500-1S" type="com.itko.lisa.ws.nx.NxWSStep" uid="8A411416529E11E6A791082820524153" useFilters="true" version="1"> 

<wsdl>{{LISA_RELATIVE_PROJ_URL}}/Data/wsdls/InstallmentAccountPayment_1.wsdl</wsdl>
<endpoint>http://qateipx522.unix.gsm1900.org:7001/tmobile-eip/InstallmentAccountPayment</endpoint>
<targetNamespace>http://com.tmobile.eip</targetNamespace>
<service>InstallmentAccountPayment</service>
<port>InstallmentAccountPaymentPort</port>
<operation>makePayment</operation>
<onError>abort</onError>
<maintainSession>true</maintainSession>
<clearSession>false</clearSession>
<request itko_enc="base64">PHNvYXBlbnY6RW52ZWxvcGUgeG1sbnM6c29hcGVudj0iaHR0cDovL3NjaGVtYXMueG1sc29hcC5vcmcvc29hcC9lbnZlbG9wZS8iIHhtbG5zOmNvbT0iaHR0cDovL2NvbS50bW9iaWxlLmVpcCI+CiAgICA8c29hcGVudjpIZWFkZXIvPgogICAgPHNvYXBlbnY6Qm9keT4KICAgICAgICA8Y29tOm1ha2VQYXltZW50PjwhLS1PcHRpb25hbDotLT4KICAgICAgICAgICAgPGNvbTpwYXltZW50UmVxdWVzdERUTz4KICAgICAgICAgICAgICAgIDxiYW4+e3tCQU59fTwvYmFuPjwhLS0xIG9yIG1vcmUgcmVwZXRpdGlvbnM6LS0+CiAgICAgICAgICAgICAgICA8cGF5bWVudFBsYW5BbW91bnRzPgogICAgICAgICAgICAgICAgICAgIDxpbnN0YWxsbWVudFBsYW5JZD57e0lOU1RBTExNRU5UUExBTklEXzF9fTwvaW5zdGFsbG1lbnRQbGFuSWQ+PCEtLU9wdGlvbmFsOi0tPgogICAgICAgICAgICAgICAgICAgIDxlaXBFcXVpcG1lbnRJZD57e0VRVUlQTUVOVElEXzF9fTwvZWlwRXF1aXBtZW50SWQ+CiAgICAgICAgICAgICAgICAgICAgPHBheW1lbnRBbW91bnQ+MzI0Ljc2PC9wYXltZW50QW1vdW50PgogICAgICAgICAgICAgICAgPC9wYXltZW50UGxhbkFtb3VudHM+CiAgICAgICAgICAgICAgICA8cGF5bWVudFR5cGU+UDwvcGF5bWVudFR5cGU+CiAgICAgICAgICAgICAgICA8YXBwbGljYXRpb25Vc2VySWQ+RUlQVVNFUjwvYXBwbGljYXRpb25Vc2VySWQ+CiAgICAgICAgICAgICAgICA8YXBwbGljYXRpb25JZD5QT1M8L2FwcGxpY2F0aW9uSWQ+CiAgICAgICAgICAgICAgICA8ZW1haWw+cmFjaGFuYS5jZEB0LW1vYmlsZS5jb208L2VtYWlsPgogICAgICAgICAgICA8L2NvbTpwYXltZW50UmVxdWVzdERUTz4KICAgICAgICA8L2NvbTptYWtlUGF5bWVudD4KICAgIDwvc29hcGVudjpCb2R5Pgo8L3NvYXBlbnY6RW52ZWxvcGU+</request>
<style>document</style>
<use>literal</use>
<sslInfo>
<ssl-keystore-password-enc>f5504e2d23a7888253a27e8ef52607d8</ssl-keystore-password-enc>
<ssl-key-password-enc>f5504e2d23a7888253a27e8ef52607d8</ssl-key-password-enc>
</sslInfo>
<wsiInfo>
<wsi-check-wsdl>false</wsi-check-wsdl>
<wsi-check-msg>false</wsi-check-msg>
<wsi-display-type>onlyFailed</wsi-display-type>
<wsi-on-fail>continue</wsi-on-fail>
</wsiInfo>
<transport>
<soapVersionUri>1.1</soapVersionUri>
<httpVersionUri>1.1</httpVersionUri>
<webMethodUri>POST</webMethodUri>
<mtom>false</mtom>
<dontSendRequest>false</dontSendRequest>
<fault-as-error>true</fault-as-error>
<callTimeout>30000</callTimeout>
</transport>
<uddiActive>false</uddiActive>
<uddi-result>
<uddi-selection>
</uddi-selection>
</uddi-result>
<wss4jInfo>
<version>5</version>
<wssecurity>
<isSend>true</isSend>
<must>false</must>
<usernameToken>
<enabled>true</enabled>
<username>BESTBUY01</username>
<password>@Password1</password>
<type>PasswordText</type>
<signed>false</signed>
<nonce>true</nonce>
<craeted>true</craeted>
<useMsTimestamp>true</useMsTimestamp>
</usernameToken>
</wssecurity>
<wsi-bsp>false</wsi-bsp>
</wss4jInfo>
<wsaInfo>
<useAddressing>false</useAddressing>
<must>false</must>
<to/>
<toOverride>false</toOverride>
<toDefault>true</toDefault>
<from/>
<fromOverride>false</fromOverride>
<fromDefault>true</fromDefault>
<action/>
<actionOverride>false</actionOverride>
<actionDefault>true</actionDefault>
<msgid/>
<msgidOverride>false</msgidOverride>
<msgidDefault>true</msgidDefault>
<replyTo/>
<replyToOverride>false</replyToOverride>
<faultTo/>
<faulttToOverride>false</faulttToOverride>
<addressingVersion>http://www.w3.org/2005/08/addressing</addressingVersion>
</wsaInfo>
<customHTTPHeaderInfo>
    <Parameter>
    <key>username</key>
    <value>BESTBUY01</value>
    </Parameter>
    <Parameter>
    <key>password</key>
    <value>@Password1</value>
    </Parameter>
</customHTTPHeaderInfo>
<attachments>
<attachment-type>MIME</attachment-type>
</attachments>
<mtom-paths>
</mtom-paths>
    </Node>


    <Node log="" name="makePayment_2" next="resyncAccount" quiet="false" think="500-1S" type="com.itko.lisa.ws.nx.NxWSStep" uid="8A411417529E11E6A791082820524153" useFilters="true" version="1"> 

<wsdl>{{LISA_RELATIVE_PROJ_URL}}/Data/wsdls/InstallmentAccountPayment_1.wsdl</wsdl>
<endpoint>http://qateipx522.unix.gsm1900.org:7001/tmobile-eip/InstallmentAccountPayment</endpoint>
<targetNamespace>http://com.tmobile.eip</targetNamespace>
<service>InstallmentAccountPayment</service>
<port>InstallmentAccountPaymentPort</port>
<operation>makePayment</operation>
<onError>abort</onError>
<maintainSession>true</maintainSession>
<clearSession>false</clearSession>
<request itko_enc="base64">PHNvYXBlbnY6RW52ZWxvcGUgeG1sbnM6c29hcGVudj0iaHR0cDovL3NjaGVtYXMueG1sc29hcC5vcmcvc29hcC9lbnZlbG9wZS8iIHhtbG5zOmNvbT0iaHR0cDovL2NvbS50bW9iaWxlLmVpcCI+CiAgICA8c29hcGVudjpIZWFkZXIvPgogICAgPHNvYXBlbnY6Qm9keT4KICAgICAgICA8Y29tOm1ha2VQYXltZW50PjwhLS1PcHRpb25hbDotLT4KICAgICAgICAgICAgPGNvbTpwYXltZW50UmVxdWVzdERUTz4KICAgICAgICAgICAgICAgIDxiYW4+e3tCQU59fTwvYmFuPjwhLS0xIG9yIG1vcmUgcmVwZXRpdGlvbnM6LS0+CiAgICAgICAgICAgICAgICA8cGF5bWVudFBsYW5BbW91bnRzPgogICAgICAgICAgICAgICAgICAgIDxpbnN0YWxsbWVudFBsYW5JZD57e0lOU1RBTExNRU5UUExBTklEXzJ9fTwvaW5zdGFsbG1lbnRQbGFuSWQ+PCEtLU9wdGlvbmFsOi0tPgogICAgICAgICAgICAgICAgICAgIDxlaXBFcXVpcG1lbnRJZD57e0VRVUlQTUVOVElEXzJ9fTwvZWlwRXF1aXBtZW50SWQ+CiAgICAgICAgICAgICAgICAgICAgPHBheW1lbnRBbW91bnQ+MzI0Ljc2PC9wYXltZW50QW1vdW50PgogICAgICAgICAgICAgICAgPC9wYXltZW50UGxhbkFtb3VudHM+CiAgICAgICAgICAgICAgICA8cGF5bWVudFR5cGU+UDwvcGF5bWVudFR5cGU+CiAgICAgICAgICAgICAgICA8YXBwbGljYXRpb25Vc2VySWQ+RUlQVVNFUjwvYXBwbGljYXRpb25Vc2VySWQ+CiAgICAgICAgICAgICAgICA8YXBwbGljYXRpb25JZD5QT1M8L2FwcGxpY2F0aW9uSWQ+CiAgICAgICAgICAgICAgICA8ZW1haWw+cmFjaGFuYS5jZEB0LW1vYmlsZS5jb208L2VtYWlsPgogICAgICAgICAgICA8L2NvbTpwYXltZW50UmVxdWVzdERUTz4KICAgICAgICA8L2NvbTptYWtlUGF5bWVudD4KICAgIDwvc29hcGVudjpCb2R5Pgo8L3NvYXBlbnY6RW52ZWxvcGU+</request>
<style>document</style>
<use>literal</use>
<soapAction/>
<sslInfo>
<ssl-keystore-password-enc>f5504e2d23a7888253a27e8ef52607d8</ssl-keystore-password-enc>
<ssl-key-password-enc>f5504e2d23a7888253a27e8ef52607d8</ssl-key-password-enc>
</sslInfo>
<wsiInfo>
<wsi-check-wsdl>false</wsi-check-wsdl>
<wsi-check-msg>false</wsi-check-msg>
<wsi-display-type>onlyFailed</wsi-display-type>
<wsi-on-fail>continue</wsi-on-fail>
</wsiInfo>
<transport>
<soapVersionUri>1.1</soapVersionUri>
<httpVersionUri>1.1</httpVersionUri>
<webMethodUri>POST</webMethodUri>
<mtom>false</mtom>
<dontSendRequest>false</dontSendRequest>
<fault-as-error>true</fault-as-error>
<callTimeout>30000</callTimeout>
</transport>
<uddiActive>false</uddiActive>
<uddi-result>
<uddi-selection>
</uddi-selection>
</uddi-result>
<wss4jInfo>
<version>5</version>
<wssecurity>
<isSend>true</isSend>
<must>false</must>
<usernameToken>
<enabled>true</enabled>
<username>BESTBUY01</username>
<password>@Password1</password>
<type>PasswordText</type>
<signed>false</signed>
<nonce>true</nonce>
<craeted>true</craeted>
<useMsTimestamp>true</useMsTimestamp>
</usernameToken>
</wssecurity>
<wsi-bsp>false</wsi-bsp>
</wss4jInfo>
<wsaInfo>
<useAddressing>false</useAddressing>
<must>false</must>
<to/>
<toOverride>false</toOverride>
<toDefault>true</toDefault>
<from/>
<fromOverride>false</fromOverride>
<fromDefault>true</fromDefault>
<action/>
<actionOverride>false</actionOverride>
<actionDefault>true</actionDefault>
<msgid/>
<msgidOverride>false</msgidOverride>
<msgidDefault>true</msgidDefault>
<replyTo/>
<replyToOverride>false</replyToOverride>
<faultTo/>
<faulttToOverride>false</faulttToOverride>
<addressingVersion>http://www.w3.org/2005/08/addressing</addressingVersion>
</wsaInfo>
<customHTTPHeaderInfo>
    <Parameter>
    <key>username</key>
    <value>BESTBUY01</value>
    </Parameter>
    <Parameter>
    <key>password</key>
    <value>@Password1</value>
    </Parameter>
</customHTTPHeaderInfo>
<attachments>
<attachment-type>MIME</attachment-type>
</attachments>
<mtom-paths>
</mtom-paths>
    </Node>


    <Node log="" name="resyncAccount" next="Write Properties to File JumpData.csv" quiet="false" think="500-1S" type="com.itko.lisa.ws.nx.NxWSStep" uid="8A411418529E11E6A791082820524153" useFilters="true" version="1"> 

<wsdl>{{LISA_RELATIVE_PROJ_URL}}/Data/wsdls/ResyncOnDemand_1.wsdl</wsdl>
<endpoint>http://QATTBCO522.unix.gsm1900.org:8115/CHUB</endpoint>
<targetNamespace>http://services.tmobile.com/resyncondemand</targetNamespace>
<service>ResyncOnDemand</service>
<port>ResyncOnDemandPort</port>
<operation>resyncAccount</operation>
<onError>abort</onError>
<maintainSession>true</maintainSession>
<clearSession>false</clearSession>
<request itko_enc="base64">PHNvYXBlbnY6RW52ZWxvcGUgeG1sbnM6c29hcGVudj0iaHR0cDovL3NjaGVtYXMueG1sc29hcC5vcmcvc29hcC9lbnZlbG9wZS8iIHhtbG5zOnJlcz0iaHR0cDovL3NlcnZpY2VzLnRtb2JpbGUuY29tL2FzaS9yZXN5bmNvbmRlbWFuZCIgeG1sbnM6YmFzZT0iaHR0cDovL3NlcnZpY2VzLnRtb2JpbGUuY29tL2FzaS9iYXNlIj4KICAgIDxzb2FwZW52OkhlYWRlci8+CiAgICA8c29hcGVudjpCb2R5PgogICAgICAgIDxyZXM6UmVzeW5jT25EZW1hbmRSZXF1ZXN0PjwhLS1PcHRpb25hbDotLT4KICAgICAgICAgICAgPGJhc2U6cmVxdWVzdEhlYWRlci8+CiAgICAgICAgICAgIDxyZXM6QkFOPnt7QkFOfX08L3JlczpCQU4+CiAgICAgICAgPC9yZXM6UmVzeW5jT25EZW1hbmRSZXF1ZXN0PgogICAgPC9zb2FwZW52OkJvZHk+Cjwvc29hcGVudjpFbnZlbG9wZT4=</request>
<style>document</style>
<use>literal</use>
<soapAction>/RESYNC/ACCOUNT</soapAction>
<sslInfo>
<ssl-keystore-password-enc>f5504e2d23a7888253a27e8ef52607d8</ssl-keystore-password-enc>
<ssl-key-password-enc>f5504e2d23a7888253a27e8ef52607d8</ssl-key-password-enc>
</sslInfo>
<wsiInfo>
<wsi-check-wsdl>false</wsi-check-wsdl>
<wsi-check-msg>false</wsi-check-msg>
<wsi-display-type>onlyFailed</wsi-display-type>
<wsi-on-fail>continue</wsi-on-fail>
</wsiInfo>
<transport>
<soapVersionUri>1.1</soapVersionUri>
<httpVersionUri>1.1</httpVersionUri>
<webMethodUri>POST</webMethodUri>
<mtom>false</mtom>
<dontSendRequest>false</dontSendRequest>
<fault-as-error>true</fault-as-error>
<callTimeout>30000</callTimeout>
</transport>
<uddiActive>false</uddiActive>
<uddi-result>
<uddi-selection>
</uddi-selection>
</uddi-result>
<wss4jInfo>
<version>5</version>
<wssecurity>
<isSend>true</isSend>
<must>false</must>
<usernameToken>
<enabled>true</enabled>
<username>BESTBUY01</username>
<password>@Password1</password>
<type>PasswordText</type>
<signed>false</signed>
<nonce>true</nonce>
<craeted>true</craeted>
<useMsTimestamp>true</useMsTimestamp>
</usernameToken>
</wssecurity>
<wsi-bsp>false</wsi-bsp>
</wss4jInfo>
<wsaInfo>
<useAddressing>false</useAddressing>
<must>false</must>
<to/>
<toOverride>false</toOverride>
<toDefault>true</toDefault>
<from/>
<fromOverride>false</fromOverride>
<fromDefault>true</fromDefault>
<action/>
<actionOverride>false</actionOverride>
<actionDefault>true</actionDefault>
<msgid/>
<msgidOverride>false</msgidOverride>
<msgidDefault>true</msgidDefault>
<replyTo/>
<replyToOverride>false</replyToOverride>
<faultTo/>
<faulttToOverride>false</faulttToOverride>
<addressingVersion>http://www.w3.org/2005/08/addressing</addressingVersion>
</wsaInfo>
<customHTTPHeaderInfo>
    <Parameter>
    <key>username</key>
    <value>BESTBUY01</value>
    </Parameter>
    <Parameter>
    <key>password</key>
    <value>@Password1</value>
    </Parameter>
</customHTTPHeaderInfo>
<attachments>
<attachment-type>MIME</attachment-type>
</attachments>
<mtom-paths>
</mtom-paths>
    </Node>


    <Node log="" name="Write Properties to File JumpData.csv" next="SSNRetail" quiet="true" think="500-1S" type="com.itko.lisa.utils.WritePropsNode" uid="8A411419529E11E6A791082820524153" useFilters="true" version="1"> 

<file>{{LISA_RELATIVE_PROJ_ROOT}}/Data/JumpData.csv</file>
<encoding>DEFAULT</encoding>
<props>
    <Parameter>
    <key>SSN</key>
    <value>'{{SSN}}</value>
    </Parameter>
    <Parameter>
    <key>SIM</key>
    <value>'{{SIM}}</value>
    </Parameter>
    <Parameter>
    <key>SIM_1</key>
    <value>'{{SIM_1}}</value>
    </Parameter>
    <Parameter>
    <key>IMEI</key>
    <value>'{{IMEI}}</value>
    </Parameter>
    <Parameter>
    <key>IMEI_1</key>
    <value>'{{IMEI_1}}</value>
    </Parameter>
    <Parameter>
    <key>WIP_CustomerId</key>
    <value>'{{WIP_CustomerId}}</value>
    </Parameter>
    <Parameter>
    <key>BAN</key>
    <value>'{{BAN}}</value>
    </Parameter>
    <Parameter>
    <key>MSISDN_1</key>
    <value>'{{MSISDN_1}}</value>
    </Parameter>
    <Parameter>
    <key>MSISDN_2</key>
    <value>'{{MSISDN_2}}</value>
    </Parameter>
    <Parameter>
    <key>INSTALLMENTPLANID_1</key>
    <value>'{{INSTALLMENTPLANID_1}}</value>
    </Parameter>
    <Parameter>
    <key>INSTALLMENTPLANID_2</key>
    <value>'{{INSTALLMENTPLANID_2}}</value>
    </Parameter>
    <Parameter>
    <key>EQUIPMENTID_1</key>
    <value>'{{EQUIPMENTID_1}}</value>
    </Parameter>
    <Parameter>
    <key>EQUIPMENTID_2</key>
    <value>'{{EQUIPMENTID_2}}</value>
    </Parameter>
</props>
    </Node>


    <Node log="" name="abort" next="" quiet="true" think="0h" type="com.itko.lisa.test.AbortStep" uid="8A41141A529E11E6A791082820524153" useFilters="true" version="1"> 

    </Node>


    <Node log="" name="fail" next="abort" quiet="true" think="0h" type="com.itko.lisa.test.Abend" uid="8A41141B529E11E6A791082820524153" useFilters="true" version="1"> 

    </Node>


    <Node log="" name="end" next="fail" quiet="true" think="0h" type="com.itko.lisa.test.NormalEnd" uid="8A41141C529E11E6A791082820524153" useFilters="true" version="1"> 

    </Node>


    <DataSet atend="end" local="false" maxItemsToFetch="0" name="Read Rows from Excel File" random="false" type="com.itko.lisa.test.ExcelDataFile">
<sample>rO0ABXNyABFqYXZhLnV0aWwuSGFzaE1hcAUH2sHDFmDRAwACRgAKbG9hZEZhY3RvckkACXRocmVzaG9sZHhwP0AAAAAAAAx3CAAAABAAAAAHdAAGSU1FSV8xdAAAdAAOVHJhbnNhY3Rpb25fSURxAH4AA3QABVNJTV8xcQB+AAN0AARJTUVJcQB+AAN0AANTU050AAkwMDA1NTEyMzR0AANTSU1xAH4AA3QAIFJlYWQgUm93cyBmcm9tIEV4Y2VsIEZpbGVfUm93TnVtdAABMXg=</sample>
    <location>{{LISA_RELATIVE_PROJ_ROOT}}/Data/InputDataSheet.xlsx</location>
    <sheetname>Sheet1</sheetname>
    </DataSet>

</TestCase>