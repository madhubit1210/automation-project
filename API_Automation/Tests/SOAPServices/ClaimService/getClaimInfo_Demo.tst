<?xml version="1.0" ?>

<TestCase name="getClaimInfo_Demo" version="5">

<meta>
   <create version="7.5.1" buildNumber="7.5.1.418" author="LMusunu" date="02/13/2015" host="mlakshman" />
   <lastEdited version="8.2.0" buildNumber="8.2.0.244" author="DMethuk" date="08/03/2016" host="dmethuku" />
</meta>

<id>741540D34CFF11E6A97612DE20524153</id>
<Documentation>Put documentation of the Test Case here.</Documentation>
<IsInProject>true</IsInProject>
<sig>ZWQ9NSZ0Y3Y9NSZsaXNhdj04LjIuMCAoOC4yLjAuMjQ0KSZub2Rlcz0xNjg0NjU4MjU5</sig>
<subprocess>false</subprocess>

<initState>
</initState>

<resultState>
</resultState>

<deletedProps>
</deletedProps>

    <Node name="GET_INPUT" log=""
          type="com.itko.lisa.test.FileNode" 
          version="1" 
          uid="741540D44CFF11E6A97612DE20524153" 
          think="500-1S" 
          useFilters="true" 
          quiet="false" 
          next="Service_Hit" > 


      <!-- Data Sets -->
<readrec>Input Data Sheet</readrec>

      <!-- Assertions -->
<CheckResult assertTrue="true" name="Ensure Property Matches Expression" type="com.itko.lisa.test.CheckResultPropRegEx">
<log>Assertion name: Ensure Property Matches Expression checks for: true  is of type: Property Value Expression.</log>
<then>GET_INPUT</then>
<valueToAssertKey></valueToAssertKey>
        <prop>TC_Execute</prop>
        <param>NO</param>
</CheckResult>

<Loc>{{LISA_RELATIVE_PROJ_ROOT}}/Data/SOAPServices/ClaimService/{{RequestFile}}.txt</Loc>
<charset>DEFAULT</charset>
<PropKey>REQUESTXML</PropKey>
<onFail>abort</onFail>
    </Node>


    <Node name="Service_Hit" log=""
          type="com.itko.lisa.ws.nx.NxWSStep" 
          version="1" 
          uid="7CFBF9024D0211E6A97612DE20524153" 
          think="500-1S" 
          useFilters="true" 
          quiet="false" 
          next="Assertions" > 


      <!-- Filters -->
      <Filter type="com.itko.lisa.xml.FilterXMLXPath">
        <valueToFilterKey>lisa.Service_Hit.rsp</valueToFilterKey>
<prop>statusCode</prop>
<xpathq>{{Query}}</xpathq>
      </Filter>

      <Filter type="com.itko.lisa.test.FilterSavePropToFile">
        <valueToFilterKey>lisa.Service_Hit.request.rsp</valueToFilterKey>
      <file>{{LISA_RELATIVE_PROJ_ROOT}}/Data/SOAPServices/ClaimService/Results/logs/{{TestCaseID}}-req.txt</file>
      <append>false</append>
      </Filter>

      <Filter type="com.itko.lisa.test.FilterSavePropToFile">
        <valueToFilterKey>lisa.Service_Hit.rsp</valueToFilterKey>
      <file>{{LISA_RELATIVE_PROJ_ROOT}}/Data/SOAPServices/ClaimService/Results/logs/{{TestCaseID}}-rsp.txt</file>
      <append>false</append>
      </Filter>

<wsdl></wsdl>
<endpoint>{{Input_ClaimService}}/CLAIM/GETCLAIMINFO</endpoint>
<targetNamespace></targetNamespace>
<service></service>
<port></port>
<operation></operation>
<onError>abort</onError>
<maintainSession>true</maintainSession>
<clearSession>false</clearSession>
<request itko_enc="base64">e3tSRVFVRVNUWE1MfX0=</request>
<style>document</style>
<use>literal</use>
<soapAction></soapAction>
<sslInfo>
<ssl-keystore-password-enc>f5504e2d23a7888253a27e8ef52607d8</ssl-keystore-password-enc>
<ssl-key-password-enc>f5504e2d23a7888253a27e8ef52607d8</ssl-key-password-enc>
</sslInfo>
<wsiInfo>
<wsi-check-wsdl>false</wsi-check-wsdl>
<wsi-check-msg>false</wsi-check-msg>
<wsi-display-type>onlyFailed</wsi-display-type>
<wsi-on-fail>continue</wsi-on-fail>
</wsiInfo>
<transport>
<soapVersionUri>1.1</soapVersionUri>
<httpVersionUri>1.1</httpVersionUri>
<webMethodUri>POST</webMethodUri>
<mtom>false</mtom>
<dontSendRequest>false</dontSendRequest>
<fault-as-error>true</fault-as-error>
<callTimeout>30000</callTimeout>
</transport>
<uddiActive>false</uddiActive>
<uddi-result>
<uddi-selection>
</uddi-selection>
</uddi-result>
<wss4jInfo>
<version>5</version>
<wssecurity>
<isSend>true</isSend>
<must>false</must>
</wssecurity>
<wsi-bsp>false</wsi-bsp>
</wss4jInfo>
<wsaInfo>
<useAddressing>false</useAddressing>
<must>false</must>
<to></to>
<toOverride>false</toOverride>
<toDefault>true</toDefault>
<from></from>
<fromOverride>false</fromOverride>
<fromDefault>true</fromDefault>
<action></action>
<actionOverride>false</actionOverride>
<actionDefault>true</actionDefault>
<msgid></msgid>
<msgidOverride>false</msgidOverride>
<msgidDefault>true</msgidDefault>
<replyTo></replyTo>
<replyToOverride>false</replyToOverride>
<faultTo></faultTo>
<faulttToOverride>false</faulttToOverride>
<addressingVersion>http://www.w3.org/2005/08/addressing</addressingVersion>
</wsaInfo>
<customHTTPHeaderInfo>
    <Parameter>
    <key>Authorization</key>
    <value>Basic cnNwYWN1aTpAUGFzc3dvcmQx</value>
    </Parameter>
</customHTTPHeaderInfo>
<attachments>
<attachment-type>MIME</attachment-type>
</attachments>
<mtom-paths>
</mtom-paths>
    </Node>


    <Node name="Assertions" log=""
          type="com.itko.lisa.test.ScriptNode" 
          version="1" 
          uid="741540D74CFF11E6A97612DE20524153" 
          think="500-1S" 
          useFilters="true" 
          quiet="false" 
          next="Write Result to File" > 

<Documentation>DBDriverClass</Documentation>
<onerror>continue (quiet)</onerror>
<script>import com.itko.lisa.vse.stateful.model.Request;&#13;&#10;import com.itko.util.ParameterList;&#13;&#10;import com.itko.tdm.framework.builder.query.*;&#13;&#10;import com.itko.tdm.framework.dto.*;&#13;&#10;&#13;&#10;&#13;&#10;var testresult;&#13;&#10;if(&quot;{{statusCode}}&quot;.equals(&quot;{{ExpRsp_statusCode}}&quot;))&#13;&#10;{&#13;&#10;testresult = &quot;Passed&quot;;&#13;&#10;}&#13;&#10;else&#13;&#10;{&#13;&#10;testresult = &quot;Failed&quot;;&#13;&#10;}&#13;&#10;&#13;&#10;testExec.setStateValue(&quot;testresult&quot;, testresult);</script>
    </Node>


    <Node name="Write Result to File" log=""
          type="com.itko.lisa.utils.WritePropsNode" 
          version="1" 
          uid="741540D84CFF11E6A97612DE20524153" 
          think="500-1S" 
          useFilters="true" 
          quiet="true" 
          next="GET_INPUT" > 

<file>{{LISA_RELATIVE_PROJ_ROOT}}/Data/SOAPServices/ClaimService/Results/getClaimInfo.csv</file>
<encoding>DEFAULT</encoding>
<bom>true</bom>
<props>
    <Parameter>
    <key>S.No</key>
    <value>{{S.No}}</value>
    </Parameter>
    <Parameter>
    <key>TestCase_Name</key>
    <value>{{TestCaseID}}</value>
    </Parameter>
    <Parameter>
    <key>RequestFile</key>
    <value>{{RequestFile}}</value>
    </Parameter>
    <Parameter>
    <key>Status</key>
    <value>{{testresult}}</value>
    </Parameter>
</props>
    </Node>


    <Node name="Subprocess ALM_TestStatusUpdate_ENTERPRISE" log=""
          type="com.itko.lisa.utils.ExecSubProcessNode" 
          version="1" 
          uid="4CDE871E54BE11E6AF0AB23320524153" 
          think="0H" 
          useFilters="true" 
          quiet="true" 
          next="end" > 

<Subprocess>{{LISA_RELATIVE_PROJ_ROOT}}/Tests/Subprocesses/ALM_TestStatusUpdate_ENTERPRISE.tst</Subprocess>
<fullyParseProps>false</fullyParseProps>
<sendCommonState>false</sendCommonState>
<getCommonState>false</getCommonState>
<onAbort>abort</onAbort>
<Parameters>
    <Parameter>
    <key>TestSetName</key>
    <value>{{TestsetName}}</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
    <Parameter>
    <key>TestSetFolderName</key>
    <value>{{TestsetFolder}}</value>
    <name>Referenced 1st in getTestSets</name>
    </Parameter>
    <Parameter>
    <key>CodeDropFolderName</key>
    <value>{{CodeDropFolderName}}</value>
    <name>Referenced 1st in getTestSetFolderParent</name>
    </Parameter>
    <Parameter>
    <key>ReleaseFolderName</key>
    <value>{{ReleaseFolderName}}</value>
    <name>rootChildFolderName</name>
    </Parameter>
    <Parameter>
    <key>ResultsPath</key>
    <value>{{ResultsPath}}</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
    <Parameter>
    <key>Authorization</key>
    <value>{{Authorization}}</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
    <Parameter>
    <key>Operation</key>
    <value>{{Operation}}</value>
    <name>Referenced 1st in AttachRequest</name>
    </Parameter>
    <Parameter>
    <key>TestCaseID</key>
    <value>{{TestCaseID}}</value>
    <name>Referenced 1st in AttachRequest</name>
    </Parameter>
</Parameters>
<SaveProps>
</SaveProps>
    </Node>


    <Node name="abort" log=""
          type="com.itko.lisa.test.AbortStep" 
          version="1" 
          uid="741540D94CFF11E6A97612DE20524153" 
          think="0h" 
          useFilters="true" 
          quiet="true" 
          next="" > 

    </Node>


    <Node name="fail" log=""
          type="com.itko.lisa.test.Abend" 
          version="1" 
          uid="741540DA4CFF11E6A97612DE20524153" 
          think="0h" 
          useFilters="true" 
          quiet="true" 
          next="abort" > 

    </Node>


    <Node name="end" log=""
          type="com.itko.lisa.test.NormalEnd" 
          version="1" 
          uid="741540DB4CFF11E6A97612DE20524153" 
          think="0h" 
          useFilters="true" 
          quiet="true" 
          next="fail" > 

    </Node>


    <DataSet type="com.itko.lisa.test.ExcelDataFile" name="Input Data Sheet" atend="Subprocess ALM_TestStatusUpdate_ENTERPRISE" local="true" random="false" maxItemsToFetch="0" >
<sample>rO0ABXNyABFqYXZhLnV0aWwuSGFzaE1hcAUH2sHDFmDRAwACRgAKbG9hZEZhY3RvckkACXRocmVzaG9sZHhwP0AAAAAAABh3CAAAACAAAAAYdAASRXhwUnNwX2V4cGxhbmF0aW9udAAAdAAVUlNQX2Jsb2NrRWxpZ2liaWxpdHkgcQB+AAN0AA1SU1BfT3duZXJOYW1lcQB+AAN0ABFSU1BfQWNjb3VudE51bWJlcnEAfgADdAAPUlNQX0Jsb2NrZWRUaW1lcQB+AAN0AA9SU1BfQ2Fycmllck5hbWVxAH4AA3QABFJTUF9xAH4AA3QAC0V4cFJzcF9jb2RldAADMTAwdAALUmVxdWVzdEZpbGV0AA93aXRoIEFsbCBmaWVsZHN0AApUZXN0Q2FzZUlEdAAsVG8gdmFsaWRhdGUgImNoZWNrQmxhY2tMaXN0IiB3aXRoIGFsbCBmaWVsZHN0AA1BcHBsaWNhdGlvbklEdAAERUNOTXQABFMuTm90AAExdAAPUlNQX0Jsb2NrZWREYXRlcQB+AAN0AAlUaW1lU3RhbXBxAH4AFXQAG1JTUF9uYXRpb25hbGJsb2NrSW5kaWNhdG9yCnEAfgADdAAESU1FSXQADzMyMjMzNDI0MjQwMDAwMHQAEUV4cFJzcF9kZWZpbml0aW9ucQB+AAN0AAlDaGFubmVsSUR0AAZSRVRBSUx0AApSU1BfTVNJU0ROcQB+AAN0AApUQ19FeGVjdXRlcQB+AAN0ACBSZWFkIFJvd3MgZnJvbSBFeGNlbCBGaWxlX1Jvd051bXQAATF0ABpSU1BfYmxvY2tlZGJ5T3RoZXJDYXJyaWVyIHEAfgADdAAIU2VuZGVySUR0AAhFUklDU1NPTnQAFEV4cFJzcF9zdWJTdGF0dXNDb2RlcQB+AAN4</sample>
    <location>{{LISA_RELATIVE_PROJ_ROOT}}/Data/SOAPServices/ClaimService/ClaimService.xlsx</location>
    <sheetname>getClaimInfo</sheetname>
    </DataSet>

</TestCase>
