<?xml version="1.0" ?>

<TestCase name="practise" version="5">

<meta>
   <create version="8.2.0" buildNumber="8.2.0.244" author="DMethuk" date="07/21/2016" host="dmethuku" />
   <lastEdited version="8.2.0" buildNumber="8.2.0.244" author="DMethuk" date="07/24/2016" host="dmethuku" />
</meta>

<id>7F6995F54FC011E6B34B108220524153</id>
<Documentation>Put documentation of the Test Case here.</Documentation>
<IsInProject>true</IsInProject>
<sig>ZWQ9NSZ0Y3Y9NSZsaXNhdj04LjIuMCAoOC4yLjAuMjQ0KSZub2Rlcz0tMTc3OTU1Nzg3MA==</sig>
<subprocess>false</subprocess>

<initState>
</initState>

<resultState>
</resultState>

<deletedProps>
</deletedProps>

    <Node name="Read file {{RequestFile}}.txt" log=""
          type="com.itko.lisa.test.FileNode" 
          version="1" 
          uid="6AB71C0250B011E68F73D6FB20524153" 
          think="500-1S" 
          useFilters="true" 
          quiet="true" 
          next="Web Service" > 


      <!-- Data Sets -->
<readrec>Read Rows from Excel File</readrec>

      <!-- Assertions -->
<CheckResult assertTrue="true" name="Ensure Property Matches Expression" type="com.itko.lisa.test.CheckResultPropRegEx">
<log>Assertion name: Ensure Property Matches Expression checks for: true is of type: Property Value Expression.</log>
<then>Read file {{RequestFile}}.txt</then>
<valueToAssertKey></valueToAssertKey>
        <prop>TC_Execute</prop>
        <param>NO</param>
</CheckResult>

<Loc>{{LISA_RELATIVE_PROJ_ROOT}}/Data/SOAPServices/ClaimService/{{RequestFile}}.txt</Loc>
<charset>DEFAULT</charset>
<PropKey>requestxml</PropKey>
<onFail>abort</onFail>
    </Node>


    <Node name="Web Service" log=""
          type="com.itko.lisa.ws.nx.NxWSStep" 
          version="1" 
          uid="B648059650B311E68F73D6FB20524153" 
          think="500-1S" 
          useFilters="true" 
          quiet="false" 
          next="Java Script Step" > 


      <!-- Filters -->
      <Filter type="com.itko.lisa.xml.FilterXMLXPath">
        <valueToFilterKey>lisa.Web Service.rsp</valueToFilterKey>
<prop>statuscode</prop>
<xpathq>{{Query}}</xpathq>
      </Filter>

<endpoint>http://qattbco522.unix.gsm1900.org:8115/Enterprise/CLAIM/GETCLAIMINFO</endpoint>
<onError>abort</onError>
<maintainSession>true</maintainSession>
<clearSession>false</clearSession>
<request itko_enc="base64">e3tyZXF1ZXN0eG1sfX0=</request>
<style>document</style>
<use>literal</use>
<sslInfo>
<ssl-keystore-password-enc>f5504e2d23a7888253a27e8ef52607d8</ssl-keystore-password-enc>
<ssl-key-password-enc>f5504e2d23a7888253a27e8ef52607d8</ssl-key-password-enc>
</sslInfo>
<wsiInfo>
<wsi-check-wsdl>false</wsi-check-wsdl>
<wsi-check-msg>false</wsi-check-msg>
<wsi-display-type>onlyFailed</wsi-display-type>
<wsi-on-fail>continue</wsi-on-fail>
</wsiInfo>
<transport>
<soapVersionUri>1.1</soapVersionUri>
<httpVersionUri>1.1</httpVersionUri>
<webMethodUri>POST</webMethodUri>
<mtom>false</mtom>
<dontSendRequest>false</dontSendRequest>
<fault-as-error>true</fault-as-error>
<callTimeout>30000</callTimeout>
</transport>
<uddiActive>false</uddiActive>
<uddi-result>
<uddi-selection>
</uddi-selection>
</uddi-result>
<wss4jInfo>
<version>5</version>
<wssecurity>
<isSend>true</isSend>
<must>false</must>
</wssecurity>
<wsi-bsp>false</wsi-bsp>
</wss4jInfo>
<wsaInfo>
<useAddressing>false</useAddressing>
<must>false</must>
<to></to>
<toOverride>false</toOverride>
<toDefault>true</toDefault>
<from></from>
<fromOverride>false</fromOverride>
<fromDefault>true</fromDefault>
<action></action>
<actionOverride>false</actionOverride>
<actionDefault>true</actionDefault>
<msgid></msgid>
<msgidOverride>false</msgidOverride>
<msgidDefault>true</msgidDefault>
<replyTo></replyTo>
<replyToOverride>false</replyToOverride>
<faultTo></faultTo>
<faulttToOverride>false</faulttToOverride>
<addressingVersion>http://www.w3.org/2005/08/addressing</addressingVersion>
</wsaInfo>
<customHTTPHeaderInfo>
</customHTTPHeaderInfo>
<attachments>
<attachment-type>MIME</attachment-type>
</attachments>
<mtom-paths>
</mtom-paths>
    </Node>


    <Node name="Java Script Step" log=""
          type="com.itko.lisa.test.ScriptNode" 
          version="1" 
          uid="9975178C50B111E68F73D6FB20524153" 
          think="500-1S" 
          useFilters="true" 
          quiet="false" 
          next="Write Result to File" > 


      <!-- Assertions -->
<CheckResult assertTrue="true" name="Any Exception Then Fail" type="com.itko.lisa.dynexec.CheckInvocationEx">
<log>Assertion name: Any Exception Then Fail checks for: true is of type: Assert on Invocation Exception.</log>
<then>fail</then>
<valueToAssertKey></valueToAssertKey>
        <param>.*</param>
</CheckResult>

<onerror>abort</onerror>
<script>import com.itko.lisa.vse.stateful.model.Request;&#13;&#10;import com.itko.util.ParameterList;&#13;&#10;import com.itko.tdm.framework.builder.query.*;&#13;&#10;import com.itko.tdm.framework.dto.*;&#13;&#10;&#13;&#10;&#13;&#10;var testresult;&#13;&#10;if(&quot;{{statuscode}}&quot;.equals(&quot;{{ExpRsp_statusCode}}&quot;))&#13;&#10;{&#13;&#10;testresult = &quot;Passed&quot;;&#13;&#10;}&#13;&#10;else&#13;&#10;{&#13;&#10;testresult = &quot;Failed&quot;;&#13;&#10;}&#13;&#10;&#13;&#10;testExec.setStateValue(&quot;testresult&quot;, testresult);</script>
    </Node>


    <Node name="Write Result to File" log=""
          type="com.itko.lisa.utils.WritePropsNode" 
          version="1" 
          uid="741540D84CFF11E6A97612DE20524153" 
          think="500-1S" 
          useFilters="true" 
          quiet="true" 
          next="Read file {{RequestFile}}.txt" > 

<file>{{LISA_RELATIVE_PROJ_ROOT}}/Data/SOAPServices/ClaimService/Results/getClaimInfo.csv</file>
<encoding>DEFAULT</encoding>
<bom>true</bom>
<props>
    <Parameter>
    <key>S.No</key>
    <value>{{S.No}}</value>
    </Parameter>
    <Parameter>
    <key>TestCase_Name</key>
    <value>{{Operation}}</value>
    </Parameter>
    <Parameter>
    <key>RequestFile</key>
    <value>{{RequestFile}}</value>
    </Parameter>
    <Parameter>
    <key>Status</key>
    <value>{{testresult}}</value>
    </Parameter>
</props>
    </Node>


    <Node name="abort" log=""
          type="com.itko.lisa.test.AbortStep" 
          version="1" 
          uid="7F69BD074FC011E6B34B108220524153" 
          think="0h" 
          useFilters="true" 
          quiet="true" 
          next="" > 

    </Node>


    <Node name="fail" log=""
          type="com.itko.lisa.test.Abend" 
          version="1" 
          uid="7F69BD094FC011E6B34B108220524153" 
          think="0h" 
          useFilters="true" 
          quiet="true" 
          next="abort" > 

    </Node>


    <Node name="end" log=""
          type="com.itko.lisa.test.NormalEnd" 
          version="1" 
          uid="7F69BD0B4FC011E6B34B108220524153" 
          think="0h" 
          useFilters="true" 
          quiet="true" 
          next="fail" > 

    </Node>


    <DataSet type="com.itko.lisa.test.ExcelDataFile" name="Read Rows from Excel File" atend="end" local="false" random="false" maxItemsToFetch="100" >
<sample>rO0ABXNyABFqYXZhLnV0aWwuSGFzaE1hcAUH2sHDFmDRAwACRgAKbG9hZEZhY3RvckkACXRocmVzaG9sZHhwP0AAAAAAAAB3CAAAAAEAAAAAeA==</sample>
    <location>{{LISA_RELATIVE_PROJ_ROOT}}/Data/SOAPServices/ClaimService/ClaimService.xlsx</location>
    <sheetname>getClaimInfo</sheetname>
    </DataSet>

</TestCase>
