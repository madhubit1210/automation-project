<?xml version="1.0" ?>

<TestCase name="getUserInfo" version="5">

<meta>
   <create version="7.5.1" buildNumber="7.5.1.418" author="LMusunu" date="02/13/2015" host="mlakshman" />
   <lastEdited version="8.2.0" buildNumber="8.2.0.244" author="sraavi2" date="07/18/2016" host="RSRUJANA" />
</meta>

<id>51F77DB447B711E6AAB9F0DEF1EA9004</id>
<Documentation>Put documentation of the Test Case here.</Documentation>
<IsInProject>true</IsInProject>
<sig>ZWQ9NSZ0Y3Y9NSZsaXNhdj04LjIuMCAoOC4yLjAuMjQ0KSZub2Rlcz0tMjU5MDQ5OTU4</sig>
<subprocess>false</subprocess>

<initState>
</initState>

<resultState>
</resultState>

<deletedProps>
</deletedProps>

    <Node name="GET_INPUT" log=""
          type="com.itko.lisa.test.FileNode" 
          version="1" 
          uid="51F77DB547B711E6AAB9F0DEF1EA9004" 
          think="500-1S" 
          useFilters="true" 
          quiet="false" 
          next="Output Log Message" > 


      <!-- Data Sets -->
<readrec>Read Rows from Excel File</readrec>

      <!-- Assertions -->
<CheckResult assertTrue="true" name="Ensure Property Matches Expression" type="com.itko.lisa.test.CheckResultPropRegEx">
<log>Assertion name: Ensure Property Matches Expression checks for: true  is of type: Property Value Expression.</log>
<then>Output Log Message</then>
<valueToAssertKey></valueToAssertKey>
        <prop>TC_Execute</prop>
        <param>Yes</param>
</CheckResult>

<Loc>{{LISA_RELATIVE_PROJ_ROOT}}/Data/SOAPServices/UserInfoService/UserInfoService.getUserInfo</Loc>
<charset>UTF-8</charset>
<PropKey>REQUESTXML</PropKey>
<onFail>abort</onFail>
    </Node>


    <Node name="Output Log Message" log=""
          type="com.itko.lisa.test.TestNodeLogger" 
          version="1" 
          uid="51F77DB647B711E6AAB9F0DEF1EA9004" 
          think="500-1S" 
          useFilters="true" 
          quiet="true" 
          next="SERVICE_HIT" > 

<log>Currently Executing Operation: {{Operation}} of SERVICE: {{Service}}...&#13;&#10;Currently Executing Test Case : {{TestCaseID}}&#13;&#10;Currently Executing Row Number : {{S.No}} , {{S.No}}&#13;&#10;Currently Executing Row Count : {{Read Rows from Excel File_RowCount}} </log>
    </Node>


    <Node name="SERVICE_HIT" log=""
          type="com.itko.lisa.ws.RawSOAPNode" 
          version="1" 
          uid="51F77DB747B711E6AAB9F0DEF1EA9004" 
          think="500-1S" 
          useFilters="true" 
          quiet="false" 
          next="Assertions" > 

<url>{{Endpoint_UserInfo}}</url>
<action>&quot;getUserInfo&quot; </action>
<soapRequest itko_enc="base64">e3tSRVFVRVNUWE1MfX0=</soapRequest>
<contentType>text/xml; charset=UTF-8</contentType>
<onError>abort</onError>
<discardResponse>false</discardResponse>
<customHTTPHeaderInfo>
</customHTTPHeaderInfo>
    </Node>


    <Node name="Assertions" log=""
          type="com.itko.lisa.test.ScriptNode" 
          version="1" 
          uid="51F77DB847B711E6AAB9F0DEF1EA9004" 
          think="500-1S" 
          useFilters="true" 
          quiet="false" 
          next="Write Result to File" > 

<Documentation>DBDriverClass</Documentation>
<onerror>continue (quiet)</onerror>
<script>import java.io.*;&#13;&#10;import java.util.*;&#13;&#10;import java.xml.*;&#13;&#10;import org.xml.sax.InputSource;&#13;&#10;import javax.xml.parsers.DocumentBuilder;&#13;&#10;import javax.xml.parsers.DocumentBuilderFactory;&#13;&#10;import org.w3c.dom.Document;&#13;&#10;import org.w3c.dom.Element;&#13;&#10;import org.w3c.dom.NodeList;&#13;&#10;import com.itko.lisa.vse.stateful.model.Request;&#13;&#10;import com.itko.util.ParameterList;&#13;&#10;import com.itko.tdm.framework.builder.query.*;&#13;&#10;import com.itko.tdm.framework.dto.*;&#13;&#10;import org.junit.Assert.*;&#13;&#10;&#13;&#10;    String response = testExec.getStateValue(&quot;LASTRESPONSE&quot;).toString();&#13;&#10;    HashMap nodeData=new HashMap();&#13;&#10;    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();&#13;&#10;    DocumentBuilder builder = factory.newDocumentBuilder();&#13;&#10;&#9;InputSource is = new InputSource();&#13;&#10;    is.setCharacterStream(new StringReader(response));&#13;&#10;    Document doc = builder.parse(is);&#13;&#10;    doc.getDocumentElement().normalize();&#13;&#10;    NodeList list = doc.getElementsByTagName(&quot;*&quot;);&#13;&#10;    //NodeList list1 = list;&#13;&#10;    //var vlaue ={};&#13;&#10;    HashMap nodeData=new HashMap();&#13;&#10;    &#13;&#10;    for (int i = 0; i &lt; list.getLength(); i++) {&#13;&#10;    &#9;int count = 1;&#13;&#10;    &#9;List valueList = new ArrayList();&#13;&#10;    &#9;String nodeName = list.item(i).getNodeName();&#13;&#10;&#13;&#10;        if(list.item(i).hasAttributes())&#13;&#10;    &#9;{&#13;&#10;    &#9;&#9;int attributeCount = list.item(i).getAttributes().getLength();&#13;&#10;    &#9;&#9;for (int attCount = 0; attCount &lt; attributeCount; attCount++) {&#13;&#10;&#9;    &#9;&#9;String attName = list.item(i).getAttributes().item(attCount).getNodeName();&#13;&#10;&#9;    &#9;&#9;List valueList1 = new ArrayList();&#13;&#10;&#9;    &#9;&#9;valueList1.add(list.item(i).getAttributes().item(attCount).getNodeValue());&#13;&#10;                if (!attName.contains(&quot;:&quot;) &amp;&amp; !valueList1.isEmpty())&#13;&#10;                    {nodeData.put(attName, valueList1);&#13;&#10;                     testExec.setStateValue(&quot;ActRsp_&quot;+attName, valueList1); &#13;&#10;                     }&#13;&#10;    &#9;&#9;}&#13;&#10;    &#9;}&#13;&#10;&#13;&#10;    &#9; if (!nodeName.contains(&quot;base:&quot;) &amp;&amp; !nodeName.contains(&quot;:id&quot;))&#13;&#10;         {&#13;&#10;    &#9;&#9; nodeName = nodeName.substring(nodeName.indexOf(&quot;:&quot;)+1, nodeName.length());&#13;&#10;             if (list.item(i).hasChildNodes()){&#13;&#10;    &#9;&#9; String val= list.item(i).getChildNodes().item(0).getNodeValue();&#9;&#13;&#10;    &#9;&#9; if (val != null &amp;&amp; !val.equalsIgnoreCase(&quot;&quot;) &amp;&amp; !val.equalsIgnoreCase(null)  )&#13;&#10;             {&#13;&#10;    &#9;&#9;&#9; valueList.add(val.trim());&#13;&#10;                for (int j = i+1; j &lt; list.getLength(); j++) {&#13;&#10;                    if(list.item(i).getNodeName() == list.item(j).getNodeName())&#13;&#10;                    {    String nodeValue= list.item(j).getChildNodes().item(0).getNodeValue().trim();&#13;&#10;                         valueList.add(nodeValue);&#13;&#10;                         count++;&#13;&#10;                         list.item(j).getParentNode().removeChild(list.item(j));&#13;&#10;                    }&#13;&#10;                }&#13;&#10;                if (list.item(i).getChildNodes().getLength() &gt; 0) {nodeData.put(nodeName, valueList);}&#13;&#10;             }   &#13;&#10;             }&#13;&#10;         }&#13;&#10;   &#9;}&#13;&#10;&#13;&#10;    &#13;&#10;    testExec.setStateValue(&quot;TC_Result&quot;, &quot;Passed&quot;);&#13;&#10;    String getAllcomparisons = &quot;&quot;;&#13;&#10;    Iterator it = nodeData.entrySet().iterator();&#13;&#10;    while (it.hasNext()) {&#13;&#10;        Map.Entry pair = (Map.Entry)it.next();&#13;&#10;        String Key = pair.getKey();&#13;&#10;        String expNodeName = &quot;ExpRsp_&quot;+Key;&#13;&#10;        String ExpectedValue = testExec.getStateValue(expNodeName); &#13;&#10;        //testExec.setStateValue(expNodeName, ExpectedValue);&#13;&#10;&#13;&#10;        String ActualValue = pair.getValue().toString().substring(1, pair.getValue().toString().length()-1).trim();&#13;&#10;        testExec.setStateValue(&quot;ActRsp_&quot;+pair.getKey(), ActualValue);&#13;&#10;&#13;&#10;        &#13;&#10;        if (ExpectedValue != null &amp;&amp; ExpectedValue != &quot;&quot;){&#13;&#10;        System.out.println(&quot;ExpectedValue :&quot;+ExpectedValue);&#13;&#10;        System.out.println(&quot;ActualValue :&quot;+ActualValue);&#13;&#10;            if (!ExpectedValue.equalsIgnoreCase(ActualValue))&#13;&#10;            {&#13;&#10;                testExec.warn(&quot;Element: &quot;+ pair.getKey()+&quot; Expected Value: &quot;+ExpectedValue+ &quot; Not Matching Actual Value: &quot;+ActualValue);&#13;&#10;                testExec.setStateValue(&quot;TC_Result&quot;, &quot;Failed&quot;);&#13;&#10;                getAllcomparisons = getAllcomparisons + &quot;Element: &quot;+ pair.getKey()+&quot; Expected Value: &quot;+ExpectedValue+ &quot; Not Matching Actual Value: &quot;+ActualValue +&quot;\n&quot;;&#13;&#10;            }&#13;&#10;            if (ExpectedValue.equalsIgnoreCase(ActualValue))&#13;&#10;            {&#13;&#10;                testExec.log(&quot;Element: &quot;+ pair.getKey()+&quot; Expected Value: &quot;+ExpectedValue+ &quot;  Matching Actual Value: &quot;+ActualValue);&#13;&#10;                getAllcomparisons = getAllcomparisons + &quot;Element: &quot;+ pair.getKey()+&quot; Expected Value: &quot;+ExpectedValue+ &quot; Matching Actual Value: &quot;+ActualValue +&quot;\n&quot;;&#13;&#10;            }&#13;&#10;        } &#13;&#10;    }&#13;&#10;    testExec.setStateValue(&quot;getAllcomparisons&quot;, getAllcomparisons);</script>
    </Node>


    <Node name="Write Result to File" log=""
          type="com.itko.lisa.utils.WritePropsNode" 
          version="1" 
          uid="51F77DB947B711E6AAB9F0DEF1EA9004" 
          think="500-1S" 
          useFilters="true" 
          quiet="true" 
          next="GET_INPUT" > 

<file>{{LISA_RELATIVE_PROJ_ROOT}}/Data/SOAPServices/UserInfoService/Results/getUserInfo.csv</file>
<encoding>DEFAULT</encoding>
<bom>true</bom>
<props>
    <Parameter>
    <key>S.No</key>
    <value>{{S.No}}</value>
    </Parameter>
    <Parameter>
    <key>TestCase_Name</key>
    <value>{{Operation}}</value>
    </Parameter>
    <Parameter>
    <key>RequestFile</key>
    <value>{{RequestFile}}</value>
    </Parameter>
    <Parameter>
    <key>Status</key>
    <value>{{TC_Result}}</value>
    </Parameter>
</props>
    </Node>


    <Node name="abort" log=""
          type="com.itko.lisa.test.AbortStep" 
          version="1" 
          uid="51F77DBC47B711E6AAB9F0DEF1EA9004" 
          think="0h" 
          useFilters="true" 
          quiet="true" 
          next="" > 

    </Node>


    <Node name="fail" log=""
          type="com.itko.lisa.test.Abend" 
          version="1" 
          uid="51F77DBB47B711E6AAB9F0DEF1EA9004" 
          think="0h" 
          useFilters="true" 
          quiet="true" 
          next="abort" > 

    </Node>


    <Node name="end" log=""
          type="com.itko.lisa.test.NormalEnd" 
          version="1" 
          uid="51F77DBA47B711E6AAB9F0DEF1EA9004" 
          think="0h" 
          useFilters="true" 
          quiet="true" 
          next="fail" > 

    </Node>


    <DataSet type="com.itko.lisa.test.ExcelDataFile" name="Read Rows from Excel File" atend="end" local="true" random="false" maxItemsToFetch="0" >
<sample>rO0ABXNyABFqYXZhLnV0aWwuSGFzaE1hcAUH2sHDFmDRAwACRgAKbG9hZEZhY3RvckkACXRocmVzaG9sZHhwP0AAAAAAABh3CAAAACAAAAAOdAAMU2VydmljZSBOYW1ldAAIVXNlckluZm90ABZJbnB1dF9wYXJ0bmVyVGltZXN0YW1wdAAdMjAxNi0wNi0zMFQwMjo1MzozNC43NjAtMDc6MDB0AAtSZXF1ZXN0RmlsZXQAC2dldFVzZXJJbmZvdAANT3BlcmF0aW9uTmFtZXQAC2dldFVzZXJJbmZvdAAEUy5Ob3QAO1RJQkNPX1VzZXJJbmZvX2dldFVzZXJJbmZvX1dpdGhBbGxNYW5kYXRvcnlFbGVtZW50c19QX1RDMDAxdAAYRXhwUnNwX3NlcnZpY2VTdGF0dXNDb2RldAADMTAwdAARUmVsZWFzZUZvbGRlck5hbWV0AAB0ABxJbnB1dFJlcV9zeXN0ZW1JZHNUb1JldHJpZXZldAAGc2Ftc29udAANQXV0aG9yaXphdGlvbnEAfgAPdAAPSW5wdXRSZXFfdXNlcklkdAAKcmV0YWlsanVtcHQAClRDX0V4ZWN1dGV0AANZZXN0ACBSZWFkIFJvd3MgZnJvbSBFeGNlbCBGaWxlX1Jvd051bXQAATF0AAtSZXN1bHRzUGF0aHEAfgAPdAASQ29kZURyb3BGb2xkZXJOYW1lcQB+AA94</sample>
    <location>{{LISA_RELATIVE_PROJ_ROOT}}/Data/SOAPServices/UserInfoService/UserInfo.xlsx</location>
    <sheetname>getUserInfo</sheetname>
    </DataSet>

</TestCase>
