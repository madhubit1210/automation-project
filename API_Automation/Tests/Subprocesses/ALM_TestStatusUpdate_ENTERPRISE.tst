<?xml version="1.0" ?>

<TestCase name="ALM_TestStatusUpdate_ENTERPRISE" version="5">

<meta>
   <create version="7.5.1" buildNumber="7.5.1.418" author="MRavill" date="04/30/2015" host="RMURALI" />
   <lastEdited version="8.2.0" buildNumber="8.2.0.244" author="DMethuk" date="08/03/2016" host="dmethuku" />
</meta>

<id>26C5426E54BE11E6AF0AB23320524153</id>
<Documentation>Put documentation of the Test Case here.</Documentation>
<IsInProject>true</IsInProject>
<sig>ZWQ9NSZ0Y3Y9NSZsaXNhdj04LjIuMCAoOC4yLjAuMjQ0KSZub2Rlcz0tNDQzNzgzOTUy</sig>
<subprocess>true</subprocess>

<initState>
    <Parameter>
    <key>TestSetName</key>
    <value>{{TestSetName}}</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
    <Parameter>
    <key>TestSetFolderName</key>
    <value>{{TestSetFolderName}}</value>
    <name>Referenced 1st in getTestSets</name>
    </Parameter>
    <Parameter>
    <key>CodeDropFolderName</key>
    <value>{{CodeDropFolderName}}</value>
    <name>Referenced 1st in getTestSetFolderParent</name>
    </Parameter>
    <Parameter>
    <key>ReleaseFolderName</key>
    <value>{{ReleaseFolderName}}</value>
    <name>rootChildFolderName</name>
    </Parameter>
    <Parameter>
    <key>ResultsPath</key>
    <value></value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
    <Parameter>
    <key>Authorization</key>
    <value>{{Authorization}}</value>
    <name>&lt;&lt; description &gt;&gt;</name>
    </Parameter>
    <Parameter>
    <key>Operation</key>
    <value>{{Operation}}</value>
    <name>Referenced 1st in AttachRequest</name>
    </Parameter>
    <Parameter>
    <key>TestCaseID</key>
    <value>70F75AAF598511E687B208FB20524153</value>
    <name>Referenced 1st in AttachRequest</name>
    </Parameter>
</initState>

<resultState>
    <Parameter>
    <key>AddressLine</key>
    <value></value>
    <name>Set in project.config configuration</name>
    </Parameter>
    <Parameter>
    <key>AlNumValue</key>
    <value></value>
    <name>Set in project.config configuration</name>
    </Parameter>
    <Parameter>
    <key>Answer</key>
    <value></value>
    <name>Set in project.config configuration</name>
    </Parameter>
    <Parameter>
    <key>AnswerSelectionType</key>
    <value></value>
    <name>Set in project.config configuration</name>
    </Parameter>
    <Parameter>
    <key>BASEPATH</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>BirthDate</key>
    <value></value>
    <name>Set in project.config configuration</name>
    </Parameter>
    <Parameter>
    <key>Boolean</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>Cart_WSIL_ENDPOINT_URL</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>City</key>
    <value></value>
    <name>Set in project.config configuration</name>
    </Parameter>
    <Parameter>
    <key>ConnectionString</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>ConnectionString1</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>CustomerID</key>
    <value></value>
    <name>Set in project.config configuration</name>
    </Parameter>
    <Parameter>
    <key>DATASET_LOCATION</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>DBConnectString</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>DBDriverClass</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>DBName</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>DBPW</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>DBServerName</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>DBServerPort</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>DBUN</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>DB_RO_Password</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>DB_RO_UserName</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>DB_RW_Password</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>DB_RW_UserName</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>DB_SprocName_FinancialAccount-updateFinancialAccountAddress</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>DB_SprocName_FinancialAccount-updateFinancialAccountContact</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>DB_SprocName_Payment-DeleteEasyPay</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>DB_SprocName_Payment-UpdateEasyPay</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>DB_SprocName_Payment-createEasyPay</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>DB_ViewName_Address-soapServices</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>DB_ViewName_Cart-getCartItem</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>DB_ViewName_Customer-advancedSearchCustomer</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>DB_ViewName_Customer-checkNewLineEligibility</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>DB_ViewName_Customer-getCustomer</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>DB_ViewName_Customer-getCustomerAccount</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>DB_ViewName_Customer-getCustomerProfile</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>DB_ViewName_Customer-getCustomerSearchCriteria</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>DB_ViewName_Customer-getCustomerSecurityProfile</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>DB_ViewName_Customer-getCustomercreditprofile</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>DB_ViewName_Customer-loanEligibility</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>DB_ViewName_Customer-privacyProfile</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>DB_ViewName_Customer-searchCustomer</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>DB_ViewName_Customer-searchCustomerPost</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>DB_ViewName_Device-getDeviceDetails</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>DB_ViewName_FinancialAccount-getBillingArrangement</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>DB_ViewName_FinancialAccount-getFinancialAccountContact</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>DB_ViewName_FinancialAccount-getFinancialAccountDetail</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>DB_ViewName_FinancialAccount-getPaymentProfile</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>DB_ViewName_FinancialAccount-getfinancialaccount</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>DB_ViewName_FinancialAccount-updateFinancialAccountAddress</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>DB_ViewName_FinancialAccount-updateFinancialAccountContact</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>DB_ViewName_LineOfService-getLineofService</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>DB_ViewName_LineOfService-getLineofServiceDetails</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>DB_ViewName_NetworkResource-getSimDetails</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>DB_ViewName_Offers-getOffers</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>DB_ViewName_Payment-getArrangedPaymentHistoryFormsisdn</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>DB_ViewName_Payment-getScheduledPaymentDetails</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>DB_ViewName_PaymentArrangement-getArrangedPaymentHistory</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>DB_ViewName_Port-getPortInDetails</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>DB_ViewName_Port-getPortInEligibility</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>DB_ViewName_Port-portRes</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>DB_ViewName_Store-getStoreDetails</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>DTI</key>
    <value></value>
    <name>Set in project.config configuration</name>
    </Parameter>
    <Parameter>
    <key>DTI1</key>
    <value></value>
    <name>Set in project.config configuration</name>
    </Parameter>
    <Parameter>
    <key>DTIC</key>
    <value></value>
    <name>Set in project.config configuration</name>
    </Parameter>
    <Parameter>
    <key>DeviceTradeInWSIL_ENDPOINT_URL</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>Domain</key>
    <value></value>
    <name>Set in project.config configuration</name>
    </Parameter>
    <Parameter>
    <key>DomainName</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>ENDPOINT</key>
    <value></value>
    <name>Set in project.config configuration</name>
    </Parameter>
    <Parameter>
    <key>ENDPOINT1</key>
    <value></value>
    <name>Set in project.config configuration</name>
    </Parameter>
    <Parameter>
    <key>ENDPOINT2</key>
    <value></value>
    <name>Set in project.config configuration</name>
    </Parameter>
    <Parameter>
    <key>Endpoint_CSC_DIT</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>Expected Vs Actual</key>
    <value></value>
    <name>Set 1st in getTestID</name>
    </Parameter>
    <Parameter>
    <key>First Name</key>
    <value></value>
    <name>Set in project.config configuration</name>
    </Parameter>
    <Parameter>
    <key>FolderName</key>
    <value></value>
    <name>Set 1st in getTestSetFolderParent</name>
    </Parameter>
    <Parameter>
    <key>IMEI</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>IMSI</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>LIVE_INVOCATION_PORT</key>
    <value></value>
    <name>Set in project.config configuration</name>
    </Parameter>
    <Parameter>
    <key>LIVE_INVOCATION_SERVER</key>
    <value></value>
    <name>Set in project.config configuration</name>
    </Parameter>
    <Parameter>
    <key>Last Name</key>
    <value></value>
    <name>Set in project.config configuration</name>
    </Parameter>
    <Parameter>
    <key>MSISDN</key>
    <value></value>
    <name>Set in project.config configuration</name>
    </Parameter>
    <Parameter>
    <key>Middle Initial</key>
    <value></value>
    <name>Set in project.config configuration</name>
    </Parameter>
    <Parameter>
    <key>NGP</key>
    <value></value>
    <name>Set in project.config configuration</name>
    </Parameter>
    <Parameter>
    <key>NPA</key>
    <value></value>
    <name>Set in project.config configuration</name>
    </Parameter>
    <Parameter>
    <key>NXX</key>
    <value></value>
    <name>Set in project.config configuration</name>
    </Parameter>
    <Parameter>
    <key>NewMSISDN</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>NewSimNumber</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>NumberManagement</key>
    <value></value>
    <name>Set in project.config configuration</name>
    </Parameter>
    <Parameter>
    <key>NumericValue</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>OrderReferenceDataWSIL_ENDPOINT_URL</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>PORT</key>
    <value></value>
    <name>Set in project.config configuration</name>
    </Parameter>
    <Parameter>
    <key>ParentName</key>
    <value></value>
    <name>Set 1st in getTestSetFolderParent</name>
    </Parameter>
    <Parameter>
    <key>PhoneNumber</key>
    <value></value>
    <name>Set in project.config configuration</name>
    </Parameter>
    <Parameter>
    <key>PhoneType</key>
    <value></value>
    <name>Set in project.config configuration</name>
    </Parameter>
    <Parameter>
    <key>ProductOfferingSummaryWSIL_ENDPOINT_URL</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>ProductReferenceDataWSIL_ENDPOINT_URL</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>Project</key>
    <value></value>
    <name>Set in project.config configuration</name>
    </Parameter>
    <Parameter>
    <key>ProjectName</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>Quantity</key>
    <value></value>
    <name>Set in project.config configuration</name>
    </Parameter>
    <Parameter>
    <key>Question</key>
    <value></value>
    <name>Set in project.config configuration</name>
    </Parameter>
    <Parameter>
    <key>Read Rows from Excel File_RowNum</key>
    <value></value>
    <name>Set 1st in getTestID</name>
    </Parameter>
    <Parameter>
    <key>Read Rows from a Delimited Data File_RowNum</key>
    <value></value>
    <name>Set 1st in getTestID</name>
    </Parameter>
    <Parameter>
    <key>ReasonCode</key>
    <value></value>
    <name>Set in project.config configuration</name>
    </Parameter>
    <Parameter>
    <key>ReasonName</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>RequestFile</key>
    <value></value>
    <name>Set 1st in getTestID</name>
    </Parameter>
    <Parameter>
    <key>RequestXML</key>
    <value></value>
    <name>Set 1st in getTestID</name>
    </Parameter>
    <Parameter>
    <key>ResponseXML</key>
    <value></value>
    <name>Set 1st in getTestID</name>
    </Parameter>
    <Parameter>
    <key>Result_File</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>S.No</key>
    <value></value>
    <name>Set 1st in getTestID</name>
    </Parameter>
    <Parameter>
    <key>S.No&#9;TestCase_Name&#9;RequestFile&#9;Status&#9;Expected Vs Actual</key>
    <value></value>
    <name>Set 1st in getTestID</name>
    </Parameter>
    <Parameter>
    <key>SAPOrderId</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>SERVER</key>
    <value></value>
    <name>Set in project.config configuration</name>
    </Parameter>
    <Parameter>
    <key>SSN</key>
    <value></value>
    <name>Set in project.config configuration</name>
    </Parameter>
    <Parameter>
    <key>SimNumber</key>
    <value></value>
    <name>Set in project.config configuration</name>
    </Parameter>
    <Parameter>
    <key>State</key>
    <value></value>
    <name>Set in project.config configuration</name>
    </Parameter>
    <Parameter>
    <key>Status</key>
    <value></value>
    <name>Set 1st in getTestID</name>
    </Parameter>
    <Parameter>
    <key>StoreType</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>String</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>Suffix</key>
    <value></value>
    <name>Set in project.config configuration</name>
    </Parameter>
    <Parameter>
    <key>Test</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>TestCase_Name</key>
    <value></value>
    <name>Set 1st in getTestID</name>
    </Parameter>
    <Parameter>
    <key>TestParam</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>TimeStamp</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>Title</key>
    <value></value>
    <name>Set in project.config configuration</name>
    </Parameter>
    <Parameter>
    <key>TransactionId</key>
    <value></value>
    <name>Set in project.config configuration</name>
    </Parameter>
    <Parameter>
    <key>URL</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>UpdateReasonCode</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>WSPORT</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>WSSERVER</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>YIFWebServiceEndPointURL</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>YIFWebService_Data_File</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>YIFWebService_ENDPOINT_URL</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>accountType</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>amount</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>applicationID</key>
    <value></value>
    <name>Set in project.config configuration</name>
    </Parameter>
    <Parameter>
    <key>applicationUserId</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>billingSystem</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>cancelReason</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>channelID</key>
    <value></value>
    <name>Set in project.config configuration</name>
    </Parameter>
    <Parameter>
    <key>clarifyingReason</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>classificationCode</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>content</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>countRS</key>
    <value></value>
    <name>Set 1st in Parse Text as Response</name>
    </Parameter>
    <Parameter>
    <key>deviceBrandId</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>deviceHistoryIndicator</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>emailAddress</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>financialAccountNumber</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>language</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>lisa.AttachRequest.rsp</key>
    <value></value>
    <name>Set 1st in AttachRequest</name>
    </Parameter>
    <Parameter>
    <key>lisa.AttachRequest.rsp.time</key>
    <value></value>
    <name>Set 1st in AttachRequest</name>
    </Parameter>
    <Parameter>
    <key>lisa.AttachResponse.rsp</key>
    <value></value>
    <name>Set 1st in AttachResponse</name>
    </Parameter>
    <Parameter>
    <key>lisa.AttachResponse.rsp.time</key>
    <value></value>
    <name>Set 1st in AttachResponse</name>
    </Parameter>
    <Parameter>
    <key>lisa.Authenticate.rsp</key>
    <value></value>
    <name>Set 1st in Authenticate</name>
    </Parameter>
    <Parameter>
    <key>lisa.Authenticate.rsp.time</key>
    <value></value>
    <name>Set 1st in Authenticate</name>
    </Parameter>
    <Parameter>
    <key>lisa.GetRunSteps.rsp</key>
    <value></value>
    <name>Set 1st in GetRunSteps</name>
    </Parameter>
    <Parameter>
    <key>lisa.GetRunSteps.rsp.time</key>
    <value></value>
    <name>Set 1st in GetRunSteps</name>
    </Parameter>
    <Parameter>
    <key>lisa.Parse Text as Response.rsp</key>
    <value></value>
    <name>Set 1st in Parse Text as Response</name>
    </Parameter>
    <Parameter>
    <key>lisa.Parse Text as Response.rsp.time</key>
    <value></value>
    <name>Set 1st in Parse Text as Response</name>
    </Parameter>
    <Parameter>
    <key>lisa.Read_Request.rsp</key>
    <value></value>
    <name>Set 1st in Read_Request</name>
    </Parameter>
    <Parameter>
    <key>lisa.Read_Request.rsp.time</key>
    <value></value>
    <name>Set 1st in Read_Request</name>
    </Parameter>
    <Parameter>
    <key>lisa.Read_Response.rsp</key>
    <value></value>
    <name>Set 1st in Read_Response</name>
    </Parameter>
    <Parameter>
    <key>lisa.Read_Response.rsp.time</key>
    <value></value>
    <name>Set 1st in Read_Response</name>
    </Parameter>
    <Parameter>
    <key>lisa.Sign Out.rsp</key>
    <value></value>
    <name>Set 1st in Sign Out</name>
    </Parameter>
    <Parameter>
    <key>lisa.Sign Out.rsp.time</key>
    <value></value>
    <name>Set 1st in Sign Out</name>
    </Parameter>
    <Parameter>
    <key>lisa.UpdateRun-Passed.rsp</key>
    <value></value>
    <name>Set 1st in UpdateRun-Passed</name>
    </Parameter>
    <Parameter>
    <key>lisa.UpdateRun-Passed.rsp.time</key>
    <value></value>
    <name>Set 1st in UpdateRun-Passed</name>
    </Parameter>
    <Parameter>
    <key>lisa.UpdateRunSteps.rsp</key>
    <value></value>
    <name>Set 1st in UpdateRunSteps</name>
    </Parameter>
    <Parameter>
    <key>lisa.UpdateRunSteps.rsp.time</key>
    <value></value>
    <name>Set 1st in UpdateRunSteps</name>
    </Parameter>
    <Parameter>
    <key>lisa.UpdateTestStatus-NoRun.rsp</key>
    <value></value>
    <name>Set 1st in UpdateTestStatus-NoRun</name>
    </Parameter>
    <Parameter>
    <key>lisa.UpdateTestStatus-NoRun.rsp.time</key>
    <value></value>
    <name>Set 1st in UpdateTestStatus-NoRun</name>
    </Parameter>
    <Parameter>
    <key>lisa.UpdateTestStatus-Passed.rsp</key>
    <value></value>
    <name>Set 1st in UpdateTestStatus-Passed</name>
    </Parameter>
    <Parameter>
    <key>lisa.UpdateTestStatus-Passed.rsp.time</key>
    <value></value>
    <name>Set 1st in UpdateTestStatus-Passed</name>
    </Parameter>
    <Parameter>
    <key>lisa.createTestInstance.rsp</key>
    <value></value>
    <name>Set 1st in createTestInstance</name>
    </Parameter>
    <Parameter>
    <key>lisa.createTestInstance.rsp.time</key>
    <value></value>
    <name>Set 1st in createTestInstance</name>
    </Parameter>
    <Parameter>
    <key>lisa.createTestSet.rsp</key>
    <value></value>
    <name>Set 1st in createTestSet</name>
    </Parameter>
    <Parameter>
    <key>lisa.createTestSet.rsp.time</key>
    <value></value>
    <name>Set 1st in createTestSet</name>
    </Parameter>
    <Parameter>
    <key>lisa.getQCcookie.rsp</key>
    <value></value>
    <name>Set 1st in getQCcookie</name>
    </Parameter>
    <Parameter>
    <key>lisa.getQCcookie.rsp.time</key>
    <value></value>
    <name>Set 1st in getQCcookie</name>
    </Parameter>
    <Parameter>
    <key>lisa.getRUN-ID.rsp</key>
    <value></value>
    <name>Set 1st in getRUN-ID</name>
    </Parameter>
    <Parameter>
    <key>lisa.getRUN-ID.rsp.time</key>
    <value></value>
    <name>Set 1st in getRUN-ID</name>
    </Parameter>
    <Parameter>
    <key>lisa.getTestCycleID.rsp</key>
    <value></value>
    <name>Set 1st in getTestCycleID</name>
    </Parameter>
    <Parameter>
    <key>lisa.getTestCycleID.rsp.time</key>
    <value></value>
    <name>Set 1st in getTestCycleID</name>
    </Parameter>
    <Parameter>
    <key>lisa.getTestID.rsp</key>
    <value></value>
    <name>Set 1st in getTestID</name>
    </Parameter>
    <Parameter>
    <key>lisa.getTestID.rsp.time</key>
    <value></value>
    <name>Set 1st in getTestID</name>
    </Parameter>
    <Parameter>
    <key>lisa.getTestInstance.rsp</key>
    <value></value>
    <name>Set 1st in getTestInstance</name>
    </Parameter>
    <Parameter>
    <key>lisa.getTestInstance.rsp.time</key>
    <value></value>
    <name>Set 1st in getTestInstance</name>
    </Parameter>
    <Parameter>
    <key>lisa.getTestSetFolder-NoTestSetPath.rsp</key>
    <value></value>
    <name>Set 1st in getTestSetFolder-NoTestSetPath</name>
    </Parameter>
    <Parameter>
    <key>lisa.getTestSetFolder-NoTestSetPath.rsp.time</key>
    <value></value>
    <name>Set 1st in getTestSetFolder-NoTestSetPath</name>
    </Parameter>
    <Parameter>
    <key>lisa.getTestSetFolder-NoTestSetPath1.rsp</key>
    <value></value>
    <name>Set 1st in getTestSetFolder-NoTestSetPath1</name>
    </Parameter>
    <Parameter>
    <key>lisa.getTestSetFolder-NoTestSetPath1.rsp.time</key>
    <value></value>
    <name>Set 1st in getTestSetFolder-NoTestSetPath1</name>
    </Parameter>
    <Parameter>
    <key>lisa.getTestSetFolder.rsp</key>
    <value></value>
    <name>Set 1st in getTestSetFolder</name>
    </Parameter>
    <Parameter>
    <key>lisa.getTestSetFolder.rsp.time</key>
    <value></value>
    <name>Set 1st in getTestSetFolder</name>
    </Parameter>
    <Parameter>
    <key>lisa.getTestSetFolderParent-NoTestSetPath.rsp</key>
    <value></value>
    <name>Set 1st in getTestSetFolderParent-NoTestSetPath</name>
    </Parameter>
    <Parameter>
    <key>lisa.getTestSetFolderParent-NoTestSetPath.rsp.time</key>
    <value></value>
    <name>Set 1st in getTestSetFolderParent-NoTestSetPath</name>
    </Parameter>
    <Parameter>
    <key>lisa.getTestSetFolderParent.rsp</key>
    <value></value>
    <name>Set 1st in getTestSetFolderParent</name>
    </Parameter>
    <Parameter>
    <key>lisa.getTestSetFolderParent.rsp.time</key>
    <value></value>
    <name>Set 1st in getTestSetFolderParent</name>
    </Parameter>
    <Parameter>
    <key>lisa.getTestSetFolders.rsp</key>
    <value></value>
    <name>Set 1st in getTestSetFolders</name>
    </Parameter>
    <Parameter>
    <key>lisa.getTestSetFolders.rsp.time</key>
    <value></value>
    <name>Set 1st in getTestSetFolders</name>
    </Parameter>
    <Parameter>
    <key>lisa.getTestSetSearch.rsp</key>
    <value></value>
    <name>Set 1st in getTestSetSearch</name>
    </Parameter>
    <Parameter>
    <key>lisa.getTestSetSearch.rsp.time</key>
    <value></value>
    <name>Set 1st in getTestSetSearch</name>
    </Parameter>
    <Parameter>
    <key>lisa.getTestSets.rsp</key>
    <value></value>
    <name>Set 1st in getTestSets</name>
    </Parameter>
    <Parameter>
    <key>lisa.getTestSets.rsp.time</key>
    <value></value>
    <name>Set 1st in getTestSets</name>
    </Parameter>
    <Parameter>
    <key>lisa.testSetFolderSearch.rsp</key>
    <value></value>
    <name>Set 1st in testSetFolderSearch</name>
    </Parameter>
    <Parameter>
    <key>lisa.testSetFolderSearch.rsp.time</key>
    <value></value>
    <name>Set 1st in testSetFolderSearch</name>
    </Parameter>
    <Parameter>
    <key>lisa.testSetFolderSearch~1.rsp</key>
    <value></value>
    <name>Set 1st in testSetFolderSearch~1</name>
    </Parameter>
    <Parameter>
    <key>lisa.testSetFolderSearch~1.rsp.time</key>
    <value></value>
    <name>Set 1st in testSetFolderSearch~1</name>
    </Parameter>
    <Parameter>
    <key>lisa.vse.execution.mode</key>
    <value></value>
    <name>Set in project.config configuration</name>
    </Parameter>
    <Parameter>
    <key>manufacturerName</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>noteType</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>notifybyEmail</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>orderId</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>orderIdNonExistant</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>orderTime</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>orderType</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>portRequestID</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>postalCode</key>
    <value></value>
    <name>Set in project.config configuration</name>
    </Parameter>
    <Parameter>
    <key>prepostIndicator</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>requestReason</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>requestType</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>runCount</key>
    <value></value>
    <name>Set 1st in getRUN-ID</name>
    </Parameter>
    <Parameter>
    <key>runID</key>
    <value></value>
    <name>Set 1st in getRUN-ID</name>
    </Parameter>
    <Parameter>
    <key>runStepCount</key>
    <value></value>
    <name>Set 1st in GetRunSteps</name>
    </Parameter>
    <Parameter>
    <key>runStepID</key>
    <value></value>
    <name>Set 1st in Parse Text as Response</name>
    </Parameter>
    <Parameter>
    <key>salesChannelCode</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>salesPerson</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>senderID</key>
    <value></value>
    <name>Set in project.config configuration</name>
    </Parameter>
    <Parameter>
    <key>simType</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>storeId</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>subTypeCode</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>testConfigID</key>
    <value></value>
    <name>Set 1st in getTestCycleID</name>
    </Parameter>
    <Parameter>
    <key>testCycleID</key>
    <value></value>
    <name>Set 1st in getTestCycleID</name>
    </Parameter>
    <Parameter>
    <key>testID</key>
    <value></value>
    <name>Set 1st in getTestID</name>
    </Parameter>
    <Parameter>
    <key>testInstanceID</key>
    <value></value>
    <name>Set 1st in getTestInstance</name>
    </Parameter>
    <Parameter>
    <key>testSetCountCaptured</key>
    <value></value>
    <name>Set 1st in getTestSets</name>
    </Parameter>
    <Parameter>
    <key>testSetFolderID</key>
    <value></value>
    <name>Set 1st in getTestSets</name>
    </Parameter>
    <Parameter>
    <key>testSetFolderParentID</key>
    <value></value>
    <name>Set 1st in getTestSetFolders</name>
    </Parameter>
    <Parameter>
    <key>testSetFoldersCountCaptured</key>
    <value></value>
    <name>Set 1st in testSetFolderSearch</name>
    </Parameter>
    <Parameter>
    <key>testSetID</key>
    <value></value>
    <name>Set 1st in getTestSets</name>
    </Parameter>
    <Parameter>
    <key>tmoOMSE2ETestDataExcelFile</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>tmoOMSGetOrderDetailsRequest</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>tmoOMSGetOrderDetailsRequestXmlFile</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>tmoOMSLoginRequest</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>tmoOMSManageOrderRequestXmlFile</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>tmoOMSManageOrderServiceRequest</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>tmoOMSSubmitOrderRequestXmlFile</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>tmoOMSSubmitOrderService</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>typeCode</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>unlockType</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>unlockableRemotely</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
    <Parameter>
    <key>zeroIMEI</key>
    <value></value>
    <name>Set in Dev_Environment.config configuration</name>
    </Parameter>
</resultState>

<deletedProps>
</deletedProps>

    <Node name="Authenticate" log=""
          type="com.itko.lisa.ws.rest.RESTNode" 
          version="3" 
          uid="26C5426F54BE11E6AF0AB23320524153" 
          think="500-1S" 
          useFilters="true" 
          quiet="false" 
          next="getQCcookie" > 


      <!-- Filters -->
      <Filter type="com.itko.lisa.test.FilterGetHTTPCookieHeader">
        <valueToFilterKey>lisa.Authenticate.rsp</valueToFilterKey>
      <propPrefix></propPrefix>
      </Filter>

<url>https://qualitycenter.internal.t-mobile.com:8443/qcbin/authentication-point/authenticate</url>
<data-type>text</data-type>
      <header field="Authorization" value="{{Authorization}}" />
<httpMethod>GET</httpMethod>
<onError>abort</onError>
    </Node>


    <Node name="getQCcookie" log=""
          type="com.itko.lisa.ws.rest.RESTNode" 
          version="3" 
          uid="26C5427054BE11E6AF0AB23320524153" 
          think="500-1S" 
          useFilters="true" 
          quiet="false" 
          next="getTestSets" > 


      <!-- Filters -->
      <Filter type="com.itko.lisa.test.FilterGetHTTPCookieHeader">
        <valueToFilterKey>lisa.getQCcookie.rsp</valueToFilterKey>
      <propPrefix></propPrefix>
      </Filter>


      <!-- Assertions -->
<CheckResult assertTrue="false" name="Scripted Assertion" type="com.itko.lisa.test.AssertByScript">
<log>Assertion name: Scripted Assertion checks for: false  is of type: Assert by Script Execution.</log>
<then>continue (quiet)</then>
<valueToAssertKey></valueToAssertKey>
        <script>// This Beanshell script should return a boolean result indicating the assertion is true or false&#13;&#10;testExec.setStateValue(&quot;testSetsCount&quot;,1);&#13;&#10;testExec.setStateValue(&quot;testSetFoldersCount&quot;,1);&#13;&#10;&#13;&#10;return (true);</script>
        <language>BeanShell</language>
        <copyprops>TestExecPropsAndSystemProps</copyprops>
</CheckResult>

<url>https://qualitycenter.internal.t-mobile.com:8443/qcbin/rest/site-session</url>
<data-type>text</data-type>
      <header field="LWSSO_COOKIE_KEY" value="{{LWSSO_COOKIE_KEY}}" />
<httpMethod>POST</httpMethod>
<onError>abort</onError>
    </Node>


    <Node name="getTestSets" log=""
          type="com.itko.lisa.ws.rest.RESTNode" 
          version="3" 
          uid="26C5427154BE11E6AF0AB23320524153" 
          think="500-1S" 
          useFilters="true" 
          quiet="false" 
          next="getTestSetFolder" > 


      <!-- Filters -->
      <Filter type="com.itko.lisa.xml.FilterXMLXPath">
        <valueToFilterKey>lisa.getTestSets.rsp</valueToFilterKey>
<prop>testSetCountCaptured</prop>
<xpathq>count(//Entity)</xpathq>
      </Filter>

      <Filter type="com.itko.lisa.xml.FilterXMLXPath">
        <valueToFilterKey>lisa.getTestSets.rsp</valueToFilterKey>
<prop>testSetID</prop>
<xpathq>//Entity[{{testSetsCount}}]/Fields/Field[@Name=&quot;id&quot;]/Value/text()</xpathq>
      </Filter>

      <Filter type="com.itko.lisa.xml.FilterXMLXPath">
        <valueToFilterKey>lisa.getTestSets.rsp</valueToFilterKey>
<prop>testSetFolderID</prop>
<xpathq>//Entity[{{testSetsCount}}]/Fields/Field[@Name=&quot;parent-id&quot;]/Value/text()</xpathq>
      </Filter>


      <!-- Assertions -->
<CheckResult assertTrue="true" name="testSetCountCaptured=0" type="com.itko.lisa.test.CheckResultPropRegEx">
<log>Assertion name: testSetCountCaptured=0 checks for: true  is of type: Property Value Expression.</log>
<then>testSetFolderSearch</then>
<valueToAssertKey></valueToAssertKey>
        <prop>testSetCountCaptured</prop>
        <param>0</param>
</CheckResult>

<CheckResult assertTrue="true" name="LoopEnds" type="com.itko.lisa.test.AssertByScript">
<log>Assertion name: LoopEnds checks for: true  is of type: Assert by Script Execution.</log>
<then>testSetFolderSearch</then>
<valueToAssertKey></valueToAssertKey>
        <script>import java.lang.*;&#13;&#10;import java.io.*;&#13;&#10;&#13;&#10;int testSetLoopedCount = testExec.getStateValue(&quot;testSetsCount&quot;);&#13;&#10;//int testSetCapCount = testExec.getStateValue(&quot;testSetCountCaptured&quot;);&#13;&#10;int testSetCapCount = {{testSetCountCaptured}};&#13;&#10;if (testSetLoopedCount &gt; testSetCapCount){ return true;}&#13;&#10;&#13;&#10;    </script>
        <language>beanshell</language>
        <copyprops>TestExecPropsAndSystemProps</copyprops>
</CheckResult>

<url>https://qualitycenter.internal.t-mobile.com:8443/qcbin/rest/domains/{{DomainName}}/projects/{{ProjectName}}/test-sets?query=%7Bname%5B%27{{TestSetName}}%27%5D%7D&amp;order-by=%7Bid%5BDESC%5D%7D</url>
<data-type>text</data-type>
      <header field="LWSSO_COOKIE_KEY" value="{{LWSSO_COOKIE_KEY}}" />
      <header field="QCSession" value="{{QCSession}}" />
<httpMethod>GET</httpMethod>
<onError>abort</onError>
    </Node>


    <Node name="getTestSetFolder" log=""
          type="com.itko.lisa.ws.rest.RESTNode" 
          version="3" 
          uid="26C5427254BE11E6AF0AB23320524153" 
          think="500-1S" 
          useFilters="true" 
          quiet="false" 
          next="getTestSetFolder" > 


      <!-- Filters -->
      <Filter type="com.itko.lisa.xml.FilterXMLXPath">
        <valueToFilterKey>lisa.getTestSetFolder.rsp</valueToFilterKey>
<prop>testSetFolderID</prop>
<xpathq>//Field[@Name=&quot;parent-id&quot;]/Value/text()</xpathq>
      </Filter>

      <Filter type="com.itko.lisa.xml.FilterXMLXPath">
        <valueToFilterKey>lisa.getTestSetFolder.rsp</valueToFilterKey>
<prop>FolderName</prop>
<xpathq>//Field[@Name=&quot;name&quot;]/Value/text()</xpathq>
      </Filter>


      <!-- Assertions -->
<CheckResult assertTrue="true" name="testSetFolderName" type="com.itko.lisa.xml.AssertXMLXPath">
<log>Assertion name: testSetFolderParentName checks for: true  is of type: XML XPath Assert.</log>
<then>getTestSetFolderParent</then>
<valueToAssertKey></valueToAssertKey>
<xpathq>//Field[@Name=&quot;name&quot;]/Value/text() = &quot;{{CodeDropFolderName}}&quot;</xpathq>
</CheckResult>

<CheckResult assertTrue="true" name="reachesRoot" type="com.itko.lisa.test.AssertByScript">
<log>Assertion name: Scripted Assertion~1 checks for: true  is of type: Assert by Script Execution.</log>
<then>getTestSets</then>
<valueToAssertKey></valueToAssertKey>
        <script>import java.lang.*;&#13;&#10;import java.io.*;&#13;&#10;&#13;&#10;String folderNameCap = testExec.getStateValue(&quot;FolderName&quot;).toString();&#13;&#10;System.out.println(&quot;------&quot;+folderNameCap);&#13;&#10;if (folderNameCap.equals(&quot;Root&quot;))&#13;&#10;{&#13;&#10;    &#13;&#10;    int currentFoldersCount = testExec.getStateValue(&quot;testSetsCount&quot;);&#13;&#10;    currentFoldersCount++;&#13;&#10;    System.out.println(&quot;in side loop------&quot;+currentFoldersCount);&#13;&#10;        testExec.setStateValue(&quot;testSetsCount&quot;, currentFoldersCount);&#13;&#10;        return true;&#13;&#10;}</script>
        <language>beanshell</language>
        <copyprops>TestExecPropsAndSystemProps</copyprops>
</CheckResult>

<url>https://qualitycenter.internal.t-mobile.com:8443/qcbin/rest/domains/{{DomainName}}/projects/{{ProjectName}}/test-set-folders/{{testSetFolderID}}</url>
<data-type>text</data-type>
      <header field="LWSSO_COOKIE_KEY" value="{{LWSSO_COOKIE_KEY}}" />
      <header field="QCSession" value="{{QCSession}}" />
<httpMethod>GET</httpMethod>
<onError>abort</onError>
    </Node>


    <Node name="testSetFolderSearch" log=""
          type="com.itko.lisa.ws.rest.RESTNode" 
          version="3" 
          uid="26C5427354BE11E6AF0AB23320524153" 
          think="500-1S" 
          useFilters="true" 
          quiet="false" 
          next="getTestSetFolder-NoTestSetPath" > 


      <!-- Filters -->
      <Filter type="com.itko.lisa.xml.FilterXMLXPath">
        <valueToFilterKey>lisa.testSetFolderSearch.rsp</valueToFilterKey>
<prop>testSetFolderParentID</prop>
<xpathq>//Entity[{{testSetFoldersCount}}]/Fields/Field[@Name=&quot;parent-id&quot;]/Value/text()</xpathq>
      </Filter>

      <Filter type="com.itko.lisa.xml.FilterXMLXPath">
        <valueToFilterKey>lisa.testSetFolderSearch.rsp</valueToFilterKey>
<prop>testSetFolderID</prop>
<xpathq>//Entity[{{testSetFoldersCount}}]/Fields/Field[@Name=&quot;id&quot;]/Value/text()</xpathq>
      </Filter>

      <Filter type="com.itko.lisa.xml.FilterXMLXPath">
        <valueToFilterKey>lisa.testSetFolderSearch.rsp</valueToFilterKey>
<prop>testSetFoldersCountCaptured</prop>
<xpathq>count(//Entity)</xpathq>
      </Filter>


      <!-- Assertions -->
<CheckResult assertTrue="true" name="LoopEnds" type="com.itko.lisa.test.AssertByScript">
<log>Assertion name: LoopEnds checks for: true  is of type: Assert by Script Execution.</log>
<then>end</then>
<valueToAssertKey></valueToAssertKey>
        <script>import java.lang.*;&#13;&#10;import java.io.*;&#13;&#10;&#13;&#10;int testSetLoopedCount = testExec.getStateValue(&quot;testSetFoldersCount&quot;);&#13;&#10;int testSetCapCount = {{testSetFoldersCountCaptured}};&#13;&#10;if (testSetLoopedCount &gt; testSetCapCount){ return true;}</script>
        <language>beanshell</language>
        <copyprops>TestExecPropsAndSystemProps</copyprops>
</CheckResult>

<url>https://qualitycenter.internal.t-mobile.com:8443/qcbin/rest/domains/{{DomainName}}/projects/{{ProjectName}}/test-set-folders?query=%7Bname%5B%27{{TestSetFolderName}}%27%5D%7D&amp;order-by=%7Bid%5BDESC%5D%7D</url>
<data-type>text</data-type>
      <header field="LWSSO_COOKIE_KEY" value="{{LWSSO_COOKIE_KEY}}" />
      <header field="QCSession" value="{{QCSession}}" />
<httpMethod>GET</httpMethod>
<onError>abort</onError>
    </Node>


    <Node name="getTestSetFolder-NoTestSetPath" log=""
          type="com.itko.lisa.ws.rest.RESTNode" 
          version="3" 
          uid="26C5427454BE11E6AF0AB23320524153" 
          think="500-1S" 
          useFilters="true" 
          quiet="false" 
          next="getTestSetFolder-NoTestSetPath" > 


      <!-- Filters -->
      <Filter type="com.itko.lisa.xml.FilterXMLXPath">
        <valueToFilterKey>lisa.getTestSetFolder-NoTestSetPath.rsp</valueToFilterKey>
<prop>testSetFolderParentID</prop>
<xpathq>//Field[@Name=&quot;parent-id&quot;]/Value/text()</xpathq>
      </Filter>

      <Filter type="com.itko.lisa.xml.FilterXMLXPath">
        <valueToFilterKey>lisa.getTestSetFolder-NoTestSetPath.rsp</valueToFilterKey>
<prop>FolderName</prop>
<xpathq>//Field[@Name=&quot;name&quot;]/Value/text()</xpathq>
      </Filter>


      <!-- Assertions -->
<CheckResult assertTrue="true" name="testSetFolderName" type="com.itko.lisa.xml.AssertXMLXPath">
<log>Assertion name: testSetFolderParentName checks for: true  is of type: XML XPath Assert.</log>
<then>getTestSetFolderParent-NoTestSetPath</then>
<valueToAssertKey></valueToAssertKey>
<xpathq>//Field[@Name=&quot;name&quot;]/Value/text() = &quot;{{CodeDropFolderName}}&quot;</xpathq>
</CheckResult>

<CheckResult assertTrue="true" name="reachesRoot1" type="com.itko.lisa.test.AssertByScript">
<log>Assertion name: Scripted Assertion~1 checks for: true  is of type: Assert by Script Execution.</log>
<then>testSetFolderSearch</then>
<valueToAssertKey></valueToAssertKey>
        <script>import java.lang.*;&#13;&#10;import java.io.*;&#13;&#10;&#13;&#10;String folderNameCap = testExec.getStateValue(&quot;FolderName&quot;).toString();&#13;&#10;if (folderNameCap.equals(&quot;Root&quot;))&#13;&#10;{&#13;&#10;    int currentFoldersCount = testExec.getStateValue(&quot;testSetFoldersCount&quot;);&#13;&#10;    currentFoldersCount++;&#13;&#10;    System.out.println(&quot;in side loop------&quot;+currentFoldersCount);&#13;&#10;    testExec.setStateValue(&quot;testSetFoldersCount&quot;, currentFoldersCount);&#13;&#10;    return true;&#13;&#10;}</script>
        <language>beanshell</language>
        <copyprops>TestExecPropsAndSystemProps</copyprops>
</CheckResult>

<url>https://qualitycenter.internal.t-mobile.com:8443/qcbin/rest/domains/{{DomainName}}/projects/{{ProjectName}}/test-set-folders/{{testSetFolderParentID}}</url>
<data-type>text</data-type>
      <header field="LWSSO_COOKIE_KEY" value="{{LWSSO_COOKIE_KEY}}" />
      <header field="QCSession" value="{{QCSession}}" />
<httpMethod>GET</httpMethod>
<onError>abort</onError>
    </Node>


    <Node name="getTestSetFolderParent" log=""
          type="com.itko.lisa.ws.rest.RESTNode" 
          version="3" 
          uid="26C5427554BE11E6AF0AB23320524153" 
          think="500-1S" 
          useFilters="true" 
          quiet="false" 
          next="getTestSets" > 


      <!-- Filters -->
      <Filter type="com.itko.lisa.xml.FilterXMLXPath">
        <valueToFilterKey>lisa.getTestSetFolderParent.rsp</valueToFilterKey>
<prop>testSetFolderID</prop>
<xpathq>//Field[@Name=&quot;parent-id&quot;]/Value/text()</xpathq>
      </Filter>

      <Filter type="com.itko.lisa.xml.FilterXMLXPath">
        <valueToFilterKey>lisa.getTestSetFolderParent.rsp</valueToFilterKey>
<prop>FolderName</prop>
<xpathq>//Field[@Name=&quot;name&quot;]/Value/text()</xpathq>
      </Filter>


      <!-- Assertions -->
<CheckResult assertTrue="true" name="testSetFolderParentName" type="com.itko.lisa.xml.AssertXMLXPath">
<log>Assertion name: testSetFolderParentName checks for: true  is of type: XML XPath Assert.</log>
<then>getTestID</then>
<valueToAssertKey></valueToAssertKey>
<xpathq>//Field[@Name=&quot;name&quot;]/Value/text() = &quot;{{ReleaseFolderName}}&quot;</xpathq>
</CheckResult>

<CheckResult assertTrue="true" name="increaseLoopCount" type="com.itko.lisa.test.AssertByScript">
<log>Assertion name: Scripted Assertion~1 checks for: true  is of type: Assert by Script Execution.</log>
<then>getTestSets</then>
<valueToAssertKey></valueToAssertKey>
        <script>&#13;&#10;int currenttestSetCount = testExec.getStateValue(&quot;testSetsCount&quot;)+1;&#13;&#10;int testSetCountCaptured = {{testSetCountCaptured}};&#13;&#10;testExec.setStateValue(&quot;testSetsCount&quot;, currenttestSetCount);&#13;&#10;return true;&#13;&#10;&#13;&#10;</script>
        <language>beanshell</language>
        <copyprops>TestExecPropsAndSystemProps</copyprops>
</CheckResult>

<url>https://qualitycenter.internal.t-mobile.com:8443/qcbin/rest/domains/{{DomainName}}/projects/{{ProjectName}}/test-set-folders/{{testSetFolderID}}</url>
<data-type>text</data-type>
      <header field="LWSSO_COOKIE_KEY" value="{{LWSSO_COOKIE_KEY}}" />
      <header field="QCSession" value="{{QCSession}}" />
<httpMethod>GET</httpMethod>
<onError>abort</onError>
    </Node>


    <Node name="getTestSetFolderParent-NoTestSetPath" log=""
          type="com.itko.lisa.ws.rest.RESTNode" 
          version="3" 
          uid="26C5427654BE11E6AF0AB23320524153" 
          think="500-1S" 
          useFilters="true" 
          quiet="false" 
          next="testSetFolderSearch" > 


      <!-- Filters -->
      <Filter type="com.itko.lisa.xml.FilterXMLXPath">
        <valueToFilterKey>lisa.getTestSetFolderParent-NoTestSetPath.rsp</valueToFilterKey>
<prop>testSetFolderParentID</prop>
<xpathq>//Field[@Name=&quot;parent-id&quot;]/Value/text()</xpathq>
      </Filter>

      <Filter type="com.itko.lisa.xml.FilterXMLXPath">
        <valueToFilterKey>lisa.getTestSetFolderParent-NoTestSetPath.rsp</valueToFilterKey>
<prop>FolderName</prop>
<xpathq>//Field[@Name=&quot;name&quot;]/Value/text()</xpathq>
      </Filter>


      <!-- Assertions -->
<CheckResult assertTrue="true" name="testSetFolderParentName" type="com.itko.lisa.xml.AssertXMLXPath">
<log>Assertion name: testSetFolderParentName checks for: true  is of type: XML XPath Assert.</log>
<then>createTestSet</then>
<valueToAssertKey></valueToAssertKey>
<xpathq>//Field[@Name=&quot;name&quot;]/Value/text() = &quot;{{ReleaseFolderName}}&quot;</xpathq>
</CheckResult>

<CheckResult assertTrue="true" name="IncreaseFolderLoopCount" type="com.itko.lisa.test.AssertByScript">
<log>Assertion name: Scripted Assertion~1 checks for: true  is of type: Assert by Script Execution.</log>
<then>testSetFolderSearch</then>
<valueToAssertKey></valueToAssertKey>
        <script>int currenttestSetFolderCount = testExec.getStateValue(&quot;testSetFoldersCount&quot;)+1;&#13;&#10;testExec.setStateValue(&quot;testSetFoldersCount&quot;, currenttestSetFolderCount);&#13;&#10;return true;     &#13;&#10;&#13;&#10;</script>
        <language>BeanShell</language>
        <copyprops>TestExecPropsAndSystemProps</copyprops>
</CheckResult>

<url>https://qualitycenter.internal.t-mobile.com:8443/qcbin/rest/domains/{{DomainName}}/projects/{{ProjectName}}/test-set-folders/{{testSetFolderParentID}}</url>
<data-type>text</data-type>
      <header field="LWSSO_COOKIE_KEY" value="{{LWSSO_COOKIE_KEY}}" />
      <header field="QCSession" value="{{QCSession}}" />
<httpMethod>GET</httpMethod>
<onError>abort</onError>
    </Node>


    <Node name="getTestID" log=""
          type="com.itko.lisa.ws.rest.RESTNode" 
          version="3" 
          uid="26C5427754BE11E6AF0AB23320524153" 
          think="500-1S" 
          useFilters="true" 
          quiet="false" 
          next="getTestCycleID" > 


      <!-- Filters -->
      <Filter type="com.itko.lisa.xml.FilterXMLXPath">
        <valueToFilterKey>lisa.getTestID.rsp</valueToFilterKey>
<prop>testID</prop>
<xpathq>//Entity[1]/Fields/Field[@Name=&quot;id&quot;]/Value/text()</xpathq>
      </Filter>


      <!-- Data Sets -->
<readrec>Read Rows from a Delimited Data File</readrec>
<url>https://qualitycenter.internal.t-mobile.com:8443/qcbin/rest/domains/{{DomainName}}/projects/{{ProjectName}}/tests?query=%7Bname%5B%27{{TestCase_Name}}%27%5D%7D</url>
<data-type>text</data-type>
      <header field="LWSSO_COOKIE_KEY" value="{{LWSSO_COOKIE_KEY}}" />
      <header field="QCSession" value="{{QCSession}}" />
<httpMethod>GET</httpMethod>
<onError>abort</onError>
    </Node>


    <Node name="getTestCycleID" log=""
          type="com.itko.lisa.ws.rest.RESTNode" 
          version="3" 
          uid="26C5427854BE11E6AF0AB23320524153" 
          think="500-1S" 
          useFilters="true" 
          quiet="false" 
          next="getTestInstance" > 


      <!-- Filters -->
      <Filter type="com.itko.lisa.xml.FilterXMLXPath">
        <valueToFilterKey>lisa.getTestCycleID.rsp</valueToFilterKey>
<prop>testCycleID</prop>
<xpathq>//Entity[1]/Fields/Field[@Name=&quot;id&quot;]/Value/text()</xpathq>
      </Filter>

      <Filter type="com.itko.lisa.xml.FilterXMLXPath">
        <valueToFilterKey>lisa.getTestCycleID.rsp</valueToFilterKey>
<prop>testConfigID</prop>
<xpathq>//Entity[1]/Fields/Field[@Name=&quot;test-config-id&quot;]/Value/text()</xpathq>
      </Filter>

<url>https://qualitycenter.internal.t-mobile.com:8443/qcbin/rest/domains/{{DomainName}}/projects/{{ProjectName}}/test-instances?query=%7Btest-id%5B{{testID}}%5D%7D</url>
<data-type>text</data-type>
      <header field="LWSSO_COOKIE_KEY" value="{{LWSSO_COOKIE_KEY}}" />
      <header field="QCSession" value="{{QCSession}}" />
<httpMethod>GET</httpMethod>
<onError>abort</onError>
    </Node>


    <Node name="getTestInstance" log=""
          type="com.itko.lisa.ws.rest.RESTNode" 
          version="3" 
          uid="26C5427954BE11E6AF0AB23320524153" 
          think="500-1S" 
          useFilters="true" 
          quiet="false" 
          next="createTestInstance" > 


      <!-- Filters -->
      <Filter type="com.itko.lisa.xml.FilterXMLXPath">
        <valueToFilterKey>lisa.getTestInstance.rsp</valueToFilterKey>
<prop>testInstanceID</prop>
<xpathq>//Field[@Name=&quot;id&quot;]/Value/text()</xpathq>
      </Filter>


      <!-- Assertions -->
<CheckResult assertTrue="true" name="testInstanceStatusPassed" type="com.itko.lisa.xml.AssertXMLXPath">
<log>Assertion name: testInstanceIDExists checks for: true  is of type: XML XPath Assert.</log>
<then>UpdateTestStatus-NoRun</then>
<valueToAssertKey></valueToAssertKey>
<xpathq>//Field[@Name=&quot;status&quot;]/Value/text() = &quot;Passed&quot;</xpathq>
</CheckResult>

<CheckResult assertTrue="true" name="testInstanceStatusFailed" type="com.itko.lisa.xml.AssertXMLXPath">
<log>Assertion name: testInstanceStatusFailed checks for: true  is of type: XML XPath Assert.</log>
<then>UpdateTestStatus-NoRun</then>
<valueToAssertKey></valueToAssertKey>
<xpathq>//Field[@Name=&quot;status&quot;]/Value/text() = &quot;Failed&quot;</xpathq>
</CheckResult>

<CheckResult assertTrue="true" name="testInstanceStatusNoRun" type="com.itko.lisa.xml.AssertXMLXPath">
<log>Assertion name: testInstanceIDExists checks for: true  is of type: XML XPath Assert.</log>
<then>UpdateTestStatus-Passed</then>
<valueToAssertKey></valueToAssertKey>
<xpathq>//Field[@Name=&quot;status&quot;]/Value/text() = &quot;No Run&quot;</xpathq>
</CheckResult>

<url>https://qualitycenter.internal.t-mobile.com:8443/qcbin/rest/domains/{{DomainName}}/projects/{{ProjectName}}/test-instances?query=%7Btest-id%5B{{testID}}%5D%3B+cycle-id%5B{{testSetID}}%5D%7D</url>
<data-type>text</data-type>
      <header field="LWSSO_COOKIE_KEY" value="{{LWSSO_COOKIE_KEY}}" />
      <header field="QCSession" value="{{QCSession}}" />
<httpMethod>GET</httpMethod>
<onError>abort</onError>
    </Node>


    <Node name="createTestInstance" log=""
          type="com.itko.lisa.ws.rest.RESTNode" 
          version="3" 
          uid="26C5427A54BE11E6AF0AB23320524153" 
          think="500-1S" 
          useFilters="true" 
          quiet="false" 
          next="UpdateTestStatus-Passed" > 


      <!-- Filters -->
      <Filter type="com.itko.lisa.xml.FilterXMLXPath">
        <valueToFilterKey>lisa.createTestInstance.rsp</valueToFilterKey>
<prop>testInstanceID</prop>
<xpathq>//Field[@Name=&quot;id&quot;]/Value/text()</xpathq>
      </Filter>

<url>https://qualitycenter.internal.t-mobile.com:8443/qcbin/rest/domains/{{DomainName}}/projects/{{ProjectName}}/test-instances?query=%7Btest-id%5B{{testID}}%5D%7D</url>
<content>&lt;Entity Type=&quot;test-instance&quot;&gt;&#10;&lt;Fields&gt;&#10;&lt;Field Name=&quot;test-instance&quot;&gt;&lt;Value&gt;1&lt;/Value&gt;&lt;/Field&gt;&#10;&lt;Field Name=&quot;cycle-id&quot;&gt;&lt;Value&gt;{{testSetID}}&lt;/Value&gt;&lt;/Field&gt;&#10;&lt;Field Name=&quot;status&quot;&gt;&lt;Value&gt;No Run&lt;/Value&gt;&lt;/Field&gt;&#10;&lt;Field Name=&quot;test-id&quot;&gt;&lt;Value&gt;{{testID}}&lt;/Value&gt;&lt;/Field&gt;&#10;&lt;Field Name=&quot;test-config-id&quot;&gt;&lt;Value&gt;{{testConfigID}}&lt;/Value&gt;&lt;/Field&gt;&#10;&lt;Field Name=&quot;subtype-id&quot;&gt;&lt;Value&gt;hp.qc.test-instance.MANUAL&lt;/Value&gt;&lt;/Field&gt;&#10;&lt;Field Name=&quot;test-order&quot;&gt;&lt;Value&gt;1&lt;/Value&gt;&lt;/Field&gt;&#10;&lt;Field Name=&quot;owner&quot;&gt;&lt;Value&gt;{{LISA_USER}}&lt;/Value&gt;&lt;/Field&gt;&#10;&lt;/Fields&gt;&#10;&lt;/Entity&gt;</content>
<content-type>application/xml</content-type>
<data-type>XML</data-type>
      <header field="LWSSO_COOKIE_KEY" value="{{LWSSO_COOKIE_KEY}}" />
      <header field="QCSession" value="{{QCSession}}" />
<httpMethod>POST</httpMethod>
<onError>abort</onError>
    </Node>


    <Node name="UpdateTestStatus-Passed" log=""
          type="com.itko.lisa.ws.rest.RESTNode" 
          version="3" 
          uid="26C5427B54BE11E6AF0AB23320524153" 
          think="500-1S" 
          useFilters="true" 
          quiet="false" 
          next="getRUN-ID" > 

<url>https://qualitycenter.internal.t-mobile.com:8443/qcbin/rest/domains/{{DomainName}}/projects/{{ProjectName}}/test-instances?query=%7Bid%5B{{testInstanceID}}%5D%7D</url>
<content>&lt;?xml version=&quot;1.0&quot; encoding=&quot;UTF-8&quot; standalone=&quot;yes&quot;?&gt;&#10;&lt;Entity Type=&quot;test-instances&quot;&gt;&#10;&lt;Fields&gt;&#10;&lt;Field Name=&quot;status&quot;&gt;&#10;&lt;Value&gt;{{Status}}&lt;/Value&gt;&#10;&lt;/Field&gt;&#10;&lt;/Fields&gt;&#10;&lt;/Entity&gt;</content>
<content-type>application/xml</content-type>
<data-type>XML</data-type>
      <header field="LWSSO_COOKIE_KEY" value="{{LWSSO_COOKIE_KEY}}" />
      <header field="QCSession" value="{{QCSession}}" />
      <header field="Content-Type" value="application/xml" />
<httpMethod>PUT</httpMethod>
<onError>abort</onError>
    </Node>


    <Node name="getRUN-ID" log=""
          type="com.itko.lisa.ws.rest.RESTNode" 
          version="3" 
          uid="26C5427C54BE11E6AF0AB23320524153" 
          think="500-1S" 
          useFilters="true" 
          quiet="false" 
          next="GetRunSteps" > 


      <!-- Filters -->
      <Filter type="com.itko.lisa.xml.FilterXMLXPath">
        <valueToFilterKey>lisa.getRUN-ID.rsp</valueToFilterKey>
<prop>runCount</prop>
<xpathq>count(//Entity)</xpathq>
      </Filter>

      <Filter type="com.itko.lisa.xml.FilterXMLXPath">
        <valueToFilterKey>lisa.getRUN-ID.rsp</valueToFilterKey>
<prop>runID</prop>
<xpathq>//Entity[1]/Fields/Field[@Name=&quot;id&quot;]/Value/text()</xpathq>
      </Filter>


      <!-- Assertions -->
<CheckResult assertTrue="false" name="Scripted Assertion~2" type="com.itko.lisa.test.AssertByScript">
<log>Assertion name: Scripted Assertion~2 checks for: false  is of type: Assert by Script Execution.</log>
<then>continue (quiet)</then>
<valueToAssertKey></valueToAssertKey>
        <script>// This Beanshell script should return a boolean result indicating the assertion is true or false&#13;&#10;testExec.setStateValue(&quot;runStepLoopStart&quot;,1);&#13;&#10;return (true);</script>
        <language>beanshell</language>
        <copyprops>TestExecPropsAndSystemProps</copyprops>
</CheckResult>

<url>https://qualitycenter.internal.t-mobile.com:8443/qcbin/rest/domains/{{DomainName}}/projects/{{ProjectName}}/runs?query=%7Btest-id%5B{{testID}}%5D%7D&amp;order-by=%7Bid%5BDESC%5D%7D</url>
<data-type>text</data-type>
      <header field="LWSSO_COOKIE_KEY" value="{{LWSSO_COOKIE_KEY}}" />
      <header field="QCSession" value="{{QCSession}}" />
<httpMethod>GET</httpMethod>
<onError>abort</onError>
    </Node>


    <Node name="GetRunSteps" log=""
          type="com.itko.lisa.ws.rest.RESTNode" 
          version="3" 
          uid="26C5427D54BE11E6AF0AB23320524153" 
          think="500-1S" 
          useFilters="true" 
          quiet="false" 
          next="UpdateRunSteps" > 


      <!-- Filters -->
      <Filter type="com.itko.lisa.xml.FilterXMLXPath">
        <valueToFilterKey>lisa.GetRunSteps.rsp</valueToFilterKey>
<prop>runStepCount</prop>
<xpathq>count(//Entity)-1</xpathq>
      </Filter>

      <Filter type="com.itko.lisa.xml.FilterXMLXPath">
        <valueToFilterKey>lisa.GetRunSteps.rsp</valueToFilterKey>
<prop>runStepID</prop>
<xpathq>//Entity[{{runStepLoopStart}}]/Fields/Field[@Name=&quot;id&quot;]/Value/text()</xpathq>
      </Filter>

<url>https://qualitycenter.internal.t-mobile.com:8443/qcbin/rest/domains/{{DomainName}}/projects/{{ProjectName}}/runs/{{runID}}/run-steps</url>
<data-type>text</data-type>
      <header field="LWSSO_COOKIE_KEY" value="{{LWSSO_COOKIE_KEY}}" />
      <header field="QCSession" value="{{QCSession}}" />
<httpMethod>GET</httpMethod>
<onError>abort</onError>
    </Node>


    <Node name="UpdateRunSteps" log=""
          type="com.itko.lisa.ws.rest.RESTNode" 
          version="3" 
          uid="26C5427E54BE11E6AF0AB23320524153" 
          think="500-1S" 
          useFilters="true" 
          quiet="false" 
          next="UpdateRun-Passed" > 


      <!-- Assertions -->
<CheckResult assertTrue="true" name="Scripted Assertion~3" type="com.itko.lisa.test.AssertByScript">
<log>Assertion name: Scripted Assertion~3 checks for: true  is of type: Assert by Script Execution.</log>
<then>GetRunSteps</then>
<valueToAssertKey></valueToAssertKey>
        <script>// This Beanshell script should return a boolean result indicating the assertion is true or false&#13;&#10;int currentRunStepLoop = testExec.getStateValue(&quot;runStepLoopStart&quot;)+1;&#13;&#10;int runStepCountCaptured = {{runStepCount}};&#13;&#10;if (runStepCountCaptured &gt;= currentRunStepLoop){&#13;&#10;    testExec.setStateValue(&quot;runStepLoopStart&quot;, currentRunStepLoop);&#13;&#10;    return true;&#13;&#10;}</script>
        <language>beanshell</language>
        <copyprops>TestExecPropsAndSystemProps</copyprops>
</CheckResult>

<url>https://qualitycenter.internal.t-mobile.com:8443/qcbin/rest/domains/{{DomainName}}/projects/{{ProjectName}}/runs/{{runID}}/run-steps/{{runStepID}}</url>
<content>&lt;?xml version=&quot;1.0&quot; encoding=&quot;UTF-8&quot; standalone=&quot;yes&quot;?&gt;&#13;&#10;&lt;Entity Type=&quot;run-step&quot;&gt;&#13;&#10;&lt;Fields&gt;&#13;&#10;&lt;Field Name=&quot;status&quot;&gt;&#13;&#10;&lt;Value&gt;{{Status}}&lt;/Value&gt;&#13;&#10;&lt;/Field&gt;&#13;&#10;&lt;/Fields&gt;&#13;&#10;&lt;/Entity&gt;</content>
<content-type>application/xml</content-type>
<data-type>XML</data-type>
      <header field="LWSSO_COOKIE_KEY" value="{{LWSSO_COOKIE_KEY}}" />
      <header field="QCSession" value="{{QCSession}}" />
<httpMethod>PUT</httpMethod>
<onError>abort</onError>
    </Node>


    <Node name="UpdateRun-Passed" log=""
          type="com.itko.lisa.ws.rest.RESTNode" 
          version="3" 
          uid="26C5427F54BE11E6AF0AB23320524153" 
          think="500-1S" 
          useFilters="true" 
          quiet="false" 
          next="Read_Request" > 

<url>https://qualitycenter.internal.t-mobile.com:8443/qcbin/rest/domains/{{DomainName}}/projects/{{ProjectName}}/runs/{{runID}}</url>
<content>&lt;?xml version=&quot;1.0&quot; encoding=&quot;UTF-8&quot; standalone=&quot;yes&quot;?&gt;&#10;&lt;Entity Type=&quot;run&quot;&gt;&#10;&lt;Fields&gt;&#10;&lt;Field Name=&quot;status&quot;&gt;&#10;&lt;Value&gt;{{Status}}&lt;/Value&gt;&#10;&lt;/Field&gt;&#10;&lt;/Fields&gt;&#10;&lt;/Entity&gt;</content>
<content-type>application/xml</content-type>
<data-type>XML</data-type>
      <header field="LWSSO_COOKIE_KEY" value="{{LWSSO_COOKIE_KEY}}" />
      <header field="QCSession" value="{{QCSession}}" />
<httpMethod>PUT</httpMethod>
<onError>abort</onError>
    </Node>


    <Node name="Read_Request" log=""
          type="com.itko.lisa.test.FileNode" 
          version="1" 
          uid="F3FC54598911E687B208FB20524153" 
          think="500-1S" 
          useFilters="true" 
          quiet="true" 
          next="AttachRequest" > 

<Loc>{{LISA_RELATIVE_PROJ_ROOT}}/Data/SOAPServices/ClaimService/Results/logs/{{TestCase_Name}}-req.txt</Loc>
<charset>DEFAULT</charset>
<PropKey>Request_Attach</PropKey>
<onFail>abort</onFail>
    </Node>


    <Node name="AttachRequest" log=""
          type="com.itko.lisa.ws.rest.RESTNode" 
          version="3" 
          uid="D1F60C758AF11E687EFB23320524153" 
          think="500-1S" 
          useFilters="true" 
          quiet="false" 
          next="Read_Response" > 

<url>https://qualitycenter.internal.t-mobile.com:8443/qcbin/rest/domains/{{DomainName}}/projects/{{ProjectName}}/runs/{{runID}}/attachments</url>
<content>{{Request_Attach}}</content>
<content-type>application/octet-stream</content-type>
<data-type>text</data-type>
      <header field="Content-Type" value="multipart/form-datac" />
      <header field="slug" value="{{TestCase_Name}}-req.txt" />
      <header field="Accept" value="application/json" />
      <header field="Connection" value="Keep-Alive" />
      <header field="Accept-Encoding" value="gzip, deflate" />
      <header field="User-Agent" value=" Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; WOW64; Trident/6.0)" />
<httpMethod>POST</httpMethod>
<onError>abort</onError>
    </Node>


    <Node name="Read_Response" log=""
          type="com.itko.lisa.test.FileNode" 
          version="1" 
          uid="5E432979598911E687B208FB20524153" 
          think="500-1S" 
          useFilters="true" 
          quiet="true" 
          next="AttachResponse" > 

<Loc>{{LISA_RELATIVE_PROJ_ROOT}}/Data/SOAPServices/ClaimService/Results/logs/{{TestCase_Name}}-rsp.txt</Loc>
<charset>DEFAULT</charset>
<PropKey>Response_Attach</PropKey>
<onFail>abort</onFail>
    </Node>


    <Node name="AttachResponse" log=""
          type="com.itko.lisa.ws.rest.RESTNode" 
          version="3" 
          uid="D1F60C758AF11E687EFB23320524153" 
          think="500-1S" 
          useFilters="true" 
          quiet="false" 
          next="getTestID" > 

<url>https://qualitycenter.internal.t-mobile.com:8443/qcbin/rest/domains/{{DomainName}}/projects/{{ProjectName}}/runs/{{runID}}/attachments</url>
<content>{{Response_Attach}}</content>
<content-type>application/octet-stream</content-type>
<data-type>text</data-type>
      <header field="Content-Type" value="multipart/form-datac" />
      <header field="slug" value="{{TestCase_Name}}-rsp.txt" />
      <header field="Accept" value="application/json" />
      <header field="Connection" value="Keep-Alive" />
      <header field="Accept-Encoding" value="gzip, deflate" />
      <header field="User-Agent" value=" Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.1; WOW64; Trident/6.0)" />
<httpMethod>POST</httpMethod>
<onError>abort</onError>
    </Node>


    <Node name="createTestSet" log=""
          type="com.itko.lisa.ws.rest.RESTNode" 
          version="3" 
          uid="26C5428054BE11E6AF0AB23320524153" 
          think="500-1S" 
          useFilters="true" 
          quiet="false" 
          next="getTestID" > 


      <!-- Filters -->
      <Filter type="com.itko.lisa.xml.FilterXMLXPath">
        <valueToFilterKey>lisa.createTestSet.rsp</valueToFilterKey>
<prop>testSetFolderParentID</prop>
<xpathq>//Entity[{{testSetFoldersCount}}]/Fields/Field[@Name=&quot;parent-id&quot;]/Value/text()</xpathq>
      </Filter>

      <Filter type="com.itko.lisa.xml.FilterXMLXPath">
        <valueToFilterKey>lisa.createTestSet.rsp</valueToFilterKey>
<prop>testSetFolderID</prop>
<xpathq>//Entity[{{testSetFoldersCount}}]/Fields/Field[@Name=&quot;id&quot;]/Value/text()</xpathq>
      </Filter>

      <Filter type="com.itko.lisa.xml.FilterXMLXPath">
        <valueToFilterKey>lisa.createTestSet.rsp</valueToFilterKey>
<prop>testSetID</prop>
<xpathq>//Field[@Name=&quot;id&quot;]/Value/text()</xpathq>
      </Filter>

<url>https://qualitycenter.internal.t-mobile.com:8443/qcbin/rest/domains/{{DomainName}}/projects/{{ProjectName}}/test-sets</url>
<content>&lt;?xml version=&quot;1.0&quot; encoding=&quot;UTF-8&quot; standalone=&quot;yes&quot;?&gt;&#10;&lt;Entity Type=&quot;test-set&quot;&gt;&#10;&lt;Fields&gt;&#10;&lt;Field Name=&quot;name&quot;&gt;&lt;Value&gt;{{TestSetName}}&lt;/Value&gt;&lt;/Field&gt;&#10;&lt;Field Name=&quot;parent-id&quot;&gt;&lt;Value&gt;{{testSetFolderID}}&lt;/Value&gt;&lt;/Field&gt;&#10;&lt;!--Field Name=&quot;user-04&quot;&gt;&lt;Value&gt;{{EnterpriseProject}}&lt;/Value&gt;&lt;/Field--&gt;&#10;&lt;Field Name=&quot;user-05&quot;&gt;&lt;Value&gt;DIT&lt;/Value&gt;&lt;/Field&gt;&#10;&lt;Field Name=&quot;user-06&quot;&gt;&lt;Value&gt;Manual&lt;/Value&gt;&lt;/Field&gt;&#10;&lt;Field Name=&quot;user-07&quot;&gt;&lt;Value&gt;6&lt;/Value&gt;&lt;/Field&gt;&#10;&lt;Field Name=&quot;subtype-id&quot;&gt;&lt;Value&gt;hp.qc.test-set.default&lt;/Value&gt;&lt;/Field&gt;&#10;&lt;/Fields&gt;&#10;&lt;/Entity&gt;</content>
<content-type>application/xml</content-type>
<data-type>XML</data-type>
      <header field="LWSSO_COOKIE_KEY" value="{{LWSSO_COOKIE_KEY}}" />
      <header field="QCSession" value="{{QCSession}}" />
<httpMethod>POST</httpMethod>
<onError>abort</onError>
    </Node>


    <Node name="Sign Out" log=""
          type="com.itko.lisa.ws.rest.RESTNode" 
          version="3" 
          uid="26C5428154BE11E6AF0AB23320524153" 
          think="500-1S" 
          useFilters="true" 
          quiet="false" 
          next="end" > 

<url>https://qualitycenter.internal.t-mobile.com:8443/qcbin/authentication-point/logout</url>
<data-type>text</data-type>
<httpMethod>GET</httpMethod>
<onError>continue</onError>
    </Node>


    <Node name="UpdateTestStatus-NoRun" log=""
          type="com.itko.lisa.ws.rest.RESTNode" 
          version="3" 
          uid="26C5428254BE11E6AF0AB23320524153" 
          think="500-1S" 
          useFilters="true" 
          quiet="false" 
          next="getRUN-ID" > 


      <!-- Filters -->
      <Filter type="com.itko.lisa.xml.FilterXMLXPath">
        <valueToFilterKey>lisa.UpdateTestStatus-NoRun.rsp</valueToFilterKey>
<prop>testInstanceID</prop>
<xpathq>//Field[@Name=&quot;id&quot;]/Value/text()</xpathq>
      </Filter>

<url>https://qualitycenter.internal.t-mobile.com:8443/qcbin/rest/domains/{{DomainName}}/projects/{{ProjectName}}/test-instances?query=%7Bid%5B{{testInstanceID}}%5D%7D</url>
<content>&lt;?xml version=&quot;1.0&quot; encoding=&quot;UTF-8&quot; standalone=&quot;yes&quot;?&gt;&#10;&lt;Entity Type=&quot;test-instances&quot;&gt;&#10;&lt;Fields&gt;&#10;&lt;Field Name=&quot;status&quot;&gt;&#10;&lt;Value&gt;No Run&lt;/Value&gt;&#10;&lt;/Field&gt;&#10;&lt;/Fields&gt;&#10;&lt;/Entity&gt;</content>
<content-type>application/xml</content-type>
<data-type>XML</data-type>
      <header field="LWSSO_COOKIE_KEY" value="{{LWSSO_COOKIE_KEY}}" />
      <header field="QCSession" value="{{QCSession}}" />
      <header field="Content-Type" value="application/xml" />
<httpMethod>PUT</httpMethod>
<onError>abort</onError>
    </Node>


    <Node name="end" log=""
          type="com.itko.lisa.test.NormalEnd" 
          version="1" 
          uid="26C5428554BE11E6AF0AB23320524153" 
          think="0h" 
          useFilters="true" 
          quiet="true" 
          next="fail" > 

    </Node>


    <Node name="fail" log=""
          type="com.itko.lisa.test.Abend" 
          version="1" 
          uid="26C5428454BE11E6AF0AB23320524153" 
          think="0h" 
          useFilters="true" 
          quiet="true" 
          next="abort" > 

    </Node>


    <Node name="abort" log=""
          type="com.itko.lisa.test.AbortStep" 
          version="1" 
          uid="26C5428354BE11E6AF0AB23320524153" 
          think="0h" 
          useFilters="true" 
          quiet="true" 
          next="" > 

    </Node>


    <DataSet type="com.itko.lisa.test.DataFile" name="Read Rows from a Delimited Data File" atend="Sign Out" local="true" random="false" maxItemsToFetch="0" >
<sample>rO0ABXNyABFqYXZhLnV0aWwuSGFzaE1hcAUH2sHDFmDRAwACRgAKbG9hZEZhY3RvckkACXRocmVzaG9sZHhwP0AAAAAAAAF3CAAAAAIAAAACdAA4Uy5ObwlUZXN0Q2FzZV9OYW1lCVJlcXVlc3RGaWxlCVN0YXR1cwlFeHBlY3RlZCBWcyBBY3R1YWx0AI8xCVRJQkNPX09DRV9jcnRPcmRDaGtfUHltbnRNdGhkQ3JlZGl0X1BfVEMwMDAxCWNyZWF0ZU9yZGVyQ2hlY2tvdXRfQ3JlZGl0CUZhaWwJRWxlbWVudDogY29kZSBFeHBlY3RlZCBWYWx1ZTogMTAwIE5vdCBNYXRjaGluZyBBY3R1YWwgVmFsdWU6IDEwMnQAK1JlYWQgUm93cyBmcm9tIGEgRGVsaW1pdGVkIERhdGEgRmlsZV9Sb3dOdW10AAExeA==</sample>
    <location>{{ResultsPath}}</location>
    <charset>DEFAULT</charset>
    <delim>,</delim>
    </DataSet>

</TestCase>
